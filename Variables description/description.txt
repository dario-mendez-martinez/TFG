//****** DESCRIPCIÓN DE LAS VARIABLES DE SESIÓN DEL SISTEMA ******\\

$_SESSION["permitido"] : Variable que es true cuando el usuario ha hecho login con éxito.
$_SESSION["tipo"] : Variable que indica el rol del usuario en la plataforma (user/admin).
$_SESSION["nick"] : Variable que almacena el nick del usuario.
$_SESSION['borradoUsuario'] : Variable que es true cuando el usuario ha sido borrado a nivel lógico.
$_SESSION['error_nick'] : Variable que es true cuando el nick introducido en el registro ya existe.
$_SESSION['error_mail'] : Variable que es true cuando el email introducido en el registro ya existe.
$_SESSION["listarUsuarios"] : Variable de sesión usada para listar los usuarios. Se reutiliza para filtrar los mismos.
$_SESSION["foto"]: Variable que almacena la url de la imagen de usuario en el servidor.
$_SESSION["perfil"]: Variable de sesión que contiene los datos de un usuario.
$_SESSION["grupo_existe"]: Variable que está a true cuando el nombre del grupo introducido ya existe en base de datos.
$_SESSION["listarGrupos"]: Variable que contiene todos los grupo a ser mostrados.
$_SESSION["ConsultarGrupo"]: Variable que contiene los datos de un grupo.
$_SESSION["ConsultarUsuariosGrupo"]: Variable que contiene los usuarios en un grupo.
$_SESSION["pertenece"]: Comprueba si el usuario pertene o no a un evento o un grupo.
$_SESSION["peticion"]: Comprueba si la peticion para entrar en un grupo o evento pertenece.
$_SESSION["listarSolicitudesRecibidas"]: Lista las solicitudes recibidas para eventos o grupo.
$_SESSION["listarSolicitudesEmitidas"]: Lista las solicitudes enviadas/emitidas para eventos o grupo.
$_SESSION["listarAmigos"]: Variable que contiene los amigos de un usuario.
$_SESSION["listarPublicaciones"]: Lista las publicaciones de un usuario.
$_SESSION["consultarPublicacion"]: Consulta los datos de una publicacion.
$_SESSION["comentarios_padre"]: Devuelve los comentarios principales de una pubicacion.
$_SESSION["comentarios_hijo"]: Variable que contiene los comentarios hijos de los comentarios padre de una publicacion.
$_SESSION["conversacion"]: Lista las conversaciones enviadas y contine los mensajes de una conversacion en el detalle de la conversación.
$_SESSION["conversacionRecibida"]: Contiene las conversaciones que no ha empezado el usuario.