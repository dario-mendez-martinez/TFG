# TFG-Dario Mendez Martinez

## Consejos:

Los usuarios existentes actualmente en la base de datos tiene como tanto el nick como la password igual,ejemplos:
ada - ada (usuario registrado)
esei - esei (usuario administrador)

## Estructura del proyecto:

#### BD:

#### EXISTE UN SCRIPT DE IMPORTACIÓN PARA LA BASE DE DATOS:
    Ejecutar:
    
        sudo chmod +x import-bd.sh
        sudo ./importar-bd.sh
        
	Aquí se encuentran dos ficheros,uno con la base de datos de prueba y otro con una copia funcional de una versión anterior.

#### Controller: En esta carpeta se encuentran los diferentes controladores de la aplicación.
		1. EventoController: Controlador que gestiona todos los eventos.
		2. GrupoController: Controllador que gestiona todo lo relativo a los grupos de la aplicación.
		3. PublicaciónController: Controlador que gestiona todo lo relativo a una publicación.
		4. UsuarioController: Controlador que gestiona todo lo relativo a los usuarios.

#### Model: En esta carpeta encontramos la recuperación de datos desde la Base de Datos y su configuración.
		1. Comentario: Modelo para la recuperación de comentarios.
		2. ConectarBD: Configuración para la conexión de la base de datos.
		3. Evento: Modelo con todo lo relacionado a los eventos.
		4. Grupo: Modelo con todo lo relacionado a los grupos.
		5. Publicacion: Modelo con todo lo relacionado a las publicaciones.
		6. Usuarios: Modelo con todo lo relacionado a los usuarios.

#### Recursos:
	Aqui se encuentran los diferentes estilos diferenciados por roles,es decir, distintos estilos para usuarios y para administradores

#### Variables description:
	Descripción de la diferentes variables de sesión para tener una guía de su uso en la aplicación.

#### View:
	La vistas usadas y diferenciadas según su rol (Administrador y Usuario).

#### index.php:
	Archivo para redireccion directa cuando se inicia la aplicación.

#### Readme.md:
	Descripción del proyecto.
