-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 05-04-2017 a las 19:30:52
-- Versión del servidor: 5.5.54-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `social`
--

DROP  DATABASE IF EXISTS`social`;
CREATE DATABASE IF NOT EXISTS `social` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `social`;


--
-- usuario social contraseña social
--

grant all privileges on `social`.* to 'social'@'localhost'
     identified by 'social' with grant option;
 flush privileges;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `amigo`
--

CREATE TABLE IF NOT EXISTS `amigo` (
  `nickEmisor` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nickReceptor` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_peticion` date DEFAULT NULL,
  `fecha_aceptacion` date DEFAULT NULL,
  `borradoAmigo` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nickEmisor`,`nickReceptor`),
  KEY `amigo_ibfk_2` (`nickReceptor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `amigo`
--

INSERT INTO `amigo` (`nickEmisor`, `nickReceptor`, `fecha_peticion`, `fecha_aceptacion`, `borradoAmigo`) VALUES
('pepe', 'esei', '2017-04-05', '2017-04-04', 0),
('pepe', 'ramon', '2017-03-09', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `codComentario` int(11) NOT NULL AUTO_INCREMENT,
  `codPublicacion` int(11) NOT NULL,
  `nickUser` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_comentario` date NOT NULL,
  PRIMARY KEY (`codComentario`),
  KEY `comentario_ibfk_1` (`nickUser`),
  KEY `comentario_ibfk_2` (`codPublicacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`codComentario`, `codPublicacion`, `nickUser`, `comentario`, `fecha_comentario`) VALUES
(1, 1, 'esei', 'prueba1,comentario', '2017-04-05'),
(2, 2, 'esei', 'prueba2,comentario', '2017-04-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `evento` (
  `nombreEvento` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nick` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `tipoUserEvento` varchar(10) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'admin',
  `publico` tinyint(1) NOT NULL DEFAULT '0',
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `AvatarEvento` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`nombreEvento`),
  KEY `evento_ibfk_1` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`nombreEvento`, `nick`, `tipoUserEvento`, `publico`, `borrado`, `AvatarEvento`) VALUES
('ejemplo1', 'ramon', 'admin', 0, 1, ''),
('ejemplo2', 'usuario2', 'admin', 1, 0, ''),
('ejemplo3', 'esei', 'admin', 1, 0, ''),
('graduacion', 'esei', 'admin', 1, 0, ''),
('prueba1', 'esei', 'admin', 0, 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE IF NOT EXISTS `grupo` (
  `nombreGrupo` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nick` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `tipoUserGrupo` varchar(10) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'admin',
  `publico` tinyint(1) NOT NULL DEFAULT '0',
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `AvatarGrupo` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`nombreGrupo`),
  KEY `grupo_ibfk_1` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`nombreGrupo`, `nick`, `tipoUserGrupo`, `publico`, `borrado`, `AvatarGrupo`) VALUES
('administracion', 'pepe', 'admin', 0, 0, '../Recursos/img/Foto.jpg'),
('baseDatos', 'pepe', 'admin', 0, 0, '../Recursos/img/Foto.jpg'),
('ejemplo1', 'usuario2', 'admin', 0, 0, '../Recursos/img/Foto.jpg'),
('investigacion', 'esei', 'admin', 1, 0, '../Recursos/img/Foto.jpg'),
('soii', 'usuario1', 'admin', 0, 0, '../Recursos/img/Foto.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembrosEvento`
--

CREATE TABLE IF NOT EXISTS `miembrosEvento` (
  `nombreEvento` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nick` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `administrador` varchar(10) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'user',
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nombreEvento`,`nick`),
  KEY `miembrosEvento_ibfk_1` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `miembrosEvento`
--

INSERT INTO `miembrosEvento` (`nombreEvento`, `nick`, `administrador`, `borrado`) VALUES
('ejemplo2', 'esei', 'user', 0),
('ejemplo3', 'esei', 'admin', 0),
('ejemplo3', 'usuario1', 'user', 0),
('prueba1', 'esei', 'admin', 0),
('prueba1', 'usuario1', 'user', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembrosGrupo`
--

CREATE TABLE IF NOT EXISTS `miembrosGrupo` (
  `nombreGrupo` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nick` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `administrador` varchar(10) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'user',
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nombreGrupo`,`nick`),
  KEY `miembrosGrupo_ibfk_1` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `miembrosGrupo`
--

INSERT INTO `miembrosGrupo` (`nombreGrupo`, `nick`, `administrador`, `borrado`) VALUES
('administracion', 'esei', 'admin', 0),
('administracion', 'usuario1', 'user', 0),
('baseDatos', 'ramon', 'admin', 0),
('baseDatos', 'usuario1', 'admin', 0),
('investigacion', 'pepe', 'user', 0),
('investigacion', 'ramon', 'user', 0),
('investigacion', 'usuario2', 'user', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE IF NOT EXISTS `publicacion` (
  `codPublicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nickUser` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nombreGrupo` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nombreEvento` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `descripcion` varchar(500) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  `fecha_publicacion` date DEFAULT NULL,
  `borradoPublicacion` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codPublicacion`),
  KEY `publicacion_ibfk_1` (`nickUser`),
  KEY `publicacion_ibfk_2` (`nombreGrupo`),
  KEY `publicacion_ibfk_3` (`nombreEvento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`codPublicacion`, `nickUser`, `nombreGrupo`, `nombreEvento`, `descripcion`, `fecha_publicacion`, `borradoPublicacion`) VALUES
(1, 'esei', 'administracion', NULL, 'hola,prueba1,con grupo', '2017-04-05', 0),
(2, 'pepe', NULL, 'ejemplo1', 'prueba2,con evento', '2017-04-05', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestaComentario`
--

CREATE TABLE IF NOT EXISTS `respuestaComentario` (
  `codComentario_principal` int(11) NOT NULL AUTO_INCREMENT,
  `codComentario_secundario` int(11) NOT NULL,
  `codPublicacion` int(11) NOT NULL,
  `nickUser` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_respuestacomentario` date NOT NULL,
  PRIMARY KEY (`codComentario_principal`,`codComentario_secundario`,`codPublicacion`),
  KEY `respuestaComentario_ibfk_1` (`nickUser`),
  KEY `respuestaComentario_2` (`codPublicacion`),
  KEY `respuestaComentario_4` (`codComentario_secundario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `tipoUser` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `passUser` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `emailUser` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nick` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `cargo` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `Avatar` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`tipoUser`, `passUser`, `emailUser`, `nick`, `nombre`, `apellidos`, `telefono`, `cargo`, `borrado`, `Avatar`) VALUES
('admin', 'esei', 'esei@gmail.es', 'esei', 'esei', 'esei', 'esei', 'administrador', 0, '../Recursos/img/Foto.jpg'),
('user', '1234', 'pepe@gmail.es', 'pepe', 'ramon', 'ramon', 'ramon', 'ramon', 0, '../Recursos/img/Foto.jpg'),
('admin', '1234', 'organizador@gmail.es', 'ramon', 'ramon', 'ramon', 'ramon', 'ramon', 0, '../Recursos/img/Foto.jpg'),
('user', '1234', 'ejemplo1@gmail.com', 'usuario1', 'usuario1', 'usuario1usuario1', '999888777', 'director', 0, '../Recursos/img/Foto.jpg'),
('user', '1234', 'ejemplo2@gmail.com', 'usuario2', 'usuario2', 'usuario2usuario2', '999888776', 'becario', 0, '../Recursos/img/Foto.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosEvento`
--

CREATE TABLE IF NOT EXISTS `usuariosEvento` (
  `nombreEvento` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nickPeticion` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nickRecepcion` varchar(30) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  `fecha_peticion` date DEFAULT NULL,
  `fecha_aceptacion` date DEFAULT NULL,
  PRIMARY KEY (`nombreEvento`,`nickPeticion`,`nickRecepcion`),
  KEY `usuariosEvento_ibfk_2` (`nickPeticion`),
  KEY `usuariosEvento_ibfk_1` (`nickRecepcion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuariosEvento`
--

INSERT INTO `usuariosEvento` (`nombreEvento`, `nickPeticion`, `nickRecepcion`, `fecha_peticion`, `fecha_aceptacion`) VALUES
('ejemplo1', 'esei', 'ramon', '2017-04-13', NULL),
('ejemplo2', 'usuario1', 'usuario2', '2017-04-05', NULL),
('ejemplo3', 'usuario1', 'esei', '2017-04-06', '2017-04-05'),
('graduacion', 'usuario1', 'esei', '2017-04-05', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosGrupo`
--

CREATE TABLE IF NOT EXISTS `usuariosGrupo` (
  `nombreGrupo` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nickPeticion` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `nickRecepcion` varchar(30) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  `fecha_peticion` date DEFAULT NULL,
  `fecha_aceptacion` date DEFAULT NULL,
  PRIMARY KEY (`nombreGrupo`,`nickPeticion`,`nickRecepcion`),
  KEY `usuariosGrupo_ibfk_2` (`nickPeticion`),
  KEY `usuariosGrupo_ibfk_1` (`nickRecepcion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuariosGrupo`
--

INSERT INTO `usuariosGrupo` (`nombreGrupo`, `nickPeticion`, `nickRecepcion`, `fecha_peticion`, `fecha_aceptacion`) VALUES
('administracion', 'pepe', 'pepe', '2017-03-09', NULL),
('baseDatos', 'pepe', 'pepe', '2017-03-17', NULL),
('ejemplo1', 'usuario2', 'pepe', '2017-03-18', NULL),
('investigacion', 'ramon', 'pepe', '2017-04-12', '2017-04-04'),
('investigacion', 'usuario1', 'pepe', '2017-04-04', NULL),
('investigacion', 'usuario2', 'pepe', '2017-04-19', '2017-04-04'),
('soii', 'usuario1', 'pepe', '2017-03-02', NULL);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `amigo`
--
ALTER TABLE `amigo`
  ADD CONSTRAINT `amigo_ibfk_1` FOREIGN KEY (`nickEmisor`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `amigo_ibfk_2` FOREIGN KEY (`nickReceptor`) REFERENCES `usuario` (`nick`);

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `comentario_ibfk_1` FOREIGN KEY (`nickUser`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `comentario_ibfk_2` FOREIGN KEY (`codPublicacion`) REFERENCES `publicacion` (`codPublicacion`);

--
-- Filtros para la tabla `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`nick`) REFERENCES `usuario` (`nick`);

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `grupo_ibfk_1` FOREIGN KEY (`nick`) REFERENCES `usuario` (`nick`);

--
-- Filtros para la tabla `miembrosEvento`
--
ALTER TABLE `miembrosEvento`
  ADD CONSTRAINT `miembrosEvento_ibfk_1` FOREIGN KEY (`nick`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `miembrosEvento_ibfk_2` FOREIGN KEY (`nombreEvento`) REFERENCES `evento` (`nombreEvento`);

--
-- Filtros para la tabla `miembrosGrupo`
--
ALTER TABLE `miembrosGrupo`
  ADD CONSTRAINT `miembrosGrupo_ibfk_1` FOREIGN KEY (`nick`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `miembrosGrupo_ibfk_2` FOREIGN KEY (`nombreGrupo`) REFERENCES `grupo` (`nombreGrupo`);

--
-- Filtros para la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD CONSTRAINT `publicacion_ibfk_1` FOREIGN KEY (`nickUser`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `publicacion_ibfk_2` FOREIGN KEY (`nombreGrupo`) REFERENCES `grupo` (`nombreGrupo`),
  ADD CONSTRAINT `publicacion_ibfk_3` FOREIGN KEY (`nombreEvento`) REFERENCES `evento` (`nombreEvento`);

--
-- Filtros para la tabla `respuestaComentario`
--
ALTER TABLE `respuestaComentario`
  ADD CONSTRAINT `respuestaComentario_ibfk_1` FOREIGN KEY (`nickUser`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `respuestaComentario_2` FOREIGN KEY (`codPublicacion`) REFERENCES `publicacion` (`codPublicacion`),
  ADD CONSTRAINT `respuestaComentario_3` FOREIGN KEY (`codComentario_principal`) REFERENCES `comentario` (`codComentario`),
  ADD CONSTRAINT `respuestaComentario_4` FOREIGN KEY (`codComentario_secundario`) REFERENCES `comentario` (`codComentario`);

--
-- Filtros para la tabla `usuariosEvento`
--
ALTER TABLE `usuariosEvento`
  ADD CONSTRAINT `usuariosEvento_ibfk_1` FOREIGN KEY (`nickRecepcion`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `usuariosEvento_ibfk_2` FOREIGN KEY (`nickPeticion`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `usuariosEvento_ibfk_3` FOREIGN KEY (`nombreEvento`) REFERENCES `evento` (`nombreEvento`);

--
-- Filtros para la tabla `usuariosGrupo`
--
ALTER TABLE `usuariosGrupo`
  ADD CONSTRAINT `usuariosGrupo_ibfk_1` FOREIGN KEY (`nickRecepcion`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `usuariosGrupo_ibfk_2` FOREIGN KEY (`nickPeticion`) REFERENCES `usuario` (`nick`),
  ADD CONSTRAINT `usuariosGrupo_ibfk_3` FOREIGN KEY (`nombreGrupo`) REFERENCES `grupo` (`nombreGrupo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
