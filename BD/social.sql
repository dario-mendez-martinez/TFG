SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
--
-- usuario social contraseña social
--
CREATE USER IF NOT EXISTS 'social'@'localhost' IDENTIFIED WITH mysql_native_password BY 'social';

/*grant all privileges on `social`.* to 'social'@'localhost';
 flush privileges;
 GRANT ALL PRIVILEGES ON *.* TO 'social'@'localhost' IDENTIFIED BY 'social';
 */
GRANT ALL PRIVILEGES ON *.* TO 'social'@'localhost' IDENTIFIED WITH mysql_native_password BY 'social';
--
-- Base de datos: `social`
--

DROP DATABASE IF EXISTS`social`;
CREATE DATABASE IF NOT EXISTS `social` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `social`;
-- --------------------------------------------------------

--
-- Tabla Usuario
--

CREATE TABLE usuario (
  nick VARCHAR(50) PRIMARY KEY NOT NULL COMMENT 'Nombre único del usuario',
  password VARCHAR(255) NOT NULL COMMENT 'Contraseña del usuario',
  email VARCHAR(255) UNIQUE NOT NULL COMMENT 'Email del usuario',
  nombre_usuario VARCHAR(50) DEFAULT NULL COMMENT 'Nombre del usuario',
  apellido_usuario VARCHAR(50) DEFAULT NULL COMMENT 'Apellido del usuario',
  telefono VARCHAR(10) DEFAULT NULL COMMENT 'Telefono del usuario',
  borrado_usuario BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario es eliminado',
  cargo VARCHAR(50) DEFAULT NULL COMMENT 'Cargo que desempeña el usuario en la empresa',
  foto VARCHAR(255) DEFAULT NULL COMMENT 'Imagen del usuario',
  descripcion_usuario VARCHAR(50) DEFAULT NULL COMMENT 'Pequeña descripción del usuario',
  tipo_usuario VARCHAR(50) DEFAULT 'user' COMMENT 'Si el usuario es o no administrador',
  fecha_registro_usuario DATETIME COMMENT 'Fecha de registro de usuario'

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `usuario` (`nick`, `password`, `email`, `nombre_usuario`, `apellido_usuario`, `telefono`, `borrado_usuario`, `cargo`, `descripcion_usuario`,`fecha_registro_usuario`, `tipo_usuario`,`foto`) VALUES
('esei', '$2y$10$drda7kmwYgAGsDSSDLKUtOJsvkUhep9YyZIVKm7UBmaPU2RzA41La', 'esei@esei.es', 'esei', 'esei', '622170844', 0, 'administrador', 'Administrador principal del sistema',NOW(),'admin', '../../Recursos/img/comun.png'),
('oliver', '$2y$10$.qSmHZ0sN9JF9tITI0fNP.eF4VkuYFrqVGf9l2leRUPXLZC7FT8.u', 'oliver@esei.es', 'Oliver', 'Atom', '600000000', 0, 'repartidor', 'Reparto el juego en la oficina',NOW(),'user', '../../Recursos/img/comun.png'),
('tom', '$2y$10$uxY6N2Pa3rAsO.q2hbZiJ.HPTdKtMn/14W5dw9s40wBUOCIa7PhLq', 'tom@esei.es', 'Tom', 'Baker', '600000000', 0, 'ayudante', 'Asisto a mis compañeros',NOW(),'user', '../../Recursos/img/comun.png'),
('mark', '$2y$10$1CDL86vowkB1IhQo0EAHhODr11ycPerHdrHk6kU3Ezjp7GYU.gHYW', 'mark@esei.es', 'Mark', 'Lenders', '600000000', 0, 'ejecutivo', 'Me encargo de concretar los negocios',NOW(),'user', '../../Recursos/img/comun.png'),
('dario', '$2y$10$c3/sxxVc5PtvAPIIKYmObOQFZbIOAtOMInvkWMP3tremxMcbP7WUS', 'dario@esei.es', 'Dario', 'Mendez', '600000000', 0, 'Gestor', 'Gestiono la plataforma',NOW(),'user', '../../Recursos/img/comun.png'),
('cliford', '$2y$10$viH2js1JmpjNnbnFHq4jFubNTBTrh8JMyZKB0GVzKvo6OXzf2Zxpe', 'cliford@esei.es', 'Cliford', 'Vázquez', '600000000', 0, 'mascota', 'El perrete de la oficina',NOW(),'user', '../../Recursos/img/comun.png'),
('ada', '$2y$10$KFghMI/N4.G6Lg13uVp7au1K1a0Kk1aIY7C.jkl5GcvxQSu6jqq7q', 'ada@esei.es', 'Ada', 'Lovelace', '600000000', 0, 'Innovadora', 'Creo historia de la informática',NOW(),'user', '../../Recursos/img/comun.png'),
('javier', '$2y$10$xX1KJMp72v2nnMw6A51R8O9Qhpby83kgRQlfUE51qQw0MyR797CIi', 'javier@esei.es', 'Javier', 'Uno', '600000000', 0, 'Gurú', 'Ayudo a mis secuaces a encontrar su camino',NOW(),'user', '../../Recursos/img/comun.png'),
('altair', '$2y$10$62ETLhb.HE3UYNoihd0/2umriOcQB0CYL1qCDbYikRN4A9tOrJnqG', 'altair@esei.es', 'Darius', 'Altair', '600000000', 0, 'ayudante del gestor', 'Limpio la suciedad en la plataforma que el gestor deja',NOW(),'user', '../../Recursos/img/comun.png'),
('paco', '$2y$10$oLF4gx9Q8kx9p8QJvFT3xeqnzcWDEmDY./hCcJ4bi/KWzYX17vIsG', 'paco@esei.es', 'Paco', 'Tar', '600000000', 0, 'Mítico', 'Un sistema sin un Paco no sería lo mismo',NOW(),'user', '../../Recursos/img/comun.png');



--
-- Tabla Grupo
--
CREATE TABLE grupo (
  nombre_grupo VARCHAR(50) PRIMARY KEY NOT NULL COMMENT 'Nombre único del grupo',
  descripcion_grupo VARCHAR(255) DEFAULT NULL COMMENT 'Descripción del grupo',
  fecha_creacion_grupo DATETIME DEFAULT NULL COMMENT 'Fecha de creación del grupo',
  imagen_grupo VARCHAR(255) DEFAULT NULL COMMENT 'Imagen del grupo',
  limite_usuarios_grupo INT DEFAULT NULL COMMENT 'Número máximo de usuarios en un grupo',
  borrado_grupo BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el grupo es eliminado',
  tipo_grupo BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el grupo es privado',

  nick_creador VARCHAR(50) DEFAULT NULL COMMENT 'Nick del creador del grupo',
  FOREIGN KEY (nick_creador) REFERENCES usuario(nick) ON DELETE SET NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `grupo` (`nombre_grupo`, `descripcion_grupo`, `fecha_creacion_grupo`, `limite_usuarios_grupo`, `borrado_grupo`, `tipo_grupo`, `nick_creador`, `imagen_grupo`) VALUES
('base de datos', 'ninguna por ahora', NOW(), 50, 0,1,'esei', '../../Recursos/img/comun.png'),
('Pruebas', 'ninguna por ahora', NOW(), 50, 0, 0,'paco', '../../Recursos/img/comun.png'),
('Sistemas Operativos', 'ninguna por ahora', NOW(), 50, 0, 1,'oliver', '../../Recursos/img/comun.png'),
('Comidas de empresa', 'ninguna por ahora', NOW(), 50, 0, 0,'dario', '../../Recursos/img/comun.png'),
('Esei', 'ninguna por ahora', NOW(), 50, 0, 0,'altair', '../../Recursos/img/comun.png');

--
-- Tabla Evento
--
CREATE TABLE evento (
  nombre_evento VARCHAR(50) PRIMARY KEY NOT NULL COMMENT 'Nombre único del evento',
  descripcion_evento VARCHAR(255) DEFAULT NULL COMMENT 'Descripción del evento',
  fecha_creacion_evento DATETIME DEFAULT NULL COMMENT 'Fecha de creación del evento',
  imagen_evento VARCHAR(255) DEFAULT NULL COMMENT 'Imagen del evento',
  limite_usuarios_evento INT DEFAULT NULL COMMENT 'Número máximo de usuarios en un evento',
  borrado_evento BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el evento es eliminado',
  tipo_evento BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el evento es privado',

  nick_creador VARCHAR(50) DEFAULT NULL COMMENT 'Nick del creador del evento',
  FOREIGN KEY (nick_creador) REFERENCES usuario(nick) ON DELETE SET NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `evento` (`nombre_evento`, `descripcion_evento`, `fecha_creacion_evento`, `limite_usuarios_evento`, `borrado_evento`, `tipo_evento`, `nick_creador`, `imagen_evento`) VALUES
('base de datos', 'ninguna por ahora', NOW(), 50, 0,1,'esei', '../../Recursos/img/comun.png'),
('Pruebas', 'ninguna por ahora', NOW(), 50, 0, 0,'paco', '../../Recursos/img/comun.png'),
('Sistemas Operativos', 'ninguna por ahora', NOW(), 50, 0, 1,'oliver', '../../Recursos/img/comun.png'),
('Comidas de empresa', 'ninguna por ahora', NOW(), 50, 0, 0,'dario', '../../Recursos/img/comun.png'),
('Esei', 'ninguna por ahora', NOW(), 50, 0, 0,'altair', '../../Recursos/img/comun.png');


--
-- usuario_pertenece_grupo
--
CREATE TABLE usuario_pertenece_grupo (
  nombre_grupo_pertenece VARCHAR(50) NOT NULL COMMENT 'Nombre único del grupo',
  FOREIGN KEY (nombre_grupo_pertenece) REFERENCES grupo(nombre_grupo) ON DELETE CASCADE,

  nick_perteneciente_grupo VARCHAR(50) NOT NULL COMMENT 'Nick del usuario perteneciente al grupo',
  FOREIGN KEY (nick_perteneciente_grupo) REFERENCES usuario(nick) ON DELETE CASCADE,

  PRIMARY KEY (nombre_grupo_pertenece,nick_perteneciente_grupo),

  fecha_ingreso_usuario DATETIME DEFAULT NULL COMMENT 'Fecha de ingreso en el grupo',
  tipo_usuario varchar(10) NOT NULL DEFAULT 'user' COMMENT 'Si el usuario es administrador o no',
  borrado_usuario_grupo BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario es eliminado'

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO `usuario_pertenece_grupo` (`nombre_grupo_pertenece`, `nick_perteneciente_grupo`, `fecha_ingreso_usuario`,`borrado_usuario_grupo`, `tipo_usuario`) VALUES
('base de datos','esei', NOW(), 0, 'admin'),
('Pruebas', 'paco', NOW(), 0, 'admin'),
('Sistemas Operativos', 'oliver', NOW(), 0, 'admin'),
('Comidas de empresa','dario', NOW(), 0, 'admin'),
('Esei','altair', NOW(), 0, 'admin');


--
-- usuario_pertenece_evento
--
CREATE TABLE usuario_pertenece_evento (
  nombre_evento_pertenece VARCHAR(50) NOT NULL COMMENT 'Nombre único del evento',
  FOREIGN KEY (nombre_evento_pertenece) REFERENCES evento(nombre_evento) ON DELETE CASCADE,

  nick_perteneciente_evento VARCHAR(50) NOT NULL COMMENT 'Nick del usuario perteneciente al evento',
  FOREIGN KEY (nick_perteneciente_evento) REFERENCES usuario(nick) ON DELETE CASCADE,

  PRIMARY KEY (nombre_evento_pertenece,nick_perteneciente_evento),

  fecha_ingreso_usuario DATETIME DEFAULT NULL COMMENT 'Fecha de ingreso en el evento',
  tipo_usuario varchar(10) NOT NULL DEFAULT 'user' COMMENT 'Si el usuario es administrador o no',
  borrado_usuario_evento BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario es eliminado'

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


INSERT INTO `usuario_pertenece_evento` (`nombre_evento_pertenece`, `nick_perteneciente_evento`, `fecha_ingreso_usuario`,`borrado_usuario_evento`,`tipo_usuario`) VALUES
('base de datos','esei', NOW(), 0, 'admin'),
('Pruebas', 'paco', NOW(), 0, 'admin'),
('Sistemas Operativos', 'oliver', NOW(), 0, 'admin'),
('Comidas de empresa','dario', NOW(), 0, 'admin'),
('Esei','altair', NOW(), 0, 'admin');

--
-- usuario_invitacion_grupo
--
CREATE TABLE usuario_invitacion_grupo (
  nombre_grupo_invitacion VARCHAR(50) NOT NULL COMMENT 'Nombre único del grupo',
  FOREIGN KEY (nombre_grupo_invitacion) REFERENCES grupo(nombre_grupo) ON DELETE CASCADE,

  nick_emisor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que envía la petición',
  FOREIGN KEY (nick_emisor) REFERENCES usuario(nick) ON DELETE CASCADE,

  nick_receptor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que recibe la petición',
  FOREIGN KEY (nick_receptor) REFERENCES usuario(nick) ON DELETE CASCADE,

  PRIMARY KEY (nombre_grupo_invitacion,nick_emisor,nick_receptor),

  fecha_invitacion_grupo DATETIME DEFAULT NULL COMMENT 'Fecha de invitacion al grupo',
  fecha_aceptacion_grupo DATETIME DEFAULT NULL COMMENT 'Fecha de aceptación en el grupo'

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- usuario_invitacion_evento
--
CREATE TABLE usuario_invitacion_evento (
  nombre_evento_invitacion VARCHAR(50) NOT NULL COMMENT 'Nombre único del evento',
  FOREIGN KEY (nombre_evento_invitacion) REFERENCES evento(nombre_evento) ON DELETE CASCADE,

  nick_emisor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que envía la petición',
  FOREIGN KEY (nick_emisor) REFERENCES usuario(nick) ON DELETE CASCADE,

  nick_receptor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que recibe la petición',
  FOREIGN KEY (nick_receptor) REFERENCES usuario(nick) ON DELETE CASCADE,

  PRIMARY KEY (nombre_evento_invitacion,nick_emisor,nick_receptor),

  fecha_invitacion_evento DATETIME DEFAULT NULL COMMENT 'Fecha de invitacion al evento',
  fecha_aceptacion_evento DATETIME DEFAULT NULL COMMENT 'Fecha de aceptación en el evento'

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- mensaje
--
CREATE TABLE mensaje (
  codigo_mensaje int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'Código auto incremental del mensaje',

  nick_emisor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que envía el mensaje',
  FOREIGN KEY (nick_emisor) REFERENCES usuario(nick) ON DELETE NO ACTION,

  nick_receptor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que recibe el mensaje',
  FOREIGN KEY (nick_receptor) REFERENCES usuario(nick) ON DELETE NO ACTION,

  codigo_mensaje_padre int(11) DEFAULT NULL COMMENT 'Nick del usuario que envía el mensaje',
  FOREIGN KEY (codigo_mensaje_padre) REFERENCES mensaje(codigo_mensaje) ON DELETE CASCADE,

  codigo_mensaje_anterior int(11) DEFAULT NULL COMMENT 'Nick del usuario que recibe el mensaje',
  FOREIGN KEY (codigo_mensaje_anterior) REFERENCES mensaje(codigo_mensaje) ON DELETE CASCADE,

  mensaje TEXT DEFAULT NULL COMMENT 'Contenido del mensaje',
  fecha_envio_mensaje DATETIME DEFAULT NULL COMMENT 'Fecha del envío del mensaje',
  borrado_mensaje BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario ha eliminado el mensaje',
  mensaje_archivado BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario ha archivado el mensaje'
  

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- amigo
--
CREATE TABLE amigo (
  codigo_amistad int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'Código auto incremental de las peticiones de amistad',

  nick_peticion_amistad VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que realiza la petición de amistad',
  FOREIGN KEY (nick_peticion_amistad) REFERENCES usuario(nick) ON DELETE CASCADE,

  nick_receptor_amistad VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que recibe la petición de amistad',
  FOREIGN KEY (nick_receptor_amistad) REFERENCES usuario(nick) ON DELETE CASCADE,

  fecha_peticion_amistad DATETIME DEFAULT NULL COMMENT 'Fecha de la petición de amistad',
  fecha_aceptacion_amistad DATETIME DEFAULT NULL COMMENT 'Fecha de la aceptación de la amistad',
  mensaje_amistad VARCHAR(255) DEFAULT NULL COMMENT 'Pequeño mensaje que se envía con la petición de amistad',
  borrado_amigo BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario elimina a un amigo'  

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- publicacion
--
CREATE TABLE publicacion (
  codigo_publicacion int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'Código auto incremental de la publicacion',

  nick_propietario VARCHAR(11) DEFAULT NULL COMMENT 'Nick del usuario que realiza la publicacion',
  FOREIGN KEY (nick_propietario) REFERENCES usuario(nick) ON DELETE SET NULL,

  nombre_grupo_publicacion VARCHAR(50) DEFAULT NULL COMMENT 'Nombre del grupo en el que se realiza la publicacion',
  FOREIGN KEY (nombre_grupo_publicacion) REFERENCES grupo(nombre_grupo) ON DELETE SET NULL,

  nombre_evento_publicacion VARCHAR(50) DEFAULT NULL COMMENT 'Nombre del grupo en el que se realiza la publicacion',
  FOREIGN KEY (nombre_evento_publicacion) REFERENCES evento(nombre_evento) ON DELETE SET NULL,

  contenido TEXT DEFAULT NULL COMMENT 'Contenido de la publicacion',
  fecha_publicacion DATETIME DEFAULT NULL COMMENT 'Fecha de la publicacion',
  borrado_publicacion BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario ha eliminado la publicacion'

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- comentario
--
CREATE TABLE comentario (
  codigo_comentario int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'Código auto incremental del comentario',

  codigo_publicacion int(11) NOT NULL COMMENT 'Código de la publicación en la que se realiza el comentario',
  FOREIGN KEY (codigo_publicacion) REFERENCES publicacion(codigo_publicacion) ON DELETE CASCADE,

  nick_emisor VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que realiza el comentario',
  FOREIGN KEY (nick_emisor) REFERENCES usuario(nick) ON DELETE NO ACTION,

  codigo_comentario_padre int(11) DEFAULT NULL COMMENT 'Codigo inicial de todo el hilo',
  FOREIGN KEY (codigo_comentario_padre) REFERENCES comentario(codigo_comentario) ON DELETE SET NULL,

  codigo_comentario_anterior int(11) DEFAULT NULL COMMENT 'Codigo del comentario inmediatamente anterior',
  FOREIGN KEY (codigo_comentario_anterior) REFERENCES comentario(codigo_comentario) ON DELETE SET NULL,

  comentario VARCHAR(255) DEFAULT NULL COMMENT 'Contenido del comentario',
  fecha_comentario DATETIME DEFAULT NULL COMMENT 'Fecha en que se realizó ',
  borrado_comentario BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario ha eliminado el comentario'  

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


--
-- documento
--
CREATE TABLE documento (
  codigo_documento int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT COMMENT 'Código auto incremental del documento',

  codigo_publicacion int(11) DEFAULT NULL COMMENT 'Código de la publicación en la que se encuentra el documento',
  FOREIGN KEY (codigo_publicacion) REFERENCES publicacion(codigo_publicacion) ON DELETE NO ACTION,

  nick_propietario VARCHAR(50) NOT NULL COMMENT 'Nick del usuario que sube el documento',
  FOREIGN KEY (nick_propietario) REFERENCES usuario(nick) ON DELETE CASCADE,

  localizacion VARCHAR(255) DEFAULT NULL COMMENT 'Localización del documento en el servidor',
  fecha_subida_documento DATETIME DEFAULT NULL COMMENT 'Fecha en la que se subio el documento',
  borrado_documento BOOLEAN DEFAULT 0 COMMENT 'Valor booleano que es 1 si el usuario ha eliminado el documento'  

) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


