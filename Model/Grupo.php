<?php

require_once 'Usuarios.php';

/**
    *Modelo Grupo donde se recuperara toda la informacion relacionada con los mismos.
    *
    * @author: Dario Mendez Martinez
*/
class Grupo {
    
    private $nombre_grupo;
    private $descripcion_grupo;
    private $fecha_creacion_grupo;
    private $imagen_grupo;
    private $limite_usuarios_grupo;
    private $borrado_grupo;
    private $tipo_grupo;
    private $nick_creador;

  function __construct($nombre_grupo=NULL,$descripcion_grupo=NULL,$fecha_creacion_grupo=NULL,$imagen_grupo=NULL,$limite_usuarios_grupo=NULL,$borrado_grupo=NULL,$tipo_grupo=NULL,$nick_creador=NULL)
  {
    $this->nombre_grupo=$nombre_grupo;
    $this->descripcion_grupo=$descripcion_grupo;
    $this->fecha_creacion_grupo=$fecha_creacion_grupo;
    $this->imagen_grupo=$imagen_grupo;
    $this->limite_usuarios_grupo=$limite_usuarios_grupo;
    $this->borrado_grupo=$borrado_grupo;
    $this->tipo_grupo=$tipo_grupo;
    $this->nick_creador=$nick_creador;
    require_once 'ConectarBD.php';

  }

  //*************************************** INICIO CRUD GRUPO ****************************************\

    /**
        * @Function CrearGrupo(): Crea un grupo en base de datos
        * @param $insertarGrupo: Variable que almacena la query para la inserción en la tabla grupo.
        * @param $insertarUsuarios: Variable que almacena la query para la inserción en la tabla usuario_pertenece_grupo.
        * @param $resultadoInsertarGrupo: Ejecuta la query de $insertarGrupo.
        * @param $resultadoInsertarUsuarios: Ejecuta la query de $insertarUsuarios.
    */
    public function CrearGrupo() {
        $insertarGrupo  = "INSERT INTO grupo(nombre_grupo,descripcion_grupo, fecha_creacion_grupo, imagen_grupo, limite_usuarios_grupo,borrado_grupo,tipo_grupo,nick_creador) 
                           VALUES ('$this->nombre_grupo','$this->descripcion_grupo',NOW(),'$this->imagen_grupo', '$this->limite_usuarios_grupo', '$this->borrado_grupo', '$this->tipo_grupo','$this->nick_creador')";
        $insertarUsuarios  = "INSERT INTO usuario_pertenece_grupo (nombre_grupo_pertenece,nick_perteneciente_grupo,fecha_ingreso_usuario,tipo_usuario) 
                              VALUES ('$this->nombre_grupo','$this->nick_creador',NOW(),'admin')";
        $resultadoInsertarGrupo = mysqli_query($this->enlace, $insertarGrupo) or die(mysqli_error($this->enlace));
        $resultadoInsertarUsuarios = mysqli_query($this->enlace, $insertarUsuarios) or die(mysqli_error($this->enlace));
    }

    /**
        * @Function checkNombreGrupo(): @Function que comprueba si el nombre del grupo ya existe.
        * @param $_SESSION["grupo_existe"]: Variable que se pone a true si el grupo ya existe.
        * @param $sql: Query para enviar a la base de datos.
        * @param $resultado: Ejecución de la query de $sql.
        * @param $rowCheckNombreGrupo: Variable auxiliar para ver si se recupero algún dato de la base de datos.
     */
    public function checkNombreGrupo(){

        $_SESSION["grupo_existe"]=false;

        $sql = "SELECT * FROM grupo WHERE nombre_grupo='$this->nombre_grupo'";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckNombreGrupo=mysqli_fetch_assoc ($resultado);

        if($rowCheckNombreGrupo){
            $_SESSION["grupo_existe"]=true;
        }
    }

    /**
        * @Function listarGrupos(): @Function para listar todos los grupos mientras no estén eliminados.
        * @param $_SESSION["tipo"]: Variable de sesión que contiene el tipo de usuario
        * @param $sql: Variable donde se almacena la query a enviar a la base de datos.
        * @param $row: Variable auxiliar para capturar la respuesta de la base de datos.
        * @param $sql2: Variable auxiliar para almacenar solamente los datos del grupo.
        * @param $_SESSION["listarGrupos"]: Variable de sesión para trabajar con los grupos capturados.
    */
    public function listarGrupos(){

        if($_SESSION["tipo"]=="user"){
            $sql = mysqli_query($this->enlace, "SELECT * FROM grupo  WHERE borrado_grupo=0") or die (mysqli_error($this->enlace));
        }else{
            $sql = mysqli_query($this->enlace, "SELECT * FROM grupo") or die (mysqli_error($this->enlace));
        }

        $sql2 = array();

        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }
        $_SESSION["listarGrupos"] = $sql2;

    }

    /**
        * @Function filtrarGrupos(): Filtra los grupos que de los cuales el usuario ha hecho una petición.
        * @param $sql: Variable que almacena la query que se enviará a la base de datos.
        * @param $sqlresponse: Variable que almacena la respuesta de la base de datos.
        * @param $grupos: Variable que se usará para ir parseando la respuesta de la base de datos.
        * @param $_SESSION["listarGrupos"]: Variable de sesión para mostrar los grupos filtrados en la vista.
    */
    public function filtrarGrupos(){
        if($_SESSION["tipo"]!="admin"){
            $sql= "SELECT * FROM grupo  where borrado_grupo=0";
        }else{
            $sql="SELECT * FROM grupo WHERE 1";
        }

        if($this->nombre_grupo!=""){
            $sql.=" AND nombre_grupo='$this->nombre_grupo' ";
        }
        if($this->nick_creador!=""){
            $sql.=" AND nick_creador='$this->nick_creador' ";
        }

        $sqlresponse=mysqli_query($this->enlace, $sql) or die (mysqli_error($this->enlace));

        $grupos = array();
        while($row = mysqli_fetch_array($sqlresponse)){
            array_push($grupos, $row);
        }

        $_SESSION["listarGrupos"] = $grupos;
    }

    /**
        * @Function eliminarGrupo(): Elimina lógicamente un grupo.
        * @param $sql: Variable que almacena la respuesta de la base de datos.
    */
    public function eliminarGrupo(){
        $sql= mysqli_query($this->enlace,"UPDATE grupo SET borrado_grupo='1' WHERE nombre_grupo='$this->nombre_grupo'") or die (mysqli_error($this->enlace));
        $sql= mysqli_query($this->enlace,"UPDATE usuario_pertenece_grupo SET borrado_usuario_grupo='1' WHERE nombre_grupo_pertenece='$this->nombre_grupo'") or die (mysqli_error($this->enlace));
    }

    /**
         * @Function misGrupos(): @Function para listar todos los grupos mientras no estén eliminados.
         * @param $_SESSION["tipo"]: Variable de sesión que contiene el tipo de usuario
         * @param $sql: Variable donde se almacena la query a enviar a la base de datos.
         * @param $row: Variable auxiliar para capturar la respuesta de la base de datos.
         * @param $sql2: Variable auxiliar para almacenar solamente los datos del grupo.
         * @param $_SESSION["listarGrupos"]: Variable de sesión para trabajar con los grupos capturados.
    */
    public function misGrupos(){
        $nick=$_SESSION['nick'];

        if(!$this->enlace){
            $this->enlace = mysqli_connect("localhost", "social", "social", "social");
        }else{
            $this->enlace=$this->enlace;
        }

        $sql=mysqli_query($this->enlace, "SELECT * FROM grupo g JOIN usuario_pertenece_grupo u WHERE borrado_grupo=0 AND g.nombre_grupo=u.nombre_grupo_pertenece AND u.nick_perteneciente_grupo='$nick'") or die (mysqli_error($this->enlace));

        $sql2 = array();

        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }
        $_SESSION["listarGrupos"] = $sql2;

    }

    /**
     * @Function consultarGrupo(): @Function para listar todos los grupos mientras no estén eliminados.
     * @param $_SESSION["tipo"]: Variable de sesión que contiene el tipo de usuario
     * @param $sql: Variable donde se almacena la query a enviar a la base de datos.
     * @param $row: Variable auxiliar para capturar la respuesta de la base de datos.
     * @param $sql2: Variable auxiliar para almacenar solamente los datos del grupo.
     * @param $_SESSION["listarGrupos"]: Variable de sesión para trabajar con los grupos capturados.
     */
    public function consultarGrupo(){
        $sql = mysqli_query($this->enlace,"SELECT * FROM grupo WHERE nombre_grupo='$this->nombre_grupo'") or die (mysqli_error($this->enlace));
        $sqlUsers = mysqli_query($this->enlace,"SELECT U.nick,U.foto FROM grupo g INNER JOIN usuario_pertenece_grupo u INNER JOIN usuario U WHERE borrado_grupo=0 AND g.nombre_grupo=u.nombre_grupo_pertenece AND u.nick_perteneciente_grupo=U.nick AND g.nombre_grupo='$this->nombre_grupo'") or die (mysqli_error($this->enlace));

        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }
        $sqlUsers2 = array();
        while($row = mysqli_fetch_array($sqlUsers)){
            array_push($sqlUsers2, $row);
        }
        $_SESSION["ConsultarGrupo"] = $sql2;
        $_SESSION["ConsultarUsuariosGrupo"] = $sqlUsers2;
    }

    /**
     * @Function ModificarGrupo: El usuario se modifica a si mismo o el administrador modifica un grupo.
     * @param $sql: Variable para almacenar la respuesta de la base de datos si hiciese falta.
     * @param $update: Cadena de caracteres que se va rellenando según los datos que el usuario o administrador quieran modificar.
     */
    public function modificarGrupo(){
        $update="";

        if($this->limite_usuarios_grupo!=0){
            $update.="limite_usuarios_grupo='".$this->limite_usuarios_grupo."',";
        }

        if($this->tipo_grupo!=""){
            $update.="tipo_grupo=".$this->tipo_grupo.",";
        }

        if($this->descripcion_grupo!=""){
            $update.="descripcion_grupo='".$this->descripcion_grupo."',";
        }

        if($this->imagen_grupo!="../" && $this->imagen_grupo!="../../Recursos/img/comun.png"){
            $update.="imagen_grupo='".$this->imagen_grupo."',";
        }

        $update = substr($update, 0, -1);


        $sql= mysqli_query($this->enlace,"UPDATE grupo SET ".$update." WHERE nombre_grupo='$this->nombre_grupo'") or die (mysqli_error($this->enlace));
    }
    //*************************************** FIN CRUD GRUPO ***************************************\

    /**
        * @Function checkPertenece(): Comprueba que el usuario que se quiere unir al grupo aún no me pertenece.
        * @param $_SESSION["pertenece"]:
        * @param $nick=$_SESSION["nick"]:
        * @param $sql:
        * @param $resultado:
        * @param $rowCheckPertenece:
        * @param $_SESSION["pertenece"]
    */
    public function checkPertenece(){
        $_SESSION["pertenece"]=false;
        $nick=$_SESSION["nick"];

        $sql = "SELECT * FROM usuario_pertenece_grupo WHERE nombre_grupo_pertenece='$this->nombre_grupo' AND nick_perteneciente_grupo='$nick'";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckPertenece=mysqli_fetch_assoc ($resultado);

        if($rowCheckPertenece){
            $_SESSION["pertenece"]=true;
        }
    }
    /**
        * @Function accederGrupo(): Inserta un usuario en la tabla usuario_pertenece_grupo.
        * $insertarUsuarioGrupo:
        * $resultado:
    */
    public function accederGrupo(){

        $nick=$_SESSION["nick"];
        $insertarUsuarioGrupo  = "INSERT INTO usuario_pertenece_grupo (nombre_grupo_pertenece,nick_perteneciente_grupo,fecha_ingreso_usuario) VALUES ('$this->nombre_grupo','$nick',NOW())";
        $resultado = mysqli_query($this->enlace, $insertarUsuarioGrupo) or die(mysqli_error($this->enlace));
    }

    /**
        * @Function checkSolicitud(): Comprueba que la solicitud no exista
    */
    public function checkSolicitud(){
        $_SESSION["peticion"]=false;
        $nick=$_SESSION["nick"];

        $sql = "SELECT * FROM usuario_invitacion_grupo WHERE nombre_grupo_invitacion='$this->nombre_grupo' AND nick_emisor='$nick'";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckPeticion=mysqli_fetch_assoc ($resultado);

        if($rowCheckPeticion){
            $_SESSION["peticion"]=true;
        }
    }

    /**
        * @Function solicitudAccesoGrupo(): Funcion que crea una solicitud para entrar a un grupo en particular
    */
    public function solicitudAccesoGrupo(){
        $nick=$_SESSION["nick"];
        $insertarUsuariosGrupo  = "INSERT INTO usuario_invitacion_grupo (nombre_grupo_invitacion,nick_emisor,nick_receptor,fecha_invitacion_grupo) VALUES ('$this->nombre_grupo','$nick','$this->nick_creador', NOW())";
        $resultado = mysqli_query($this->enlace, $insertarUsuariosGrupo) or die(mysqli_error($this->enlace));
    }

    /**
        * @Function mostrarSolicitudes(): Muestra las solicitudes que tiene un usuario sobre los grupos
    */
    public function mostrarSolicitudes(){
        $nick=$_SESSION["nick"];
        $sql= mysqli_query($this->enlace, "SELECT * FROM  usuario_invitacion_grupo WHERE nick_receptor='$nick' AND fecha_aceptacion_grupo IS NULL ") or die (mysqli_error($this->enlace));

        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){array_push($sql2, $row);}
        $_SESSION["listarSolicitudesRecibidas"] = $sql2;

        if($_SESSION["tipo"]=="user"){
            $sqlEmitidas= mysqli_query($this->enlace, "SELECT * FROM  usuario_invitacion_grupo WHERE nick_emisor='$nick' AND fecha_aceptacion_grupo IS NULL ") or die (mysqli_error($this->enlace));

            $sqlEmitidasArray = array();
            while($row2 = mysqli_fetch_array($sqlEmitidas)){array_push($sqlEmitidasArray, $row2);}
            $_SESSION["listarSolicitudesEmitidas"] = $sqlEmitidasArray;
        }
    }

    /**
        * @Function checkSolicitudAmigoGrupo($nombre_grupo,$nick_emisor,$nombre_amigo):
    */
    public function checkSolicitudAmigoGrupo($nombre_grupo,$nick_emisor,$nombre_amigo){
        $checkInvitacion=false;

        $sql = "SELECT * FROM usuario_invitacion_grupo WHERE nombre_grupo_invitacion='$nombre_grupo' AND ( (nick_emisor='$nick_emisor' OR nick_receptor='$nick_emisor') AND ((nick_emisor='$nombre_amigo' OR nick_receptor='$nombre_amigo'))) ";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckPeticion=mysqli_fetch_assoc ($resultado);

        if($rowCheckPeticion){
            $checkInvitacion=true;
        }

        return $checkInvitacion;
    }
    /**
        * @Function enviarSolicitudAmigo($nombre_grupo,$nick_emisor,$nombre_amigo)
    */
    public function enviarSolicitudAmigo($nombre_grupo,$nick_emisor,$nombre_amigo){
        $insertarUsuariosGrupo  = "INSERT INTO usuario_invitacion_grupo (nombre_grupo_invitacion,nick_emisor,nick_receptor,fecha_invitacion_grupo) VALUES ('$nombre_grupo','$nick_emisor','$nombre_amigo', NOW())";
        $resultado = mysqli_query($this->enlace, $insertarUsuariosGrupo) or die(mysqli_error($this->enlace));
    }


    /**
         * @Function aceptarUsuarioGrupo($nombre_grupo,$nick_emisor,$nick_receptor): Acepta a un usuario que haya hecho una petición para entrar en un grupo.
         * @param $nombre_grupo
         * @param $nick_emisor
         * @param $nick_receptor
    */
    public function aceptarUsuarioGrupo($nombre_grupo,$nick_emisor,$nick_receptor){

        $borrarPeticionGrupo  = "UPDATE usuario_invitacion_grupo SET fecha_aceptacion_grupo=NOW() WHERE nick_receptor='$nick_receptor' AND nick_emisor='$nick_emisor' AND nombre_grupo_invitacion='$nombre_grupo' ";
        $insertarUsuarios  = "INSERT INTO usuario_pertenece_grupo (nombre_grupo_pertenece,nick_perteneciente_grupo,fecha_ingreso_usuario) VALUES ('$nombre_grupo','$nick_receptor',NOW())";
        $resultado = mysqli_query($this->enlace, $borrarPeticionGrupo) or die(mysqli_error($this->enlace));
        $resultadopertenece = mysqli_query($this->enlace, $insertarUsuarios) or die(mysqli_error($this->enlace));

    }

    /**
         * @Function rechazarUsuarioGrupo($nombre_grupo,$nick_emisor,$nick_receptor): ELimina de la base de datos una petición de entrada en un grupo.
         * @param $nombre_grupo
         * @param $nick_emisor
         * @param $nick_receptor
     */
    public function rechazarUsuarioGrupo($nombre_grupo,$nick_emisor,$nick_receptor){
        $borrarPeticionGrupo  = "DELETE FROM usuario_invitacion_grupo WHERE nick_receptor='$nick_receptor' AND nick_emisor='$nick_emisor' AND nombre_grupo_invitacion='$nombre_grupo' ";
        $resultado = mysqli_query($this->enlace, $borrarPeticionGrupo) or die(mysqli_error($this->enlace));
    }
    
}
?>