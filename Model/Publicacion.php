<?php
require_once 'Usuarios.php';
require_once 'Grupo.php';
require_once 'Evento.php';

/**
    *Modelo Publicacion donde se recuperara toda la informacion relacionada con las mismas.
    *
    * @author: Dario Mendez Martinez
*/

class Publicacion {

    private $nick_propietario;
    private $nombre_grupo_publicacion;
    private $nombre_evento_publicacion;
    private $contenido;
    private $fecha_publicacion;
    private $borrado_publicacion;
  

  function __construct($nick_propietario=NULL,$nombre_grupo_publicacion=NULL,$nombre_evento_publicacion=NULL,$contenido=NULL,$fecha_publicacion=NULL,$borrado_publicacion=NULL)
  {
    
    $this->nick_propietario=$nick_propietario;
    $this->nombre_grupo_publicacion=$nombre_grupo_publicacion;
    $this->nombre_evento_publicacion=$nombre_evento_publicacion;
    $this->contenido=$contenido;
    $this->fecha_publicacion=$fecha_publicacion;
    $this->borrado_publicacion=$borrado_publicacion;
    require_once 'ConectarBD.php';
    
  }

  //***************************** INICIO CRUD PUBLICACION *****************************\\

    /**
        * @Function crearPublicacion($urlDocumentos): Inserta una publicación en base de datos y los documentos si hiciese falta.
        * @param $urlDocumentos
     */
    public function crearPublicacion($urlDocumentos) {
            if($this->nombre_grupo_publicacion==""){
                $this->nombre_grupo_publicacion=null;
            }
            if($this->nombre_evento_publicacion==""){
                $this->nombre_evento_publicacion=null;
            }

            mysqli_query($this->enlace, "SET FOREIGN_KEY_CHECKS = 0") or die(mysqli_error($this->enlace));

            $crearPublicacion  = "INSERT INTO publicacion(nick_propietario,nombre_grupo_publicacion,nombre_evento_publicacion,contenido,fecha_publicacion) VALUES ('$this->nick_propietario','$this->nombre_grupo_publicacion', '$this->nombre_evento_publicacion','$this->contenido',NOW())";
            $resultado = mysqli_query($this->enlace, $crearPublicacion) or die(mysqli_error($this->enlace));
            $idPublicacion = mysqli_insert_id($this->enlace);

            mysqli_query($this->enlace, "SET FOREIGN_KEY_CHECKS = 1") or die(mysqli_error($this->enlace));

            if(COUNT($urlDocumentos)>0){
                foreach ($urlDocumentos as $data){
                    $localizacion='../'.$data;
                    mysqli_query($this->enlace, "INSERT INTO documento(codigo_publicacion,nick_propietario,localizacion,fecha_subida_documento) VALUES ($idPublicacion,'$this->nick_propietario','$localizacion',NOW())") or die(mysqli_error($this->enlace));
                }
            }

        }

    /**
     *
     */
    public function misPublicaciones($nick){

        if(!$this->enlace){
            $this->enlace = mysqli_connect("localhost", "social", "social", "social");
        }else{
            $this->enlace=$this->enlace;
        }

        if(!$this->nick_propietario && $nick){
            $flag=true;
            $this->nick_propietario=$nick;
        }else{
            $this->nick_propietario=$_SESSION["nick"];
        }

        if($_SESSION["tipo"]=="user"){
            $sql= mysqli_query($this->enlace, "SELECT * FROM publicacion  where borrado_publicacion=0 AND nick_propietario='$this->nick_propietario'") or die (mysqli_error($this->enlace));
        }else{
            if($flag){
                $sql= mysqli_query($this->enlace, "SELECT * FROM publicacion  where borrado_publicacion=0 AND nick_propietario='$this->nick_propietario'") or die (mysqli_error($this->enlace));
            }else{
                $sql = mysqli_query($this->enlace, "SELECT * FROM publicacion  where borrado_publicacion=0") or die (mysqli_error($this->enlace));
            }
        }
        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }

        $_SESSION["listarPublicaciones"] = $sql2;
    }

    /**
     * @param
     */
    public function publicacionesGrupoEvento($nombre_grupo,$nombre_evento){

        if(!$this->enlace){
            $this->enlace = mysqli_connect("localhost", "social", "social", "social");
        }else{
            $this->enlace=$this->enlace;
        }

        if($nombre_grupo){
            $sql= mysqli_query($this->enlace, "SELECT * FROM publicacion  where borrado_publicacion=0 AND nombre_grupo_publicacion='$nombre_grupo'") or die (mysqli_error($this->enlace));
        }else if($nombre_evento){
            $sql= mysqli_query($this->enlace, "SELECT * FROM publicacion  where borrado_publicacion=0 AND nombre_evento_publicacion='$nombre_evento'") or die (mysqli_error($this->enlace));
        }

        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }

        $_SESSION["listarPublicaciones"] = $sql2;

    }

    /**
        * @Function eliminarPublicacion($codigo_publicacion)
        * @param $codigo_publicacion
     */
    public function eliminarPublicacion($codigo_publicacion){
        $sql= mysqli_query($this->enlace,"UPDATE publicacion SET borrado_publicacion='1' WHERE codigo_publicacion='$codigo_publicacion'") or die (mysqli_error($this->enlace));

    }

    /**
        * @param $_POST
     */
    public function verPublicacion($codigo_publicacion){
        $publicacion= mysqli_query($this->enlace, "SELECT * FROM publicacion  where borrado_publicacion=0 AND codigo_publicacion=$codigo_publicacion") or die (mysqli_error($this->enlace));
        $comentario = mysqli_query($this->enlace, "SELECT * FROM comentario  where borrado_comentario=0 AND codigo_publicacion=$codigo_publicacion") or die (mysqli_error($this->enlace));
        $comentario_hijo = mysqli_query($this->enlace, "SELECT * FROM comentario  where borrado_comentario=0 AND codigo_publicacion=$codigo_publicacion AND codigo_comentario_padre IS NOT NULL") or die (mysqli_error($this->enlace));

        $arrayPublicacion = array();
        while($row = mysqli_fetch_array($publicacion)){
            array_push($arrayPublicacion, $row);
        }
        $_SESSION["consultarPublicacion"] = $arrayPublicacion;

        $arrayComentarioPadre = array();
        while($row = mysqli_fetch_array($comentario)){
            array_push($arrayComentarioPadre, $row);
        }
        $_SESSION["comentarios_padre"] = $arrayComentarioPadre;

        $arrayComentarioHijo = array();
        while($row = mysqli_fetch_array($comentario_hijo)){
            array_push($arrayComentarioHijo, $row);
        }
        $_SESSION["comentarios_hijo"] = $arrayComentarioHijo;

    }

    /**
     *
     */
    public function modificarPublicacion($urlDocumentos,$codigo_publicacion)
    {
        $update = "";
        $localizacion="";
        $this->nick_propietario=$_SESSION['nick'];

        if ($this->nombre_grupo_publicacion != "") {
            $update .= "nombre_grupo_publicacion='" . $this->nombre_grupo_publicacion . "',";
        }

        if ($this->nombre_evento_publicacion != "") {
            $update .= "nombre_evento_publicacion='" . $this->nombre_evento_publicacion . "',";
        }
        if ($this->contenido != "") {
            $update .= "contenido='" . $this->contenido . "' ,";
        }

        $update = substr($update, 0, -1);

        $sql= mysqli_query($this->enlace,"UPDATE publicacion SET ".$update." WHERE codigo_publicacion=$codigo_publicacion") or die (mysqli_error($this->enlace));

        if(COUNT($urlDocumentos)>0){
            foreach ($urlDocumentos as $data){
                $localizacion='../'.$data;
                mysqli_query($this->enlace, "INSERT INTO documento(codigo_publicacion,nick_propietario,localizacion,fecha_subida_documento) VALUES ($codigo_publicacion,'$this->nick_propietario','$localizacion',NOW())") or die(mysqli_error($this->enlace));
            }
        }
    }
   //***************************** INICIO CRUD PUBLICACION *****************************\\

    /**
     * @param $codigo_publicacion
     * @param $nick_emisor
     * @param $comentario
     * @param $codigo_comentario_padre
     */

    public function comentar($codigo_publicacion,$nick_emisor,$comentario,$codigo_comentario_padre){

        if(!$codigo_comentario_padre) {
            mysqli_query($this->enlace, "INSERT INTO comentario(codigo_publicacion,nick_emisor,comentario,fecha_comentario) VALUES ($codigo_publicacion,'$nick_emisor','$comentario',NOW())") or die(mysqli_error($this->enlace));
        }else{
            mysqli_query($this->enlace, "INSERT INTO comentario(codigo_publicacion,nick_emisor,comentario,codigo_comentario_padre,fecha_comentario) VALUES ($codigo_publicacion,'$nick_emisor','$comentario',$codigo_comentario_padre,NOW())") or die(mysqli_error($this->enlace));
        }
    }

    /**
     * @param $codigo_comentario
     */
    public function eliminarComentario($codigo_comentario){
        $sql= mysqli_query($this->enlace,"UPDATE comentario SET borrado_comentario='1' WHERE codigo_comentario='$codigo_comentario'") or die (mysqli_error($this->enlace));
        $sql2= mysqli_query($this->enlace,"UPDATE comentario SET borrado_comentario='1' WHERE codigo_comentario_padre='$codigo_comentario'") or die (mysqli_error($this->enlace));
    }
}
?>