<?php

session_start();


/**
    *Modelo Usuarios donde se recuperara toda la informacion relacionada con los mismos.
    *
    * @author: Dario Mendez Martinez
*/
class Usuarios {
    
    private $nick;
    private $password;
    private $email;
    private $nombre_usuario;
    private $apellido_usuario;
    private $telefono;
    private $borrado_usuario;
    private $cargo;
    private $foto;
    private $descripcion_usuario;
    private $tipo_usuario;
    private $fecha_registro_usuario;

  //*******************  @Function constructor y getters and setters necesarios  *******************\\
  function __construct($nick=NULL,$password=NULL,$email=NULL,$nombre_usuario=NULL,$apellido_usuario=NULL,$telefono=NULL,$borrado_usuario=NULL,$cargo=NULL,$foto=NULL,$descripcion_usuario=NULL,$tipo_usuario=NULL,$fecha_registro_usuario=NULL)
  {
    $this->nick=$nick;
    $this->password=$password;
    $this->email=$email;
    $this->nombre_usuario=$nombre_usuario;
    $this->apellido_usuario=$apellido_usuario;
    $this->telefono=$telefono;
    $this->borrado_usuario=$borrado_usuario;
    $this->cargo=$cargo;
    $this->foto=$foto;
    $this->descripcion_usuario=$descripcion_usuario;
    $this->tipo_usuario=$tipo_usuario;
    $this->fecha_registro_usuario=$fecha_registro_usuario;
    require_once 'ConectarBD.php';
    
  }

  public function setNick($nick){
      $this->nick=$nick;
  }
  //*******************  @Function constructor y getters and setters necesarios  *******************\\
    
    /**
        ** Función para comprobar si un usuario puede hacer o no login.
        *
        * @param $consultarUsuario: Variable para almacenar la query y posteriormente la respuesta de la base de datos.
        * @param $row: Almacena las tuplas de la base de datos.
        * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo de usuario para distinguir su rol.
        * @param $_SESSION["nick"]: Variable de sesión que almacena el nick del usuario para su uso en la plataforma.
        * @param $_SESSION["foto"]: Variable con la url de la imagen de usuario en el servidor.
    */
    public function login(){
        $consultarUsuario = "SELECT * FROM usuario WHERE nick = '$this->nick' AND borrado_usuario='0' ";
        $consultarUsuario = mysqli_query($this->enlace , $consultarUsuario) or die (mysqli_error($this->enlace));
            if (isset($consultarUsuario)){
              $row=mysqli_fetch_assoc ($consultarUsuario);
                if(password_verify($this->password,$row["password"])=="true"){
                    $_SESSION["tipo"] = $row['tipo_usuario'] ;
                    $_SESSION["nick"] = $row['nick'];
                    $_SESSION["foto"] = $row['foto'];
                }
            }
    }

    //*********************************** INICIO CRUD USUARIO ***********************************\

    /**
        **Función para comprobar que un usuario no ha cerrado su cuenta y comprobar que tanto el mail como el nick no existen.
        *
        * @param $consultarUsuario: Variable para almacenar la query y posteriormente la respuesta de la base de datos.
        * @param $row: Almacena las tuplas de la base de datos.
        * @param $_SESSION['borradoUsuario']: Variable de sesión para saber si un usuario ya existe en la base de datos y ha sido borrado lógicamente.
        * @param $_SESSION['error_nick']: Variable de sesión que devuelve true si el nick ya existe.
        * @param $_SESSION['error_mail']: Variable de sesión que devuelve true si el email que ha introducido el usuario ya existe.
        * @param $checkBorradoUsuario: Almacena la query y la respuesta de la base de datos (todas las variable $check son análogas).
        * @param $rowCheckBorradoUsuario: Almacena las tuplas de la respuesta de la base de datos(todas las variable $row son análogas).
    */
    public function checkNickAndMail(){

        $_SESSION['borradoUsuario']=false;
        $_SESSION['error_nick']=false;
        $_SESSION['error_mail']=false;

        $checkBorradoUsuario = "SELECT * FROM usuario WHERE (nick = '$this->nick' OR email= '$this->email') AND borrado_usuario='1' ";
        $checkBorradoUsuario = mysqli_query($this->enlace , $checkBorradoUsuario) or die (mysqli_error($this->enlace));
        $rowCheckBorradoUsuario=mysqli_fetch_assoc ($checkBorradoUsuario);

        if($rowCheckBorradoUsuario){
            $_SESSION['borradoUsuario']=true;
        }


        $checkNickUsuario = "SELECT * FROM usuario WHERE nick = '$this->nick' AND borrado_usuario='0' ";
        $checkNickUsuario = mysqli_query($this->enlace , $checkNickUsuario) or die (mysqli_error($this->enlace));
        $rowcheckNickUsuario=mysqli_fetch_assoc ($checkNickUsuario);

        if($rowcheckNickUsuario){
            $_SESSION['error_nick']=true;
        }        

        $checkMailUsuario = "SELECT * FROM usuario WHERE  email= '$this->email' AND borrado_usuario='0' ";
        $checkMailUsuario = mysqli_query($this->enlace , $checkMailUsuario) or die (mysqli_error($this->enlace));
        $rowcheckMailUsuario=mysqli_fetch_assoc ($checkMailUsuario);

        if($rowcheckMailUsuario){
            $_SESSION['error_mail']=true;
        }

    }
    

    /**
        ** Función que registra a un usuario en la aplicación.
        *
        * @param $insertarUsuario: Query para la inserción de un usuario.
        * @param $resultado: Resultado de la inserción.
    */
    public function altaUsuario() {
        $insertarUsuario  = "INSERT INTO usuario(nick,password,email,nombre_usuario,apellido_usuario,telefono,cargo,descripcion_usuario,fecha_registro_usuario,foto)VALUES ('$this->nick','$this->password', '$this->email', '$this->nombre_usuario', '$this->apellido_usuario','$this->telefono','$this->cargo','$this->descripcion_usuario',NOW(),'$this->foto')";
        $resultado = mysqli_query($this->enlace, $insertarUsuario) or die(mysqli_error($this->enlace));
    }
    
    /**
        *Función que recupera todos los usuario de la base de datos.
        *
        * @param $sql: Query para recuperar los usuarios.
        * @param $users: Array donde se introduciran todos los usuarios despues de un fetch.
        * @param $row: Variable donde se introducen los datos de la base de datos.
        * @param $_SESSION["listarUsuarios"]: Variable de sesión que se utilizará en las dos vistas(usuario y administrador) para mostrar los usuarios. 
    */
    public function listarUsuarios(){
        
        if($_SESSION["tipo"]!="admin"){
            $sql= mysqli_query($this->enlace, "SELECT * FROM usuario  where borrado_usuario=0") or die (mysqli_error($this->enlace));
        }else{
            $sql= mysqli_query($this->enlace, "SELECT * FROM usuario") or die (mysqli_error($this->enlace));
        }

        $users = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($users, $row);
        }
        
        $_SESSION["listarUsuarios"] = $users;

    }
        
    /**
        *Función que filtra a los usuarios según los parametros que se hayan introducido.
        *
        * @param $sql: Query para recuperar los usuarios.
        * @param $users: Array donde se introduciran todos los usuarios despues de un fetch.
        * @param $row: Variable donde se introducen los datos de la base de datos.
        * @param $_SESSION["listarUsuarios"]: Variable de sesión que se utilizará en las dos vistas(usuario y administrador) para mostrar los usuarios. 
        * @param $sqlresponse: Variable para almacenar la respuesta de la base de datos a la query realizada.
    */
    public function filtrarUsuarios(){
        
        if($_SESSION["tipo"]!="admin"){
            $sql= "SELECT * FROM usuario  where borrado_usuario=0";
        }else{
            $sql="SELECT * FROM usuario WHERE 1";
        }

        if($this->nick!=""){
            $sql.=" AND nick='$this->nick' ";
        }
        if($this->email!=""){
            $sql.=" AND email='$this->email' ";
        }
        if($this->nombre_usuario!=""){
            $sql.=" AND nombre_usuario='$this->nombre_usuario' ";
        }
        if($this->cargo!=""){
            $sql.=" AND cargo='$this->cargo' ";
        }
        
        $sqlresponse=mysqli_query($this->enlace, $sql) or die (mysqli_error($this->enlace));
        var_dump($sqlresponse);

        $users = array();
        while($row = mysqli_fetch_array($sqlresponse)){
            array_push($users, $row);
        }
        
        $_SESSION["listarUsuarios"] = $users;
    }

    /**
        *Función que recopila todos los datos de un usuario para formar su perfil o función que recopila los datos del usuario cuando se solicita ver usuario.
        *
        * @param $sql: Variable que captura los datos del usuario solicitado.
        * @param $user: Variable auxiliar para guardar los datos de la query devuelta.
        * @param $_SESSION["perfil"]: Variable de sesión que contiene los datos de un usuario.
    */
    public function PerfilUsuario(){
        if($_SESSION["tipo"]!='admin') {
            $sql = mysqli_query($this->enlace, "SELECT * FROM usuario  WHERE nick='$this->nick' AND borrado_usuario='0'") or die (mysqli_error($this->enlace));
        }else{
            $sql = mysqli_query($this->enlace, "SELECT * FROM usuario  WHERE nick='$this->nick'") or die (mysqli_error($this->enlace));
        }

        $user = array();

        while($row = mysqli_fetch_array($sql)) {
            array_push($user, $row);
        }

        $_SESSION["perfil"] = $user;
    }

    /**
        *@Function ModificarUsuario: El usuario se modifica a si mismo o el administrador modifica un usuario.
        * @param $sql: Variable para almacenar la respuesta de la base de datos si hiciese falta.
        * @param $update: Cadena de caracteres que se va rellenando según los datos que el usuario o administrador quieran modificar.
    */
    public function modificarUsuario(){
        $update="";

        if($this->nombre_usuario!=""){
            $update.="nombre_usuario='".$this->nombre_usuario."',";
        }

        if($this->apellido_usuario!=""){
            $update.="apellido_usuario='".$this->apellido_usuario."',";
        }

        if($this->password!=""){
            $update.="password=".$this->password.",";
        }

        if($this->descripcion_usuario!=""){
            $update.="descripcion_usuario='".$this->descripcion_usuario."',";
        }

        if($this->cargo!=""){
            $update.="cargo='".$this->cargo."',";
        }

        if($this->telefono!=""){
            $update.="telefono='".$this->telefono."',";
        }

        if($this->foto!="../" && $this->foto!="../../Recursos/img/comun.png"){
            $update.="foto='".$this->foto."',";
        }

        $update = substr($update, 0, -1);

        $sql= mysqli_query($this->enlace,"UPDATE usuario SET ".$update." WHERE nick='$this->nick'") or die (mysqli_error($this->enlace));
    }

    /**
        *@Function borrarUsuario: Elimina un usuario(administrador) o el usuario se elimina a si mismo.
        * @param $sql: Variable para almacenar la respuesta de la base de datos si hiciese falta.
    */
    public function borrarUsuario(){
        $sql= mysqli_query($this->enlace,"UPDATE usuario SET borrado_usuario='1' WHERE nick='$this->nick' ") or die (mysqli_error($this->enlace));
    }

    /**
        *@Function volverCrearUsuario: El usuario puede volver a dar de alta el usuario.
        * @param $sql: Variable para almacenar la respuesta de la base de datos si hiciese falta.
     */
    public function volverCrearUsuario(){
        $sql= mysqli_query($this->enlace,"UPDATE usuario SET borrado_usuario='0' WHERE nick='$this->nick' ") or die (mysqli_error($this->enlace));
    }

    //*********************************** FIN CRUD USUARIO ***********************************\

    /**
        * @Function para comprobar que esa petición aún no existe.
        * @param $checkPeticionAmistad: Valor para comprobar que se pone a true si la petición existe.
        * @param $nick: Variable que almacena el nick del usuario
        * @param $query: Variable que contiene la query que se lanzará contra la base de datos.
        * @param $check: Petición de la query a la base de datos.
        * @param $rowcheck: Variable usada para saber si la base de datos devuelve algún valor.
        * @return Devuelve true si la petición existe.     
     */
    public function checkPeticionAmistad(){
        $checkPeticionAmistad = false;
        $nick=$_SESSION["nick"];


        $query = "SELECT * FROM amigo WHERE ( nick_peticion_amistad = '$this->nick' OR  nick_receptor_amistad= '$this->nick')  AND ( nick_peticion_amistad = '$nick' OR  nick_receptor_amistad= '$nick') ";
        $check = mysqli_query($this->enlace , $query) or die (mysqli_error($this->enlace));
        $rowcheck=mysqli_fetch_assoc ($check);

        if($rowcheck){
            $checkPeticionAmistad=true;
        }
        return $checkPeticionAmistad;
    }
    /**
        * @Function crearPeticionAmistad(): Crea la petición de amistad entre dos usuarios
        * @param $nickEmisor: Variable que almacena el nick del usuario que emite la peticion.
        * @param $insertarPeticion: Query que se lanzará contra la base de datos.
        * @param $resultado: Variable que almacena el resultado despues de lanzar la query a la base de datos por si nos hiciese falta.   
    */
    public function crearPeticionAmistad(){
        $nickEmisor=$_SESSION["nick"];
        $insertarPeticion  = "INSERT INTO amigo(nick_peticion_amistad, nick_receptor_amistad,fecha_peticion_amistad) VALUES ( '$nickEmisor','$this->nick',NOW())";
        $resultado = mysqli_query($this->enlace, $insertarPeticion) or die(mysqli_error($this->enlace));
    }


    /**
        * @Function listarAmigos(): Lista los amigos de un usuario
        * @param $sql: Variable que almacena el resultado de lanzar la query contra la base de datos.
        * @param $sql2: Variable auxiliar para obtener los valores devueltos de la base de datos.
        * @param $_SESSION["listarAmigos"]: Variable de sesion que almacenará todos los amigos del usuario.
    */
    public function listarAmigos($nick){
        if($nick){
           $this->nick = $nick;
        }
        $sql= mysqli_query($this->enlace, "SELECT * FROM amigo a join usuario u where a.fecha_aceptacion_amistad IS NOT NULL AND (a.nick_receptor_amistad='$this->nick' OR a.nick_peticion_amistad='$this->nick') AND borrado_amigo='0' AND (a.nick_receptor_amistad=u.nick OR a.nick_peticion_amistad=u.nick) AND u.nick<>'$this->nick'") or die (mysqli_error($this->enlace));

        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){array_push($sql2, $row);}
        $_SESSION["listarAmigos"] = $sql2;

    }

    /**
        * @Function borrarAmigo():
        * @param $nickReceptor: Nick del receptor de la peticion de amistad
        * @param $sql: Variable para almacenar la respuesta de la base de datos.
        * @Funtion mysqli_close(): Función que necesité para evitar el error en proximas llamadas a la base de datos.
    */
    public function borrarAmigo(){
        $nickReceptor=$_SESSION["nick"];
        $sql= mysqli_query($this->enlace,"UPDATE amigo SET borrado_amigo='1' WHERE (nick_receptor_amistad='$nickReceptor' AND nick_peticion_amistad='$this->nick') OR (nick_receptor_amistad='$this->nick' AND nick_peticion_amistad='$nickReceptor') ") or die (mysqli_error($this->enlace));
        mysqli_close();
    }

    /**
        * @Function listarSolicitudesAmistad():
        * @param $socitudesRecibidas: Resultado de la base de datos al lanzar la query.
        * @param $socitudesEnviadas: Resultado de la base de datos al lanzar la query.
        * @param $sql: Variable que sirve de auxiliar para recoger los campos del resultado de base de datos.
        * @param $sql2: Variable que sirve de auxiliar para recoger los campos del resultado de base de datos.
        * @param $_SESSION["listarSolicitudesRecibidas"]: Variable de sesión que almacena las solicitudes de amistad recibidas.
        * @param $_SESSION["listarSolicitudesEmitidas"]: Variable de sesión que almacena las solicitudes de amistad emitidas.
    */
    public function listarSolicitudesAmistad(){
        $socitudesRecibidas= mysqli_query($this->enlace, "SELECT * FROM amigo  where fecha_aceptacion_amistad IS NULL AND nick_receptor_amistad='$this->nick'") or die (mysqli_error($this->enlace));
        $socitudesEnviadas= mysqli_query($this->enlace, "SELECT * FROM amigo  where fecha_aceptacion_amistad IS NULL AND nick_peticion_amistad='$this->nick'") or die (mysqli_error($this->enlace));

        $sql = array();
        while($row = mysqli_fetch_array($socitudesRecibidas)){array_push($sql, $row);}
        $_SESSION["listarSolicitudesRecibidas"] = $sql;

        $sql2 = array();
        while($row = mysqli_fetch_array($socitudesEnviadas)){array_push($sql2, $row);}
        $_SESSION["listarSolicitudesEmitidas"] = $sql2;

    }

    /**
        * @Funtion aceptarAmistad($usuario_receptor): Acepta la solicitud de amistad de un usuario.
        * @param $aceptarAmigo: Query que se enviará a la base de datos.
        * @param $resultado: Resultado de la base de datos por si hiciese falta en un futuro.

    */
    public function aceptarAmistad($usuario_receptor){
        $aceptarAmigo  = "UPDATE amigo SET fecha_aceptacion_amistad=NOW() WHERE nick_receptor_amistad='$usuario_receptor' AND nick_peticion_amistad='$this->nick' ";
        $resultado = mysqli_query($this->enlace, $aceptarAmigo) or die(mysqli_error($this->enlace));
    }

    /**
        * @Function rechazarAmistad($user_receptor): Funcion que elimina de la base de datos una petición de amistad realizada por un usuario
        * @param $borrarPeticionAmistad: Query que se enviará a la base de datos.
        * @param $resultado: Resultado de la base de datos por si hiciese falta en un futuro.
    */
    public function rechazarAmistad($usuario_receptor){

        $borrarPeticionAmistad  = "DELETE FROM amigo WHERE nick_receptor_amistad='$usuario_receptor' AND nick_peticion_amistad='$this->nick'";
        $resultado = mysqli_query($this->enlace, $borrarPeticionAmistad) or die(mysqli_error($this->enlace));
    }

    /**
        * @funtion enviarMensaje($nick_emisor,$nick_receptor,$mensaje,$codigo_mensaje_padre)
     */
    public function enviarMensaje($nick_emisor,$nick_receptor,$mensaje,$codigo_mensaje_padre){
        if(!$codigo_mensaje_padre){
            mysqli_query($this->enlace, "INSERT INTO mensaje(nick_emisor,nick_receptor,mensaje,fecha_envio_mensaje) VALUES ('$nick_emisor','$nick_receptor','$mensaje',NOW())") or die(mysqli_error($this->enlace));
        }else{
            var_dump($nick_emisor);
            var_dump($nick_receptor);
            var_dump($mensaje);
            var_dump($codigo_mensaje_padre);
            mysqli_query($this->enlace, "INSERT INTO mensaje(nick_emisor,nick_receptor,mensaje,fecha_envio_mensaje,codigo_mensaje_padre) VALUES ('$nick_emisor','$nick_receptor','$mensaje',NOW(),$codigo_mensaje_padre)") or die(mysqli_error($this->enlace));
        }
    }

    /**
        * @function listarConversacion($nick)
    */
    public function listarConversacion($nick){
        if($_SESSION['tipo']=="user") {
            $conversacion = mysqli_query($this->enlace, "SELECT DISTINCT nick_emisor,nick_receptor FROM mensaje WHERE nick_emisor='$nick' AND borrado_mensaje=0") or die (mysqli_error($this->enlace));
            $conversacionRecibida = mysqli_query($this->enlace, "SELECT DISTINCT nick_emisor,nick_receptor FROM mensaje WHERE nick_receptor='$nick' AND borrado_mensaje=0") or die (mysqli_error($this->enlace));

            $sql = array();
            while ($row = mysqli_fetch_array($conversacion)) {
                array_push($sql, $row);
            }
            $_SESSION["conversacion"] = $sql;

            $sql2 = array();
            while ($row = mysqli_fetch_array($conversacionRecibida)) {
                array_push($sql2, $row);
            }
            $_SESSION["conversacionRecibida"] = $sql2;
        }else{
            $conversacion = mysqli_query($this->enlace, "SELECT DISTINCT nick_emisor,nick_receptor FROM mensaje") or die (mysqli_error($this->enlace));

            $sql = array();
            while ($row = mysqli_fetch_array($conversacion)) {
                array_push($sql, $row);
            }
            $_SESSION["conversacion"] = $sql;
        }
    }

    /**
        * @function eliminarConversacion($nick_emisor,$nick_receptor)
    */
    public function eliminarConversacion($nick_emisor,$nick_receptor){
        mysqli_query($this->enlace,"UPDATE mensaje SET borrado_mensaje='1' WHERE (nick_emisor='$nick_emisor' AND nick_receptor='$nick_receptor') OR (nick_receptor='$nick_emisor' AND nick_emisor='$nick_receptor') ") or die (mysqli_error($this->enlace));
    }

    /**
        *
    */
    public function verConversacion($nick_emisor,$nick_receptor){
        $conversacion = mysqli_query($this->enlace,"SELECT * FROM mensaje WHERE borrado_mensaje=0 AND ((nick_emisor='$nick_emisor' AND nick_receptor='$nick_receptor') OR (nick_receptor='$nick_emisor' AND nick_emisor='$nick_receptor')) ORDER BY fecha_envio_mensaje ASC") or die (mysqli_error($this->enlace));

        $sql = array();
        while ($row = mysqli_fetch_array($conversacion)) {
            array_push($sql, $row);
        }
        $_SESSION["conversacion"] = $sql;
    }


    /**
        * @function eliminarMensaje($codigo_mensaje)
    */
    public function eliminarMensaje($codigo_mensaje){
        mysqli_query($this->enlace,"UPDATE mensaje SET borrado_mensaje='1' WHERE codigo_mensaje=$codigo_mensaje ") or die (mysqli_error($this->enlace));

    }

    /**
        * @function listarDocumentos($nick): Lista los documentos de un usuario o todos los documentos si el usuario es administrador.
        * @param $nick
     */
    public function listarDocumentos($nick){

        if($_SESSION['tipo']=="user") {
            $documento = mysqli_query($this->enlace, "SELECT * FROM documento WHERE nick_propietario='$nick' AND borrado_documento='0' ") or die (mysqli_error($this->enlace));
        }else{
            $documento = mysqli_query($this->enlace, "SELECT * FROM documento") or die (mysqli_error($this->enlace));
        }
        $sql = array();
        while ($row = mysqli_fetch_array($documento)) {
            array_push($sql, $row);
        }
        $_SESSION["documento"] = $sql;
    }

    /**
        * @Function eliminarDocumento($_POST['codigo_documento'])
    */
    public function eliminarDocumento($codigo_documento){
        mysqli_query($this->enlace,"UPDATE documento SET borrado_documento='1' WHERE codigo_documento=$codigo_documento ") or die (mysqli_error($this->enlace));
    }

    /**
        * @Function subirDocumento($_SESSION['nick'],$uploadfile)
    */
    public function subirDocumento($nick_propietario,$localizacion){
        mysqli_query($this->enlace, "INSERT INTO documento(nick_propietario,localizacion,fecha_subida_documento) VALUES ('$nick_propietario','$localizacion',NOW())") or die(mysqli_error($this->enlace));

    }

}
?>