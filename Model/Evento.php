<?php

require_once 'Usuarios.php';

/**
 *Modelo evento donde se recuperara toda la informacion relacionada con los mismos.
 *
 * @author: Dario Mendez Martinez
 */
class evento {

    private $nombre_evento;
    private $descripcion_evento;
    private $fecha_creacion_evento;
    private $imagen_evento;
    private $limite_usuarios_evento;
    private $borrado_evento;
    private $tipo_evento;
    private $nick_creador;

    function __construct($nombre_evento=NULL,$descripcion_evento=NULL,$fecha_creacion_evento=NULL,$imagen_evento=NULL,$limite_usuarios_evento=NULL,$borrado_evento=NULL,$tipo_evento=NULL,$nick_creador=NULL)
    {
        $this->nombre_evento=$nombre_evento;
        $this->descripcion_evento=$descripcion_evento;
        $this->fecha_creacion_evento=$fecha_creacion_evento;
        $this->imagen_evento=$imagen_evento;
        $this->limite_usuarios_evento=$limite_usuarios_evento;
        $this->borrado_evento=$borrado_evento;
        $this->tipo_evento=$tipo_evento;
        $this->nick_creador=$nick_creador;
        require_once 'ConectarBD.php';

    }

    //*************************************** INICIO CRUD evento ****************************************\

    /**
     * Función Crearevento(): Crea un evento en base de datos
     * @param $insertarevento: Variable que almacena la query para la inserción en la tabla evento.
     * @param $insertarUsuarios: Variable que almacena la query para la inserción en la tabla usuario_pertenece_evento.
     * @param $resultadoInsertarevento: Ejecuta la query de $insertarevento.
     * @param $resultadoInsertarUsuarios: Ejecuta la query de $insertarUsuarios.
     */
    public function Crearevento() {
        $tipoEvento = intval($this->tipo_evento);
        $insertarevento  = "INSERT INTO evento(nombre_evento,descripcion_evento, fecha_creacion_evento, imagen_evento, limite_usuarios_evento,borrado_evento,tipo_evento,nick_creador) 
                           VALUES ('$this->nombre_evento','$this->descripcion_evento',NOW(),'$this->imagen_evento', '$this->limite_usuarios_evento', '$this->borrado_evento', '$tipoEvento','$this->nick_creador')";
        $insertarUsuarios  = "INSERT INTO usuario_pertenece_evento (nombre_evento_pertenece,nick_perteneciente_evento,fecha_ingreso_usuario,tipo_usuario) 
                              VALUES ('$this->nombre_evento','$this->nick_creador',NOW(),'admin')";
        $resultadoInsertarevento = mysqli_query($this->enlace, $insertarevento) or die(mysqli_error($this->enlace));
        $resultadoInsertarUsuarios = mysqli_query($this->enlace, $insertarUsuarios) or die(mysqli_error($this->enlace));
    }

    /**
     * Función checkNombreevento(): Función que comprueba si el nombre del evento ya existe.
     * @param $_SESSION["evento_existe"]: Variable que se pone a true si el evento ya existe.
     * @param $sql: Query para enviar a la base de datos.
     * @param $resultado: Ejecución de la query de $sql.
     * @param $rowCheckNombreevento: Variable auxiliar para ver si se recupero algún dato de la base de datos.
     */
    public function checkNombreevento(){

        $_SESSION["evento_existe"]=false;

        $sql = "SELECT * FROM evento WHERE nombre_evento='$this->nombre_evento'";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckNombreevento=mysqli_fetch_assoc ($resultado);

        if($rowCheckNombreevento){
            $_SESSION["evento_existe"]=true;
        }
    }

    /**
     * Función listareventos(): Función para listar todos los eventos mientras no estén eliminados.
     * @param $_SESSION["tipo"]: Variable de sesión que contiene el tipo de usuario
     * @param $sql: Variable donde se almacena la query a enviar a la base de datos.
     * @param $row: Variable auxiliar para capturar la respuesta de la base de datos.
     * @param $sql2: Variable auxiliar para almacenar solamente los datos del evento.
     * @param $_SESSION["listareventos"]: Variable de sesión para trabajar con los eventos capturados.
     */
    public function listareventos(){

        if($_SESSION["tipo"]=="user"){
            $sql = mysqli_query($this->enlace, "SELECT * FROM evento  WHERE borrado_evento=0") or die (mysqli_error($this->enlace));
        }else{
            $sql = mysqli_query($this->enlace, "SELECT * FROM evento") or die (mysqli_error($this->enlace));
        }

        $sql2 = array();

        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }
        $_SESSION["listareventos"] = $sql2;

    }

    /**
     * Función filtrareventos(): Filtra los eventos que de los cuales el usuario ha hecho una petición.
     * @param $sql: Variable que almacena la query que se enviará a la base de datos.
     * @param $sqlresponse: Variable que almacena la respuesta de la base de datos.
     * @param $eventos: Variable que se usará para ir parseando la respuesta de la base de datos.
     * @param $_SESSION["listareventos"]: Variable de sesión para mostrar los eventos filtrados en la vista.
     */
    public function filtrareventos(){
        if($_SESSION["tipo"]!="admin"){
            $sql= "SELECT * FROM evento  where borrado_evento=0";
        }else{
            $sql="SELECT * FROM evento WHERE 1";
        }

        if($this->nombre_evento!=""){
            $sql.=" AND nombre_evento='$this->nombre_evento' ";
        }
        if($this->nick_creador!=""){
            $sql.=" AND nick_creador='$this->nick_creador' ";
        }

        $sqlresponse=mysqli_query($this->enlace, $sql) or die (mysqli_error($this->enlace));

        $eventos = array();
        while($row = mysqli_fetch_array($sqlresponse)){
            array_push($eventos, $row);
        }

        $_SESSION["listareventos"] = $eventos;
    }

    /**
     * Función eliminarevento(): Elimina lógicamente un evento.
     * @param $sql: Variable que almacena la respuesta de la base de datos.
     */
    public function eliminarevento(){
        $sql= mysqli_query($this->enlace,"UPDATE evento SET borrado_evento='1' WHERE nombre_evento='$this->nombre_evento'") or die (mysqli_error($this->enlace));
        $sql= mysqli_query($this->enlace,"UPDATE usuario_pertenece_evento SET borrado_usuario_evento='1' WHERE nombre_evento_pertenece='$this->nombre_evento'") or die (mysqli_error($this->enlace));
    }

    /**
     * Función miseventos(): Función para listar todos los eventos mientras no estén eliminados.
     * @param $_SESSION["tipo"]: Variable de sesión que contiene el tipo de usuario
     * @param $sql: Variable donde se almacena la query a enviar a la base de datos.
     * @param $row: Variable auxiliar para capturar la respuesta de la base de datos.
     * @param $sql2: Variable auxiliar para almacenar solamente los datos del evento.
     * @param $_SESSION["listareventos"]: Variable de sesión para trabajar con los eventos capturados.
     */
    public function miseventos(){

        $nick=$_SESSION['nick'];

        if(!$this->enlace){
            $this->enlace = mysqli_connect("localhost", "social", "social", "social");
        }else{
            $this->enlace=$this->enlace;
        }


        $aux="SELECT * FROM evento g JOIN usuario_pertenece_evento u WHERE g.borrado_evento=0 AND g.nombre_evento=u.nombre_evento_pertenece AND u.nick_perteneciente_evento='$nick'";
        $misEventos = mysqli_query($this->enlace, $aux) or die (mysqli_error($this->enlace));

        $sql2 = array();

        while($row = mysqli_fetch_array($misEventos)){
            array_push($sql2, $row);
        }
        $_SESSION["listareventos"] = $sql2;
    }

    /**
     * Función consultarevento(): Función para listar todos los eventos mientras no estén eliminados.
     * @param $_SESSION["tipo"]: Variable de sesión que contiene el tipo de usuario
     * @param $sql: Variable donde se almacena la query a enviar a la base de datos.
     * @param $row: Variable auxiliar para capturar la respuesta de la base de datos.
     * @param $sql2: Variable auxiliar para almacenar solamente los datos del evento.
     * @param $_SESSION["listareventos"]: Variable de sesión para trabajar con los eventos capturados.
     */
    public function consultarevento(){
        $sql = mysqli_query($this->enlace,"SELECT * FROM evento WHERE nombre_evento='$this->nombre_evento'") or die (mysqli_error($this->enlace));
        $sqlUsers = mysqli_query($this->enlace,"SELECT U.nick,U.foto FROM evento g INNER JOIN usuario_pertenece_evento u INNER JOIN usuario U WHERE borrado_evento=0 AND g.nombre_evento=u.nombre_evento_pertenece AND u.nick_perteneciente_evento=U.nick AND g.nombre_evento='$this->nombre_evento'") or die (mysqli_error($this->enlace));

        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($sql2, $row);
        }
        $sqlUsers2 = array();
        while($row = mysqli_fetch_array($sqlUsers)){
            array_push($sqlUsers2, $row);
        }
        $_SESSION["Consultarevento"] = $sql2;
        $_SESSION["ConsultarUsuariosevento"] = $sqlUsers2;
    }

    /**
     *Funcion Modificarevento: El usuario se modifica a si mismo o el administrador modifica un evento.
     * @param $sql: Variable para almacenar la respuesta de la base de datos si hiciese falta.
     * @param $update: Cadena de caracteres que se va rellenando según los datos que el usuario o administrador quieran modificar.
     */
    public function modificarevento(){
        $update="";

        if($this->limite_usuarios_evento!=0){
            $update.="limite_usuarios_evento='".$this->limite_usuarios_evento."',";
        }

        if($this->tipo_evento!=""){
            $update.="tipo_evento=".$this->tipo_evento.",";
        }

        if($this->descripcion_evento!=""){
            $update.="descripcion_evento='".$this->descripcion_evento."',";
        }

        if($this->imagen_evento!="../" && $this->imagen_evento!="../../Recursos/img/comun.png"){
            $update.="imagen_evento='".$this->imagen_evento."',";
        }

        $update = substr($update, 0, -1);

        $sql= mysqli_query($this->enlace,"UPDATE evento SET ".$update." WHERE nombre_evento='$this->nombre_evento'") or die (mysqli_error($this->enlace));
    }
    //*************************************** FIN CRUD evento ***************************************\

    /**
     * Funcion checkPertenece(): Comprueba que el usuario que se quiere unir al evento aún no me pertenece.
     * @param $_SESSION["pertenece"]:
     * @param $nick=$_SESSION["nick"]:
     * @param $sql:
     * @param $resultado:
     * @param $rowCheckPertenece:
     * @param $_SESSION["pertenece"]
     */
    public function checkPertenece(){
        $_SESSION["pertenece"]=false;
        $nick=$_SESSION["nick"];

        $sql = "SELECT * FROM usuario_pertenece_evento WHERE nombre_evento_pertenece='$this->nombre_evento' AND nick_perteneciente_evento='$nick'";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckPertenece=mysqli_fetch_assoc ($resultado);

        if($rowCheckPertenece){
            $_SESSION["pertenece"]=true;
        }
    }
    /**
     * Funcion accederevento(): Inserta un usuario en la tabla usuario_pertenece_evento.
     * $insertarUsuarioevento:
     * $resultado:
     */
    public function accederevento(){

        $nick=$_SESSION["nick"];
        $insertarUsuarioevento  = "INSERT INTO usuario_pertenece_evento (nombre_evento_pertenece,nick_perteneciente_evento,fecha_ingreso_usuario) VALUES ('$this->nombre_evento','$nick',NOW())";
        $resultado = mysqli_query($this->enlace, $insertarUsuarioevento) or die(mysqli_error($this->enlace));
    }

    /**
     *
     */
    public function checkSolicitud(){
        $_SESSION["peticion"]=false;
        $nick=$_SESSION["nick"];

        $sql = "SELECT * FROM usuario_invitacion_evento WHERE nombre_evento_invitacion='$this->nombre_evento' AND nick_emisor='$nick'";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckPeticion=mysqli_fetch_assoc ($resultado);

        if($rowCheckPeticion){
            $_SESSION["peticion"]=true;
        }
    }

    /**
     *
     */
    public function solicitudAccesoevento(){
        $nick=$_SESSION["nick"];
        $insertarUsuariosevento  = "INSERT INTO usuario_invitacion_evento (nombre_evento_invitacion,nick_emisor,nick_receptor,fecha_invitacion_evento) VALUES ('$this->nombre_evento','$nick','$this->nick_creador', NOW())";
        $resultado = mysqli_query($this->enlace, $insertarUsuariosevento) or die(mysqli_error($this->enlace));
    }

    /**
     *
     */
    public function mostrarSolicitudes(){
        $nick=$_SESSION["nick"];
        $sql= mysqli_query($this->enlace, "SELECT * FROM  usuario_invitacion_evento WHERE nick_receptor='$nick' AND fecha_aceptacion_evento IS NULL ") or die (mysqli_error($this->enlace));

        $sql2 = array();
        while($row = mysqli_fetch_array($sql)){array_push($sql2, $row);}
        $_SESSION["listarSolicitudesRecibidas"] = $sql2;

        if($_SESSION["tipo"]=="user"){
            $sqlEmitidas= mysqli_query($this->enlace, "SELECT * FROM  usuario_invitacion_evento WHERE nick_emisor='$nick' AND fecha_aceptacion_evento IS NULL ") or die (mysqli_error($this->enlace));

            $sqlEmitidasArray = array();
            while($row = mysqli_fetch_array($sqlEmitidas)){array_push($sqlEmitidasArray, $row);}
            $_SESSION["listarSolicitudesEmitidas"] = $sqlEmitidasArray;
        }
    }

    /**
     * @Function checkSolicitudAmigoEvento($nombre_evento,$nick_emisor,$nombre_amigo):
     */
    public function checkSolicitudAmigoEvento($nombre_evento,$nick_emisor,$nombre_amigo){
        $checkInvitacion=false;

        $sql = "SELECT * FROM usuario_invitacion_evento WHERE nombre_evento_invitacion='$nombre_evento' AND ( (nick_emisor='$nick_emisor' OR nick_receptor='$nick_emisor') AND ((nick_emisor='$nombre_amigo' OR nick_receptor='$nombre_amigo'))) ";
        $resultado = mysqli_query($this->enlace, $sql) or die(mysqli_error($this->enlace));

        $rowCheckPeticion=mysqli_fetch_assoc ($resultado);

        if($rowCheckPeticion){
            $checkInvitacion=true;
        }

        return $checkInvitacion;
    }
    /**
     * @Function enviarSolicitudAmigo($nombre_evento,$nick_emisor,$nombre_amigo)
     */
    public function enviarSolicitudAmigo($nombre_evento,$nick_emisor,$nombre_amigo){
        $insertarUsuariosEvento  = "INSERT INTO usuario_invitacion_evento (nombre_evento_invitacion,nick_emisor,nick_receptor,fecha_invitacion_evento) VALUES ('$nombre_evento','$nick_emisor','$nombre_amigo', NOW())";
        $resultado = mysqli_query($this->enlace, $insertarUsuariosEvento) or die(mysqli_error($this->enlace));
    }


    /**
     * @param $nombre_evento
     * @param $nick_emisor
     * @param $nick_receptor
     */
    public function aceptarUsuarioevento($nombre_evento,$nick_emisor,$nick_receptor){

        $borrarPeticionevento  = "UPDATE usuario_invitacion_evento SET fecha_aceptacion_evento=NOW() WHERE nick_receptor='$nick_receptor' AND nick_emisor='$nick_emisor' AND nombre_evento_invitacion='$nombre_evento' ";
        $insertarUsuarios  = "INSERT INTO usuario_pertenece_evento (nombre_evento_pertenece,nick_perteneciente_evento,fecha_ingreso_usuario) VALUES ('$nombre_evento','$nick_receptor',NOW())";
        $resultado = mysqli_query($this->enlace, $borrarPeticionevento) or die(mysqli_error($this->enlace));
        $resultadopertenece = mysqli_query($this->enlace, $insertarUsuarios) or die(mysqli_error($this->enlace));

    }

    /**
     * @param $nombre_evento
     * @param $nick_emisor
     * @param $nick_receptor
     */
    public function rechazarUsuarioevento($nombre_evento,$nick_emisor,$nick_receptor){
        $borrarPeticionevento  = "DELETE FROM usuario_invitacion_evento WHERE nick_receptor='$nick_receptor' AND nick_emisor='$nick_emisor' AND nombre_evento_invitacion='$nombre_evento' ";
        $resultado = mysqli_query($this->enlace, $borrarPeticionevento) or die(mysqli_error($this->enlace));
    }

}
?>