<?php
// Controlador de Usuarios
require_once '../Model/Usuarios.php';
require_once '../Model/Grupo.php';
require_once '../Model/Evento.php';
require_once '../Model/Publicacion.php';

if($_SESSION["lang"] != null){
    $langA = $_SESSION["lang"];
    require_once "../Recursos/languages/".$langA.".php";
}else{
    require_once "../Recursos/languages/es.php";
}

$accion = $_REQUEST['accion'];
switch ($accion) {


    //******************************* INICION CRUD PUBLICACION *******************************\\
    /**
        *Evento CrearPublicacion: Redirige a un usuario a la página de CrearPublicacion.php.
        *Evento Publicar: Permite al usuario crear una publicacion en el sistema.
    */
    case $lang['CrearPublicacion']:

          $grupo=new Grupo("","","","","","","","");
          $evento=new Evento("","","","","","","","");
          $grupo->misGrupos();
          $evento->miseventos();

          if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/CrearPublicacion.php');
            }else{
                header('location: ../View/Admin/CrearPublicacion.php');
            }
        break;
    case $lang['Publicar']:

            $error_documento=0;
            $urlDocumentos=array();


            if($_FILES['documento_usuario']['name'][0]==""){
                $urlDocumentos=null;
            }else{
                $uploaddir = '../Recursos/Documentos/';
                for($i=0; $i<count($_FILES['documento_usuario']['name']) ; $i++) {
                    $time= date("Ymd");
                    $uploadfile = $uploaddir . basename($time.$_FILES['documento_usuario']['name'][$i]);

                    if (move_uploaded_file($_FILES['documento_usuario']['tmp_name'][$i], $uploadfile)) {
                        array_push($urlDocumentos, $uploadfile);
                        echo "File is valid, and was successfully uploaded.\n";
                    } else {
                        echo "Upload failed";
                        $error_documento = $error_documento++;
                    }
                }
            }

            $publicacion = new Publicacion($_SESSION['nick'],$_POST['nombre_grupo_publicacion'],$_POST['nombre_evento_publicacion'],$_POST['contenido'],"","");
            $publicacion->crearPublicacion($urlDocumentos);


            if($error_documento==0) {
                $publicacion->misPublicaciones($_SESSION['nick']);
                if ($_SESSION["tipo"] == "user") {
                    header('location: ../View/User/MisPublicaciones.php?crear_publicacion=true');
                } else {
                    header('location: ../View/Admin/MisPublicaciones.php?crear_publicacion=true');
                }
            }else{
                if ($_SESSION["tipo"] == "user") {
                    header('location: ../View/User/CrearPublicacion.php?crear_publicacion=false');
                } else {
                    header('location: ../View/Admin/CrearPublicacion.php?crear_publicacion=false');
                }
            }
        break;


    /**
        * Evento Eliminar Publicacion: Elimina una publicacion.
    */
    case $lang['Eliminar Publicacion']:
            $publicacion = new Publicacion($_SESSION['nick'],"","","","","");
            $publicacion->eliminarPublicacion($_POST['codigo_publicacion']);
            $publicacion->misPublicaciones($_SESSION['nick']);

            if ($_SESSION["tipo"] == "user") {
                    header('location: ../View/User/MisPublicaciones.php?crear_publicacion=true');
            } else {
                    header('location: ../View/Admin/MisPublicaciones.php?crear_publicacion=true');
            }

        break;

    /**
     * Evento Modificar Publicacion: Redirige a la página de modificación.
     * Evento Modificar: Modifica los datos del usuario
     * @param $uploadfile: Variable que almacena la localización de la imagen.
     * @param $uploaddir: Variable que almacena el path donde se almacenaran las imágenes.
     */
    case $lang['Modificar Publicacion']:


        $publicacion = new Publicacion("","","","","","");

        $publicacion->verPublicacion($_POST['codigo_publicacion']);

        $grupo=new Grupo("","","","","","","","");
        $evento=new Evento("","","","","","","","");
        $grupo->misGrupos();
        $evento->miseventos();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/ModificarPublicacion.php');
        }else{
            header('location: ../View/Admin/ModificarPublicacion.php');
        }

        break;
    case $lang['Modificar']:

        $error_documento=0;
        $uploadfile="";

        if($_FILES['documento_usuario']['name'][0]==""){
            $urlDocumentos=null;
        }else{
            $uploaddir = '../Recursos/Documentos/';
            for($i=0; $i<count($_FILES['documento_usuario']['name']) ; $i++) {
                $time= date("Ymd");
                $uploadfile = $uploaddir . basename($time.$_FILES['documento_usuario']['name'][$i]);

                if (move_uploaded_file($_FILES['documento_usuario']['tmp_name'][$i], $uploadfile)) {
                    array_push($urlDocumentos, $uploadfile);
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_documento = $error_documento++;
                }
            }
        }

        $publicacion = new Publicacion($_SESSION['nick'],$_POST["nombre_grupo_publicacion"],$_POST["nombre_evento_publicacion"],$_POST["contenido"],"","");

        if($error_documento==0){

            $publicacion->modificarPublicacion($urlDocumentos,$_POST['codigo_publicacion']);
            $publicacion->misPublicaciones($_SESSION['nick']);
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/MisPublicaciones.php?exito=1');
            }else{
                header('location: ../View/Admin/MisPublicaciones.php?exito=1');
            }
        }else{
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ModificarPublicacion.php?error=1');
            }else{
                header('location: ../View/Admin/ModificarPublicacion.php?error=1');
            }

        }
        break;

    /**
        *Evento MisPublicaciones: Muestra las publicaciones de un usuario.
    */
    case $lang['MisPublicaciones']:
        $publicacion = new Publicacion($_SESSION['nick'],"","","","","");
        $publicacion->misPublicaciones($_SESSION['nick']);

        if ($_SESSION["tipo"] == "user") {
            header('location: ../View/User/MisPublicaciones.php');
        } else {
            header('location: ../View/Admin/MisPublicaciones.php');
        }
    break;

    /**
        *Evento Ver Publicacion: Muestra un publicacion y sus comentarios.
     */
    case $lang['Ver Publicacion']:
            $publicacion = new Publicacion("","","","","","");
            $publicacion->verPublicacion($_POST["codigo_publicacion"]);

            if ($_SESSION["tipo"] == "user") {
                header('location: ../View/User/ConsultarPublicacion.php');
            } else {
                header('location: ../View/Admin/ConsultarPublicacion.php');
            }
        break;
    //******************************* INICION CRUD PUBLICACION *******************************\\

    /**
        *Evento Comentar: Crea un comentario padre en un pubicación.
     */
    case $lang['Comentar']:
        $publicacion = new Publicacion("","","","","","");
        $publicacion->comentar($_POST['codigo_publicacion'],$_SESSION['nick'],$_POST['comentario'],$_POST['codigo_comentario']);
        $publicacion->verPublicacion($_POST["codigo_publicacion"]);

        if ($_SESSION["tipo"] == "user") {
            header('location: ../View/User/ConsultarPublicacion.php');
        } else {
            header('location: ../View/Admin/ConsultarPublicacion.php');
        }
        break;

    /**
        *Evento Responder: Responde al mensaje de otro usuario.
     */
    case $lang['Responder']:
            $publicacion = new Publicacion("","","","","","");
            $publicacion->comentar($_POST['codigo_publicacion'],$_SESSION['nick'],$_POST['comentario'],$_POST['codigo_comentario']);
            $publicacion->verPublicacion($_POST["codigo_publicacion"]);

            if ($_SESSION["tipo"] == "user") {
                header('location: ../View/User/ConsultarPublicacion.php');
            } else {
                header('location: ../View/Admin/ConsultarPublicacion.php');
            }
        break;

    /**
        *Evento Eliminar Comentario: Elimina los comentarios de una conversación.
     */
    case $lang['Eliminar Comentario']:
        $publicacion = new Publicacion("","","","","","");
        $publicacion->eliminarComentario($_POST['codigo_comentario']);
        $publicacion->verPublicacion($_POST["codigo_publicacion"]);

        if ($_SESSION["tipo"] == "user") {
            header('location: ../View/User/ConsultarPublicacion.php');
        } else {
            header('location: ../View/Admin/ConsultarPublicacion.php');
        }
        break;
    default:
        echo "La acción requerida no existe";
      break;
}
?>
