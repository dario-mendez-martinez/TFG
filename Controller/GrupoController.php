<?php
/** 
    *Controlador de Usuarios
    *
    * @author: Dario Mendez Martinez
*/
require_once '../Model/Usuarios.php';
require_once '../Model/Grupo.php';
require_once '../Model/Publicacion.php';

if($_SESSION["lang"] != null){
    $langA = $_SESSION["lang"];
    require_once "../Recursos/languages/".$langA.".php";
}else{
    require_once "../Recursos/languages/es.php";
}

$accion = $_REQUEST['accion'];
switch ($accion) {
    

    //**************************** INICIO CRUD GRUPOS ****************************\
    /**
        * Evento CrearGrupo: Permite al usuario registrar un grupo en el sistema.
        * @param $_SESSION["grupo_existe"]: Variable de sesión que se pone a true si el nombre del grupo ya existe
        * @param $grupo: Variable para crear un objeto de tipo grupo
        * @param $error_foto: Se pone a true si existe algún problema con la foto.
        * @param $uploadfile: Concatenación de la ruta donde se almacena la foto y su nombre.
        * @function checkNombreGrupo(): Función que comprueba si el nombre del grupo ya existe.
        * @function CrearGrupo(): Función que inserta el grupo en la base de datos.
        * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo del usuario.
    */
    case $lang['Crear Grupo']:

        $error_foto=false;

        if($_POST["nombre_grupo"]==""){

            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/CrearGrupo.php?registro_grupo_vacio=true');
            }else{
                header('location: ../View/Admin/CrearGrupo.php?registro_grupo_vacio=true');
            }

        }else{
            if(!empty($_FILES['imagen_grupo']['name'])){
                $uploaddir = '../Recursos/img/';
                $uploadfile = $uploaddir.basename($_FILES['imagen_grupo']['name']);

                if (move_uploaded_file($_FILES['imagen_grupo']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_foto=true;
                }
            }else{
                $uploadfile="../Recursos/img/comun.png";
            }

            $grupo=new Grupo($_POST["nombre_grupo"],$_POST["descripcion_grupo"],"",'../'.$uploadfile,intval($_POST["limite_usuarios_grupo"]),"0",$_POST["tipo_grupo"],$_SESSION["nick"]);
            $grupo->checkNombreGrupo();

            if($_SESSION["grupo_existe"]==false && $error_foto==false){
                $grupo->CrearGrupo();
                $grupo->listarGrupos();
                if($_SESSION["tipo"]=="user") {
                    header('location: ../View/User/ListarGrupos.php?registro_grupo_exito=true');
                }else{
                    header('location: ../View/Admin/ListarGrupos.php?registro_grupo_exito=true');
                }
            }else{
                if($_SESSION["tipo"]=="user") {
                    header('location: ../View/User/CrearGrupo.php?registro_grupo_error=true');
                }else{
                    header('location: ../View/Admin/CrearGrupo.php?registro_grupo_error=true');
                }
            }
        }

    break;
    
        
    /**
        * Evento ListarGrupo: Permite al sistema listar todos los grupos almacenados en él.
        * @param $grupo: Variable para crear el objeto de tipo grupo.
        * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo del usuario.
    */
    case $lang['ListarGrupos']:
        $grupo = new Grupo("","","","","","","","");
        $grupo->listarGrupos();

        if($_SESSION["tipo"]=="user") {
            header('location: ../View/User/ListarGrupos.php');
        }else{
            header('location: ../View/Admin/ListarGrupos.php');
        }
    break;
    
        
    /**
        * Evento Filtrar: Permite al sistema buscar un grupo en concreto.
        * @param $grupo: Variable para construir el objeto de tipo grupo.
        * @param $_SESSION["tipo"]: Variable de sesión para redirigir a una vista u otra.
    */
    case $lang['Filtrar']:
        $grupo=new Grupo($_POST["nombre_grupo"],"","","","","",$_SESSION["nick_creador"]);
        $grupo->filtrarGrupos();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/ListarGrupos.php');
        }else{
            header('location: ../View/Admin/ListarGrupos.php');
        }
    break;

    /**
        * Evento Eliminar Grupo: El administrador podrá eliminar cualquier grupo y un usuario solo aquellos grupos donde sea creador.
        * @param $grupo: Variable para construir el objeto de tipo grupo.
        * @param $_SESSION["tipo"]: Variable de sesión para redirigir a una vista u otra.
     */
    case $lang['Eliminar grupo']:
            $grupo =  new Grupo($_POST["nombre_grupo"],"","","","","","","");
            $grupo->eliminarGrupo();
            $grupo->listarGrupos();

            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/ListarGrupos.php');
            }else{
                header('location: ../View/Admin/ListarGrupos.php');
            }
        break;

    case $lang['MisGrupos']:
            $grupo = new Grupo("","","","","","","","");
            $grupo->misGrupos();

            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisGrupos.php');
            }else{
                header('location: ../View/Admin/MisGrupos.php');
            }
        break;

    /**
        *Evento Ver Grupo: devuelve la informacion de un grupo en concreto
    */
    case $lang['Ver Grupo']:
        $grupo = new Grupo($_POST["nombre_grupo"],"","","","","","","");
        $grupo->consultarGrupo();
        $publicacion=new Publicacion();
        $publicacion->publicacionesGrupoEvento($_POST["nombre_grupo"],null);
        if($_SESSION["tipo"]=="user") {
            header('location: ../View/User/ConsultarGrupo.php');
        }else{
            header('location: ../View/Admin/ConsultarGrupo.php');
        }
        break;

    /**
        *Evento Modificar Grupo: devuelve la informacion de un grupo en concreto
     */
    case $lang['Modificar Grupo']:

        $grupo = new Grupo($_POST["nombre_grupo"]);
        $grupo->consultarGrupo();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/ModificarGrupo.php');
        }else{
            header('location: ../View/Admin/ModificarGrupo.php');
        }

        break;
    /**
        *Evento Modificar: devuelve la informacion de un grupo en concreto
     */
    case $lang['Modificar']:

        $error_foto=false;
        $uploadfile="";

        if(isset($_FILES["imagen_grupo"])) {
            if (!empty($_FILES['imagen_grupo']['name'])) {
                $uploaddir = '../Recursos/img/';
                $uploadfile = $uploaddir . basename($_FILES['imagen_grupo']['name']);

                if (move_uploaded_file($_FILES['imagen_grupo']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_foto = true;
                }
            } else {
                $uploadfile = "../Recursos/img/comun.png";
            }
        }

        $grupo=new Grupo($_POST["nombre_grupo"],$_POST["descripcion_grupo"],"",'../'.$uploadfile,intval($_POST["limite_usuarios_grupo"]),"0",$_POST["tipo_grupo"],$_SESSION["nick"]);

        if($error_foto==false){

            $grupo->modificarGrupo();
            if($_SESSION["tipo"]!="admin"){
                $grupo->consultarGrupo();
                header('location: ../View/User/ModificarGrupo.php?exito=1');
            }else{
                $grupo->consultarGrupo();
                header('location: ../View/Admin/ModificarGrupo.php?exito=1');
            }
        }else{
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ModificarGrupo.php?error=1');
            }else{
                header('location: ../View/Admin/ModificarGrupo.php?error=1');
            }

        }
        break;
    //**************************** FIN CRUD GRUPOS ****************************\

    /**
        *Evento Acceder a grupo: Permite al usuario inscribirse a un grupo publico.
    */
    case $lang['Acceder a grupo']:
        $pertenece=false;

        $grupo = new Grupo($_POST["nombre_grupo"],"","","","","","","");
        $grupo->checkPertenece();

        $pertenece=$_SESSION["pertenece"];

        if($pertenece==false){
            $grupo->accederGrupo();
            $grupo->misGrupos();
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisGrupos.php');
            }else{
                header('location: ../View/Admin/MisGrupos.php');
            }
        }else{
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisGrupos.php');
            }else{
                header('location: ../View/Admin/MisGrupos.php');
            }
        }
        break;

    /**
        * Evento Solicitar acceso: Solicita el acceso a un grupo por parte de un usuario.
    */
    case $lang['Solicitar acceso']:
        $peticion=false;
        $grupo=new Grupo($_POST["nombre_grupo"],"","","","","","",$_POST["nick_creador"]);

        $grupo->checkSolicitud();
        $peticion=$_SESSION["peticion"];


        if($peticion==false){
            $grupo->solicitudAccesoGrupo();
            $grupo->misGrupos();

            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisGrupos.php');
            }else{
                header('location: ../View/Admin/MisGrupos.php');
            }
        }else{
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisGrupos.php');
            }else{
                header('location: ../View/Admin/MisGrupos.php');
            }
        }
        break;

    /**
        *Evento MisSolicitudes: Muestra las solicitudes de un usuario en cuanto a grupos.
    */
    case $lang['MisSolicitudes']:
        $grupo = new Grupo();
        $grupo->mostrarSolicitudes();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudes.php');
        }else{
            header('location: ../View/Admin/MisSolicitudes.php');
        }
    break;
        

    
    /**
        * Evento Aceptar usuario: Acepta a un usuario en un grupo.
    */   
    case $lang['Aceptar usuario']:

        $nombre_grupo=$_POST["nombre_grupo_invitacion"];
        $nick_emisor=$_POST["nick_emisor"];
        $grupo = new Grupo();
        $grupo->aceptarUsuarioGrupo($nombre_grupo,$nick_emisor,$_SESSION["nick"]);
        $grupo->mostrarSolicitudes();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudes.php');
        }else{
            header('location: ../View/Admin/MisSolicitudes.php');
        }
    break;


    /**
        * Evento Rechazar usuario: Rechaza la entrada de un usuario en un grupo
    */
    case $lang['Rechazar usuario']:

        $nombre_grupo=$_POST["nombre_grupo_invitacion"];
        $nick_emisor=$_POST["nick_emisor"];
        $grupo = new Grupo();
        $grupo->rechazarUsuarioGrupo($nombre_grupo,$nick_emisor,$_SESSION["nick"]);
        $grupo->mostrarSolicitudes();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudes.php');
        }else{
            header('location: ../View/Admin/MisSolicitudes.php');
        }
        break;
    /**
        * Evento Invitar Amigo: Invita a un amigo a pertenecer a un grupo.
    */
    case $lang['Invitar Amigo']:
        $peticion=false;
        $grupo=new Grupo($_POST["nombre_grupo"],"","","","","","","");
        $peticion=$grupo->checkSolicitudAmigoGrupo($_POST["nombre_grupo"],$_SESSION["nick"],$_POST["nombre_amigo"]);


        if($peticion==false){
            $grupo->enviarSolicitudAmigo($_POST["nombre_grupo"],$_SESSION["nick"],$_POST["nombre_amigo"]);
            $grupo->mostrarSolicitudes();

            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisSolicitudes.php');
            }else{
                header('location: ../View/Admin/MisSolicitudes.php');
            }
        }else{
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisGrupos.php?error_invitar_amigo=true');
            }else{
                header('location: ../View/Admin/MisGrupos.php?error_invitar_amigo=true');
            }
        }
        break;

    default:
        echo "La acción requerida no existe";
      break;
}
?>
