<?php

/**
    *Controlador de Usuarios
    *
    * @author: Dario Mendez Martinez
*/

require_once '../Model/Usuarios.php';
require_once '../Model/Grupo.php';
require_once '../Model/Evento.php';
require_once '../Model/Publicacion.php';

if($_SESSION["lang"] != null){
    $langA = $_SESSION["lang"];
    var_dump($langA);
    require_once "../Recursos/languages/".$langA.".php";
}else{
    require_once "../Recursos/languages/es.php";
}

$accion = $_REQUEST['accion'];
switch ($accion) {
      
        
    /**
        *Redireccion desde el index.php
    */
    case $lang["Inicio"]:
            header('Location: ../View/Login.php');
        break;
    
        
    /** 
        * Evento Login: Se comprueba la existencia de un usuario en la base de datos y se accede al sistema si este existe
        *
        * @param $usuario: Creación del objeto de tipo usuario para realizar operaciones con él.
        * @param $_SESSION["permitido"]: Variable de sesión que evita el acceso a usuarios que no estén logueados. En este caso se pone a true para permitir navegar al usuario.
        * @param $_SESSION["tipo"]: Distinción de roles para los usuarios. 
    */
    case $lang["Login"]:
            $usuario = new Usuarios($_POST["nick"],$_POST["password"],"","","","","","","","");
            $usuario->login();
            $publicacion=new Publicacion();

            if(isset($_SESSION["tipo"])){
                $_SESSION["permitido"]=TRUE;
                $usuario->PerfilUsuario();
                $publicacion->misPublicaciones("");
                if($_SESSION["tipo"]!="admin"){
                    header("location: ../View/User/IndexUsuario.php");
                }else{
                    header("location: ../View/Admin/indexAdmin.php");
                }
            }else{
                header("location: ../View/Login.php?error_login=true");
            }
        
        break;


    /*********************** INICIO CRUD USUARIO ***********************\

    /**
        * Evento Registro: Registra un usuario.
        *
        * @param $usuario: Creación del objeto de tipo usuario para realizar operaciones con él.
        * @param $error_nick: Variable para capturar si el usuario existe o no en el sistema.
        * @param $error_password: Variable que comprueba si la contraseña introducida por el usuario cumple o no los criterios.
        * @param $error_mail: Variable que comprueba si el email introducido tiene algún error.
        * @param $error_foto: Variable donde se almacena true en caso de que exista algún error con la imagen del usuario.
        * @param $borrado_usuario: Variable que se pone a true si el usuario está en el sistema y ha sido eliminado.
        * @param $uploadfile: Variable que almacena la localización de la imagen.
        * @param $uploaddir: Variable que almacena el path donde se almacenaran las imágenes.
        * @param $error_reglas_password: Variable que verifica las condiciones necesarias para que una contraseña sea válida.
    */
    case $lang["Registro"]:

            $error_nick=false;
            $error_password=false;
            $error_reglas_password=false;
            $error_mail=false;
            $error_foto=false;
            $borrado_usuario=false;


            if(!empty($_FILES['foto']['name'])){
                $uploaddir = '../Recursos/img/';
                $uploadfile = $uploaddir.basename($_FILES['foto']['name']);

                if (move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_foto=true;
                }
            }else{
                $uploadfile="../Recursos/img/comun.png";
            }

            $usuario = new Usuarios($_POST["nick"],password_hash($_POST["password1"], PASSWORD_DEFAULT),$_POST["email"],$_POST["nombre"],$_POST["apellido"],$_POST["telefono"],"",$_POST["cargo"],"../".$uploadfile,"","","");
            $usuario->checkNickAndMail();

            $borrado_usuario = $_SESSION['borradoUsuario'];
            $error_nick = $_SESSION['error_nick'];
            $error_mail = $_SESSION['error_mail'];

            if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
                $error_mail=true;
            }

            if ($_POST["password1"]!==$_POST["password2"]) {
              $error_password=true;
            }

            if(strlen($_POST["password1"]) < 6){
                $error_reglas_password=true;
            }
            if(strlen($_POST["password1"]) > 16){
                $error_reglas_password=true;
            }
            if (!preg_match('`[a-z]`',$_POST["password1"])){
                $error_reglas_password=true;
            }
            if (!preg_match('`[A-Z]`',$_POST["password1"])){
                $error_reglas_password=true;
            }
            if (!preg_match('`[0-9]`',$_POST["password1"])){
                $error_reglas_password=true;
            }

            if($error_nick==false && $error_password==false && $error_mail==false && $error_foto==false && $borrado_usuario==false && $error_reglas_password==false){
                $usuario->altaUsuario();
                header('location: ../View/Login.php?registro_exito=true');
            }else{
                header('location: ../View/User/Registro.php?error_nick='.$error_nick.'&error_password='.$error_password.'&error_mail='.$error_mail.'&error_foto='.$error_foto.'&borrado_usuario='.$borrado_usuario.'&error_reglas_password='.$error_reglas_password.'');
            }
        break;
       
        
    /**
        *Evento listar usuarios: Crea una lista al administrador con todos los usuarios registrados en el sistema. Siempre que no esten eliminados logicamente(sin eliminar sus datos en la bd en el caso de un usuario normal). 
        *
        * @param $usuario: Creación del objeto de tipo usuario para realizar operaciones con él.
        * @param $_SESSION["tipo"]: Para enviar a una u otra vista según el tipo de invitado.
    */
    case $lang["ListarUsuarios"]:
            $usuario = new Usuarios("","","","","","","","","","");
            $usuario->listarUsuarios();

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarUsuarios.php');
            }else{
                header('location: ../View/Admin/ListarUsuarios.php');
            }
        break;
    
    /**
        *Evento Filtrar: En este evento se capturarán los filtros introducidos por el usuario y se devolverá la búsqueda resultante.
        *
        * @param $usuario: Creación del objeto de tipo usuario para realizar operaciones con él.
        * @param $_SESSION["tipo"]: Para enviar a una u otra vista según el tipo de invitado.
    */  
    case $lang["Filtrar"]:
            $usuario = new Usuarios($_POST["nick"],"",$_POST["email"],$_POST["nombre"],"","","",$_POST["cargo"],"","","","");
            $usuario->filtrarUsuarios();

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarUsuarios.php');
            }else{
                header('location: ../View/Admin/ListarUsuarios.php');
            }
        
        break;

    /**
        *Evento ConsultarUsuario: Muestra el perfil de un usuario en concreto.
        * @param $usuario: Creación del objeto de tipo usuarios para realizar operaciones con él.
        * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo de usuario.
    */
    case $lang["Ver Usuario"]:
            $usuario = new Usuarios($_POST['nick']);
            $usuario->PerfilUsuario();
            $publicacion=new Publicacion();
            $publicacion->misPublicaciones($_POST['nick']);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/PerfilUsuario.php');
            }else{
                header('location: ../View/Admin/PerfilUsuario.php');
            }

        break;

    /**
        *Evento Perfil: Contruye el perfil del usuario.
        * @param $usuario: Creación del objeto de tipo usuarios para realizar operaciones con él.
        * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo de usuario.
     */
    case $lang["Perfil"]:
        var_dump("hola");
    		if(isset($_POST['nick'])){
            	$nick=$_POST['nick'];
            }else{
            	$nick=$_SESSION['nick'];
            }

            $usuario = new Usuarios($_SESSION['nick']);
            $usuario->PerfilUsuario();
            $publicacion=new Publicacion();
            $publicacion->misPublicaciones($nick);
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/IndexUsuario.php');
            }else{
                header('location: ../View/Admin/indexAdmin.php');
            }

        break;

    /**
        *Evento Ver Modificar Usuario: Redirige a la página de modificación.
        *Evento Modificar: Modifica los datos del usuario
        * @param $error_password: Variable que comprueba si la contraseña introducida por el usuario cumple o no los criterios.
        * @param $error_foto: Variable donde se almacena true en caso de que exista algún error con la imagen del usuario.
        * @param $uploadfile: Variable que almacena la localización de la imagen.
        * @param $uploaddir: Variable que almacena el path donde se almacenaran las imágenes.
        * @param $error_reglas_password: Variable que verifica las condiciones necesarias para que una contraseña sea válida.
        * @param $password: Variable auxiliar para enviar correctamente un posible cambio de contraseña.
    */
    case $lang['Modificar Usuario']:

            if(isset($_POST['nick'])){
                $nick = $_POST['nick'];
            }else{
                $nick=$_SESSION["nick"];
            }

            $usuario = new Usuarios($nick);
            $usuario->PerfilUsuario();

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ModificarUsuario.php');
            }else{
                header('location: ../View/Admin/ModificarUsuario.php');
            }

        break;
    case $lang['Modificar']:

            $error_password=false;
            $error_reglas_password=false;
            $error_foto=false;
            $uploadfile="";
            $password="";

            if(isset($_FILES["foto"])) {
                if (!empty($_FILES['foto']['name'])) {
                    $uploaddir = '../Recursos/img/';
                    $uploadfile = $uploaddir . basename($_FILES['foto']['name']);

                    if (move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile)) {
                        echo "File is valid, and was successfully uploaded.\n";
                    } else {
                        echo "Upload failed";
                        $error_foto = true;
                    }
                } else {
                    $uploadfile = "../Recursos/img/comun.png";
                }
            }

            if(isset($_POST["password1"]) && isset($_POST["password2"]) && ($_POST['password1']!="" && $_POST['password2']!="")) {
                $password=password_hash($_POST["password1"],PASSWORD_DEFAULT);
                if ($_POST["password1"] !== $_POST["password2"]) {
                    $error_password = true;
                }

                if (strlen($_POST["password1"]) < 6) {
                    $error_reglas_password = true;
                }
                if (strlen($_POST["password1"]) > 16) {
                    $error_reglas_password = true;
                }
                if (!preg_match('`[a-z]`', $_POST["password1"])) {
                    $error_reglas_password = true;
                }
                if (!preg_match('`[A-Z]`', $_POST["password1"])) {
                    $error_reglas_password = true;
                }
                if (!preg_match('`[0-9]`', $_POST["password1"])) {
                    $error_reglas_password = true;
                }
            }

            $usuario = new Usuarios($_POST["nick"], $password,"",$_POST["nombre"],$_POST["apellido"],$_POST["telefono"],"",$_POST["cargo"],"../".$uploadfile,$_POST["descripcion"],"","");

            if($error_password==false && $error_foto==false && $error_reglas_password==false){

                $usuario->modificarUsuario();
                if($_SESSION["tipo"]!="admin"){
                    $usuario->PerfilUsuario();
                    header('location: ../View/User/ModificarUsuario.php?exito=1');
                }else{
                    $usuario->PerfilUsuario();
                    header('location: ../View/Admin/ModificarUsuario.php?exito=1');
                }
            }else{
                if($_SESSION["tipo"]!="admin"){
                    header('location: ../View/User/ModificarUsuario.php?error_password='.$error_password.'&error_foto='.$error_foto.'&error_reglas_password='.$error_reglas_password.'');
                }else{
                    header('location: ../View/Admin/ModificarUsuario.php?error_password='.$error_password.'&error_foto='.$error_foto.'&error_reglas_password='.$error_reglas_password.'');
                }

            }
        break;

    /**
        *Evento Eliminar Usuario: El usuario o el administrador eliminan a un usuario(Usuario a si mismo,administrador a quien quiera).
        * @param $usuario: Variable para crear el objeto de tipo usuario
        * @param $_SESSION["tipo"]: Variable de sesión para saber que tarea realizar.
        * @function session_unset(): Limpia las sesiones creadas mientras el usuario estaba en la aplicación.
        * @function session_destroy(): Después de limpiar se destruyen todas las variables.
    */
    case $lang['Eliminar Usuario']:

            $usuario = new Usuarios($_POST["nick"]);
            $usuario->borrarUsuario();

            if($_SESSION["tipo"]=="user"){
                $_SESSION["permitido"]=FALSE;
                session_unset();
                session_destroy();
                header('location: ../View/Login.php');
            }else{
                $usuario->listarUsuarios();
                header('location: ../View/Admin/ListarUsuarios.php');
            }

        break;

    /**
        *Evento Volver a crear: El usuario o el administrador eliminan a un usuario(Usuario a si mismo,administrador a quien quiera).
        * @param
     */
    case $lang['Volver a crear']:
            $usuario = new Usuarios($_POST["nick"]);
            $usuario->volverCrearUsuario();

            $usuario->listarUsuarios();
            header('location: ../View/Admin/ListarUsuarios.php');
        break;
    /*********************** FIN CRUD USUARIO ***********************\

    /**
        * Evento Pedir amistad: En este evento se enviará la petición de amistad de un usuario a otro.
        *
        *
    */
    case $lang['Pedir amistad']:

        $checkPeticion=false;
        $usuario = new Usuarios($_POST["nick"],"","","","","","","","","");
        $checkPeticion = $usuario->checkPeticionAmistad();

        if($checkPeticion==false){

            $usuario->crearPeticionAmistad();
            if($_SESSION["tipo"]!="admin"){
                //$usuario->PerfilUsuario();
                header('location: ../View/User/MisSolicitudesAmistad.php?exito=1');
            }else{
                //$usuario->PerfilUsuario();
                header('location: ../View/Admin/MisSolicitudesAmistad.php?exito=1');
            }
        }else{
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarUsuarios.php?error_peticion_amistad='.$checkPeticion);
            }else{
                header('location: ../View/Admin/ListarUsuarios.php?error_peticion_amistad='.$checkPeticion);
            }

        }
    break;  

    /**
        *Evento ListarAmigos:Lista a los amigos del usuario.
    */
    case $lang['ListarAmigos']:
    var_dump($_SESSION["nick"]);
        $usuario = new Usuarios($_SESSION["nick"],"","","","","","","","","");
        $usuario->listarAmigos("");
        
        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisAmigos.php');
        }else{
            header('location: ../View/Admin/MisAmigos.php');
        }
    break;


    /**
        *Evento ListarSolicitudesAmistad: Muestra las solicitudes de amistad que tiene un usuario.
    */
    case $lang['ListarSolicitudesAmistad']:
        $usuario = new Usuarios($_SESSION["nick"],"","","","","","","","","");
        $usuario->listarSolicitudesAmistad();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudesAmistad.php');
        }else{
            header('location: ../View/Admin/MisSolicitudesAmistad.php');
        }
        break;

    /**
        *Evento Eliminar Amigo: El usuario puede eliminar un amigo(de forma logica,sus datos seguiran en la bd).
    */
    case $lang['Eliminar Amigo']:
        //var_dump()
            $usuario = new Usuarios($_POST["nick"],"","","","","","","","","");
            $usuario->borrarAmigo();
            $usuario->setNick($_SESSION["nick"]);
            $usuario->listarAmigos($_SESSION["nick"]);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/MisAmigos.php?error_peticion_amistad=true');
            }else{
                header('location: ../View/Admin/MisAmigos.php?error_peticion_amistad=true');
            }
        break;

    /**
        *Evento Aceptar amistad: Permite al usuario aceptar una peticion de amistad 
    */
    case $lang['Aceptar Amistad']:
        $usuario = new Usuarios($_POST["nick_peticion_amistad"],"","","","","","","","","");
        $usuario->aceptarAmistad($_SESSION["nick"]);

        $usuario->setNick($_SESSION["nick"]);
        $usuario->listarSolicitudesAmistad();
        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudesAmistad.php');
        }else{
            header('location: ../View/Admin/MisSolicitudesAmistad.php');
        }
    break;

    /**
        *Evento Rechazar amistad: Permite al usuario rechazar una peticion de amistad
     */
    case $lang['Rechazar Amistad']:
            $usuario = new Usuarios($_POST["nick_peticion_amistad"],"","","","","","","","","");
            $usuario->rechazarAmistad($_SESSION["nick"]);

            $usuario->setNick($_SESSION["nick"]);
            $usuario->listarSolicitudesAmistad();
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/MisSolicitudesAmistad.php');
            }else{
                header('location: ../View/Admin/MisSolicitudesAmistad.php');
            }
        break;


    /**
        *Evento Invitar a grupo: Despues de elegis al usuario al cual invitar,redirige a los grupos del usuario
    */
    case $lang['Invitar a grupo']:

            $grupo = new Grupo("","","","","","","","","","");
            $grupo->misGrupos();

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/MisGrupos.php?amigo='.$_POST["nick"]);
            }else{
                header('location: ../View/Admin/MisGrupos.php?amigo='.$_POST["nick"]);
            }
        break;

    /**
        *Evento Invitar a evento: Despues de elegis al usuario al cual invitar,redirige a los eventos del usuario
     */
    case $lang['Invitar a evento']:

            $evento = new Evento("","","","","","","","","","");
            $evento->misEventos();

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/MisEventos.php?amigo='.$_POST["nick"]);
            }else{
                header('location: ../View/Admin/MisEventos.php?amigo='.$_POST["nick"]);
            }
        break;

    /**
        * Evento CrearMensaje: Evento que reedirige a la página de creación de mensajes.
        * Evento Enviar Mensaje: Crea un nuevo mensaje para enviar al usuario que el cliente tenga como amigo.
     */
    case $lang['CrearMensaje']:

            $usuario = new Usuarios();
            if($_SESSION['tipo']=="user"){
                $usuario->listarAmigos($_SESSION["nick"]);
            }else{
                $usuario->listarUsuarios();
            }

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/CrearMensaje.php');
            }else{
                header('location: ../View/Admin/CrearMensaje.php');
            }
        break;
    case $lang['Enviar Mensaje']:
            $usuario = new Usuarios();
            $usuario->enviarMensaje($_SESSION['nick'],$_POST['nick_receptor'],$_POST["mensaje"],null);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/IndexUsuario.php');
            }else{
                header('location: ../View/Admin/indexAdmin.php');
            }
        break;

    /**
        * Evento MisConversaciones: Muestra las conversacion enviadas y recibidas de un usuario.
    */
    case $lang['MisConversaciones']:
            $usuario = new Usuarios();
            $usuario->listarConversacion($_SESSION['nick']);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarConversaciones.php');
            }else{
                header('location: ../View/Admin/ListarConversaciones.php');
            }
        break;

    /**
        * Evento Eliminar Conversacion: Elimina una conversacion con los mensajes que contenía
    */
    case $lang['Eliminar Conversacion']:
            $usuario = new Usuarios();
            $usuario->eliminarConversacion($_POST['nick_emisor'],$_POST['nick_receptor']);
            $usuario->listarConversacion($_SESSION['nick']);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarConversaciones.php');
            }else{
                header('location: ../View/Admin/ListarConversaciones.php');
            }
        break;

    /**
        * Evento Ver Conversacion: Muestra en detalle una conversación.
    */
    case $lang['Ver Conversacion']:
            $usuario = new Usuarios();
            $usuario->verConversacion($_POST['nick_emisor'],$_POST['nick_receptor']);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/VerConversacion.php');
            }else{
                header('location: ../View/Admin/VerConversacion.php');
            }
        break;

    /**
        * Evento Eliminar Mensaje: ELimina un mensaje de una conversacion
     */
    case $lang['Eliminar Mensaje']:
            $usuario = new Usuarios();
            $usuario->eliminarMensaje($_POST['codigo_mensaje']);
            $usuario->verConversacion($_SESSION['nick'],$_POST['nick_receptor']);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/VerConversacion.php');
            }else{
                header('location: ../View/Admin/VerConversacion.php');
            }
        break;

    /**
        * Evento Responder Mensaje: Responde a un mensaje
    */
    case $lang['Responder Mensaje']:
        $usuario = new Usuarios();
        var_dump($_POST);
        $usuario->enviarMensaje($_SESSION['nick'],$_POST['nick_receptor'],$_POST["mensaje"],intval($_POST['codigo_mensaje_padre']));
        $usuario->verConversacion($_SESSION['nick'],$_POST['nick_receptor']);

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/VerConversacion.php');
        }else{
            header('location: ../View/Admin/VerConversacion.php');
        }
        break;

    /**
        * Evento ListarDocumentos: Evento que recupera la lista de documentos subida por un usuario o todos los documentos para un administrador.
    */
    case $lang['ListarDocumentos']:
            $usuario=new Usuarios();
            $usuario->listarDocumentos($_SESSION['nick']);


            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarDocumentos.php');
            }else{
                header('location: ../View/Admin/ListarDocumentos.php');
            }
        break;

    /**
        *
    */
    case $lang['Eliminar documento']:
            $usuario=new Usuarios();
            $usuario->eliminarDocumento($_POST['codigo_documento']);
            $usuario->listarDocumentos($_SESSION['nick']);

            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ListarDocumentos.php');
            }else{
                header('location: ../View/Admin/ListarDocumentos.php');
            }
        break;
    /**
        *
    */
    case $lang['Subir Documento']:
            $error_documento=false;
            $usuario=new Usuarios();

            if(!empty($_FILES['documento']['name'])){
                $uploaddir = '../Recursos/Documentos/';
                $time= date("Ymds");
                $uploadfile = $uploaddir.basename($time.$_FILES['documento']['name']);
                if (copy($_FILES['documento']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_documento=true;
                }
            }else{
                    $error_documento=true;
            }

            if($error_documento==false) {
                $usuario->subirDocumento($_SESSION['nick'],'../'.$uploadfile);
                $usuario->listarDocumentos($_SESSION['nick']);
                if ($_SESSION["tipo"] != "admin") {
                    header('location: ../View/User/ListarDocumentos.php?exito=true');
                } else {
                    header('location: ../View/Admin/ListarDocumentos.php?exito=true');
                }
            }else{
                if ($_SESSION["tipo"] != "admin") {
                    header('location: ../View/User/ListarDocumentos.php?exito=false');
                } else {
                    header('location: ../View/Admin/ListarDocumentos.php?exito=false');
                }
            }
        break;

    /**
        * Evento CerrarSesion: Se cierra la sesión de un usuario
        * @param $_SESSION["permitido"]: Evita que el usuario una vez ha salido de la aplicación pueda acceder a ella por la url.
        * @function session_unset(): Limpia las sesiones creadas mientras el usuario estaba en la aplicación.
        * @function session_destroy(): Después de limpiar se destruyen todas las variables.
    */   
    case $lang['cerrarSesion']:
        $_SESSION["permitido"]=FALSE;
        session_unset();
        session_destroy();
        header('location: ../View/Login.php');
    break;
    
    default:
        echo "La acción requerida no existe";
      break;
}
?>
