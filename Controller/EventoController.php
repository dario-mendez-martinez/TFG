<?php
/**
 *Controlador de Usuarios
 *
 * @author: Dario Mendez Martinez
 */
require_once '../Model/Usuarios.php';
require_once '../Model/Evento.php';
require_once '../Model/Publicacion.php';

if($_SESSION["lang"] != null){
    $langA = $_SESSION["lang"];
    require_once "../Recursos/languages/".$langA.".php";
}else{
    require_once "../Recursos/languages/es.php";
}

$accion = $_REQUEST['accion'];
switch ($accion) {


    //**************************** INICIO CRUD Eventos ****************************\
    /**
     * Evento CrearEvento: Permite al usuario registrar un Evento en el sistema.
     * @param $_SESSION["Evento_existe"]: Variable de sesión que se pone a true si el nombre del Evento ya existe
     * @param $Evento: Variable para crear un objeto de tipo Evento
     * @param $error_foto: Se pone a true si existe algún problema con la foto.
     * @param $uploadfile: Concatenación de la ruta donde se almacena la foto y su nombre.
     * @function checkNombreEvento(): Función que comprueba si el nombre del Evento ya existe.
     * @function CrearEvento(): Función que inserta el Evento en la base de datos.
     * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo del usuario.
     */
    case $lang['Crear Evento']:

        $error_foto=false;
        if($_POST["nombre_evento"]==""){
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/CrearEvento.php?registro_Evento_vacio=true');
            }else{
                header('location: ../View/Admin/CrearEvento.php?registro_Evento_vacio=true');
            }
        }else{
            if(!empty($_FILES['imagen_evento']['name'])){
                $uploaddir = '../Recursos/img/';
                $uploadfile = $uploaddir.basename($_FILES['imagen_evento']['name']);

                if (move_uploaded_file($_FILES['imagen_evento']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_foto=true;
                }
            }else{
                $uploadfile="../Recursos/img/comun.png";
            }

            $Evento=new Evento($_POST["nombre_evento"],$_POST["descripcion_evento"],"",'../'.$uploadfile,intval($_POST["limite_usuarios_evento"]),"0",$_POST["tipo_evento"],$_SESSION["nick"]);
            $Evento->checkNombreEvento();
            if($_SESSION["evento_existe"]==false && $error_foto==false){
                $Evento->CrearEvento();
                $Evento->listarEventos();
                if($_SESSION["tipo"]=="user") {
                    header('location: ../View/User/ListarEventos.php?registro_evento_exito=true');
                }else{
                    header('location: ../View/Admin/ListarEventos.php?registro_evento_exito=true');
                }
            }else{
                if($_SESSION["tipo"]=="user") {
                    header('location: ../View/User/CrearEvento.php?registro_evento_error=true');
                }else{
                    header('location: ../View/Admin/CrearEvento.php?registro_evento_error=true');
                }
            }
        }

        break;


    /**
     * Evento ListarEvento: Permite al sistema listar todos los Eventos almacenados en él.
     * @param $Evento: Variable para crear el objeto de tipo Evento.
     * @param $_SESSION["tipo"]: Variable de sesión que almacena el tipo del usuario.
     */
    case $lang['ListarEventos']:
        $Evento = new Evento("","","","","","","","");
        $Evento->listarEventos();

        if($_SESSION["tipo"]=="user") {
            header('location: ../View/User/ListarEventos.php');
        }else{
            header('location: ../View/Admin/ListarEventos.php');
        }
        break;


    /**
     * Evento Filtrar: Permite al sistema buscar un Evento en concreto.
     * @param $Evento: Variable para construir el objeto de tipo Evento.
     * @param $_SESSION["tipo"]: Variable de sesión para redirigir a una vista u otra.
     */
    case $lang['Filtrar']:
        $Evento=new Evento($_POST["nombre_evento"],"","","","","",$_SESSION["nick_creador"]);
        $Evento->filtrarEventos();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/ListarEventos.php');
        }else{
            header('location: ../View/Admin/ListarEventos.php');
        }
        break;

    /**
     * Evento Eliminar Evento: El administrador podrá eliminar cualquier Evento y un usuario solo aquellos Eventos donde sea creador.
     * @param $Evento: Variable para construir el objeto de tipo Evento.
     * @param $_SESSION["tipo"]: Variable de sesión para redirigir a una vista u otra.
     */
    case $lang['Eliminar Evento']:
        $Evento =  new Evento($_POST["nombre_evento"],"","","","","","","");
        $Evento->eliminarEvento();
        $Evento->listarEventos();

        if($_SESSION["tipo"]=="user") {
            header('location: ../View/User/ListarEventos.php');
        }else{
            header('location: ../View/Admin/ListarEventos.php');
        }
        break;

    case $lang['MisEventos']:
        $Evento = new Evento("","","","","","","","");
        $Evento->misEventos();

        if($_SESSION["tipo"]=="user") {
            header('location: ../View/User/MisEventos.php');
        }else{
            header('location: ../View/Admin/MisEventos.php');
        }
        break;

    /**
        *Evento Ver Evento: devuelve la informacion de un Evento en concreto
     */
    case $lang['Ver evento']:
        $Evento = new Evento($_POST["nombre_evento"],"","","","","","","");
        $Evento->consultarEvento();
        $publicacion=new Publicacion();
        $publicacion->publicacionesGrupoEvento(null,$_POST["nombre_evento"]);
        if($_SESSION["tipo"]=="user") {
            header('location: ../View/User/ConsultarEvento.php');
        }else{
            header('location: ../View/Admin/ConsultarEvento.php');
        }
        break;

    /**
        *Evento Modificar Evento: devuelve la informacion de un Evento en concreto
     */
    case $lang['Modificar evento']:

        $Evento = new Evento($_POST["nombre_evento"],"","","","","","","");
        $Evento->consultarEvento();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/ModificarEvento.php');
        }else{
            header('location: ../View/Admin/ModificarEvento.php');
        }

        break;
    /**
        *Evento Modificar: devuelve la informacion de un Evento en concreto
     */
    case $lang['Modificar']:

        $error_foto=false;
        $uploadfile="";

        if(isset($_FILES["imagen_evento"])) {
            if (!empty($_FILES['imagen_evento']['name'])) {
                $uploaddir = '../Recursos/img/';
                $uploadfile = $uploaddir . basename($_FILES['imagen_evento']['name']);

                if (move_uploaded_file($_FILES['imagen_evento']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.\n";
                } else {
                    echo "Upload failed";
                    $error_foto = true;
                }
            } else {
                $uploadfile = "../Recursos/img/comun.png";
            }
        }

        $Evento=new Evento($_POST["nombre_evento"],$_POST["descripcion_evento"],"",'../'.$uploadfile,intval($_POST["limite_usuarios_evento"]),"0",$_POST["tipo_evento"],$_SESSION["nick"]);

        if($error_foto==false){

            $Evento->modificarEvento();
            if($_SESSION["tipo"]!="admin"){
                $Evento->consultarEvento();
                header('location: ../View/User/ModificarEvento.php?exito=1');
            }else{
                $Evento->consultarEvento();
                header('location: ../View/Admin/ModificarEvento.php?exito=1');
            }
        }else{
            if($_SESSION["tipo"]!="admin"){
                header('location: ../View/User/ModificarEvento.php?error=1');
            }else{
                header('location: ../View/Admin/ModificarEvento.php?error=1');
            }

        }
        break;
    //**************************** FIN CRUD Eventos ****************************\

    /**
        *Evento Acceder a Evento: Permite al usuario inscribirse a un Evento publico.
     */
    case $lang['Acceder a Evento']:
        $pertenece=false;

        $Evento = new Evento($_POST["nombre_evento"],"","","","","","","");
        $Evento->checkPertenece();

        $pertenece=$_SESSION["pertenece"];

        if($pertenece==false){
            $Evento->accederEvento();
            $Evento->misEventos();
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisEventos.php');
            }else{
                header('location: ../View/Admin/MisEventos.php');
            }
        }else{
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisEventos.php');
            }else{
                header('location: ../View/Admin/MisEventos.php');
            }
        }
        break;

    /**
        * Evento Solicitar acceso: Solicita la entrada a un evento.
     */
    case $lang['Solicitar acceso']:
        $peticion=false;
        $Evento=new Evento($_POST["nombre_evento"],"","","","","","",$_POST["nick_creador"]);

        $Evento->checkSolicitud();
        $peticion=$_SESSION["peticion"];


        if($peticion==false){
            $Evento->solicitudAccesoEvento();
            $Evento->misEventos();

           if($_SESSION["tipo"]=="user") {
               header('location: ../View/User/MisEventos.php');
           }else{
               header('location: ../View/Admin/MisEventos.php');
           }
        }else{
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisEventos.php');
            }else{
                header('location: ../View/Admin/MisEventos.php');
            }
        }
        break;

    /**
        *Evento MisSolicitudes: Muestra las solicitudes que tiene un usuario en lo referente a eventos.
     */
    case $lang['MisSolicitudes']:
        $Evento = new Evento();
        $Evento->mostrarSolicitudes();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudesEventos.php');
        }else{
            header('location: ../View/Admin/MisSolicitudesEventos.php');
        }
        break;



    /**
        * Evento Aceptar usuario: Se acepta la solicitud de un usuario para entrar a un evento
     */
    case $lang['Aceptar usuario']:

        $nombre_Evento=$_POST["nombre_evento_invitacion"];
        $nick_emisor=$_POST["nick_emisor"];
        $Evento = new Evento();
        $Evento->aceptarUsuarioEvento($nombre_Evento,$nick_emisor,$_SESSION["nick"]);
        $Evento->mostrarSolicitudes();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudes.php');
        }else{
            header('location: ../View/Admin/MisSolicitudes.php');
        }
        break;


    /**
        * Evento Rechazar usuario: No se permite la entrada de un usuario a un evento
     */
    case $lang['Rechazar usuario']:

        $nombre_Evento=$_POST["nombre_evento_invitacion"];
        $nick_emisor=$_POST["nick_emisor"];
        $Evento = new Evento();
        $Evento->rechazarUsuarioEvento($nombre_Evento,$nick_emisor,$_SESSION["nick"]);
        $Evento->mostrarSolicitudes();

        if($_SESSION["tipo"]!="admin"){
            header('location: ../View/User/MisSolicitudes.php');
        }else{
            header('location: ../View/Admin/MisSolicitudes.php');
        }
        break;
    /**
        * Evento Invitar Amigo: Invita a un amigo a pertenecer a un evento
     */
    case $lang['Invitar Amigo']:
        $peticion=false;
        $evento=new Evento($_POST["nombre_evento"],"","","","","","","");
        $peticion=$evento->checkSolicitudAmigoEvento($_POST["nombre_evento"],$_SESSION["nick"],$_POST["nombre_amigo"]);


        if($peticion==false){
            $evento->enviarSolicitudAmigo($_POST["nombre_evento"],$_SESSION["nick"],$_POST["nombre_amigo"]);
            $evento->mostrarSolicitudes();

            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisSolicitudesEventos.php');
            }else{
                header('location: ../View/Admin/MisSolicitudesEventos.php');
            }
        }else{
            if($_SESSION["tipo"]=="user") {
                header('location: ../View/User/MisEventos.php?error_invitar_amigo=true');
            }else{
                header('location: ../View/Admin/MisEventos.php?error_invitar_amigo=true');
            }
        }
        break;

    default:
        echo "La acción requerida no existe";
        break;
}
?>
