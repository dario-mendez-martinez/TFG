<?php
require_once'ScriptsAcceso/Redireccion.php';
?>

<!DOCTYPE HTML>
<html>
<head>
	<title>Social Network</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<link href="../Recursos/styleUser/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
	<!-- Custom Theme files -->
	<link href="../Recursos/styleUser/css/style.css" rel="stylesheet" type="text/css" media="all"/>
	<!--js-->
	<script src="../Recursos/styleUser/js/jquery-2.1.1.min.js"></script> 
	<!--icons-css-->
	<link href="../Recursos/styleUser/css/font-awesome.css" rel="stylesheet"> 
	<!--Google Fonts-->
	<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
	<!--static chart-->
</head>
<body>	
<div class="login-page">
    <div class="login-main">  	
    	 <div class="login-head">
			</div>
			<div class="login-block">
				<form role="form" action="../Controller/UsuarioController.php" method="post">
					
					<?php if(isset($_GET["registro_exito"]) &&  $_GET["registro_exito"]=="true"){ ?>
					<div class="alert alert-success alert-dismissable">
              			<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
              			Todo el registro ha sido correcto, ya puede acceder a la plataforma. 
          			</div>
					<?php }?>

					<input type="text" name="nick" placeholder="Nick">
					<input type="password" name="password" class="lock" placeholder="Contraseña">
					
					<?php if(isset($_GET["error_login"]) &&  $_GET["error_login"]=="true"){?>
						<div class="alert alert-danger alert-dismissable">
	                		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
	               			¡Su Nick o contraseña no son correctos!
	               		</div>
					<?php }?>
					
					<div class="forgot-top-grids">
						<div class="forgot">
							<a href="#">Forgot password?</a>
						</div>
						<div class="clearfix"> </div>
					</div>
					<input type="submit" name="accion" id="button" value="Login"></a>	
					<h3>Aún no eres miembro?<a href="User/Registro.php"> Registrate ahora</a></h3>				
				</form>
			</div>
      </div>
</div>

<!--scrolling js-->
		<script src="../Recursos/styleUser/js/jquery.nicescroll.js"></script>
		<script src="../Recursos/styleUser/js/scripts.js"></script>
		<!--//scrolling js-->
<script src="../Recursos/styleUser/js/bootstrap.js"> </script>
<!-- mother grid end here-->
</body>
</html>


                      
						
