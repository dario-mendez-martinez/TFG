<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$UPublicaciones = $_SESSION["consultarPublicacion"];
$comentario_padre=$_SESSION["comentarios_padre"];
$comentario_hijo = $_SESSION["comentarios_hijo"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <?php if(isset($UPublicaciones)){ ?>
                <?php foreach ($UPublicaciones as $data){ ?>
                    <!-- Publicacion -->
                    <div class="row">
                        <!-- /.col -->
                        <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <div class="tab-content listar-publicacion-content">
                                        <div class="active tab-pane" id="activity">
                                            <!-- Post -->
                                            <div class="post">
                                                <div class="user-block">
                                        <span class="username">
                                            <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                        </span>
                                                    <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                                </div>
                                                <!-- /.user-block -->
                                                <div class="user-block contenido-publicacion-listar">
                                                    <p><?php echo $data['contenido']?></p>
                                                </div>
                                                <?php
                                                $codigo_publicacion=$data['codigo_publicacion']
                                                ?>
                                                <!--<ul class="list-inline">
                                                    <li>
                                                        <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                        <input type="submit" class="link-black text-sm" name="accion" value="Comentar"/>
                                                    </li>
                                                </ul>-->
                                            </div>
                                            <!-- /.post -->

                                            <!-- /.post -->
                                        </div>

                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.nav-tabs-custom -->
                            </div>
                        </form>
                    </div>
                    <!-- Publicacion -->
                <?php } ?>
            <?php } ?>

            <section>
            <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content listar-publicacion-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="post">
                                    <!-- /.user-block -->
                                    <div class="user-block contenido-publicacion-listar">
                                        <textarea class="input-modificar" name="comentario" required></textarea>
                                    </div>
                                    <ul class="list-inline">
                                        <li class="">
                                            <input type="hidden" name="codigo_publicacion" value="<?php echo $codigo_publicacion?>"/>
                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Comentar']?>"/>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.post -->

                                <!-- /.post -->
                            </div>

                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
            </form>
            </section>

            <section class="">
                <h2>
                    <?php echo $lang['Comentarios']?>
                    <small></small>
                </h2>
            </section>
            <?php if(isset($comentario_padre)){ ?>
                <?php foreach ($comentario_padre as $data){ ?>
                    <?php if(!$data['codigo_comentario_padre']){ ?>
                    <!-- Publicacion -->
                    <div class="row">
                        <!-- /.col -->
                        <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                            <div class="col-md-6">
                                <div class="nav-tabs-custom">
                                    <div class="tab-content listar-publicacion-content">
                                        <div class="active tab-pane" id="activity">
                                            <!-- Post -->
                                            <div class="post">
                                                <div class="user-block">
                                                    <span class="username">
                                                        <a href="#"><?php echo $lang['usuario-solo']?>: <?php echo $data['nick_emisor']?></a>
                                                    </span>
                                                    <span class="description"><?php echo $lang['hora-comentario']?> - <?php echo $data['fecha_comentario']?></span>
                                                </div>
                                                <!-- /.user-block -->
                                                <div class="user-block contenido-publicacion-listar">
                                                    <p><?php echo $data['comentario']?></p>
                                                </div>
                                                    <?php if(isset($comentario_hijo)){ ?>
                                                        <?php foreach ($comentario_hijo as $data_hijo){ ?>
                                                            <?php if($data_hijo['codigo_comentario_padre']==$data['codigo_comentario']){ ?>
                                                                <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                                                                    <div class="col-md-12">
                                                                        <div class="nav-tabs-custom">
                                                                            <div class="tab-content listar-publicacion-content comentario-hijo">
                                                                                <div class="active tab-pane" id="activity">
                                                                                    <!-- Post -->
                                                                                    <div class="post">
                                                                                        <!-- /.user-block -->
                                                                                        <div class="user-block contenido-publicacion-listar">
                                                                                            <p><?php echo $data_hijo['comentario']?></p>
                                                                                        </div>
                                                                                        <ul class="list-inline">
                                                                                            <li class="">
                                                                                                <input type="hidden" name="codigo_publicacion" value="<?php echo $codigo_publicacion?>"/>
                                                                                                <?php if($data_hijo['nick_emisor']==$_SESSION['nick']){ ?>
                                                                                                    <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Comentario']?>"/>
                                                                                                <?php } ?>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <!-- /.post -->

                                                                                    <!-- /.post -->
                                                                                </div>

                                                                            </div>
                                                                            <!-- /.tab-content -->
                                                                        </div>
                                                                        <!-- /.nav-tabs-custom -->
                                                                    </div>
                                                                </form>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <div class="user-block contenido-publicacion-listar">
                                                    <textarea class="input-modificar" name="comentario"></textarea>
                                                </div>
                                                <ul class="list-inline">
                                                    <li>
                                                        <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                        <input type="hidden" name="codigo_comentario" value="<?php echo $data['codigo_comentario']?>"/>
                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Comentario']?>"/>
                                                    </li>
                                                    <li class="pull-right">
                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Responder']?>"/>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.post -->

                                            <!-- /.post -->
                                        </div>

                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.nav-tabs-custom -->
                            </div>
                        </form>
                    </div>
                    <!-- Publicacion -->
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>