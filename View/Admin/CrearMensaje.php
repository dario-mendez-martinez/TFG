<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';
$LUsuarios = $_SESSION["listarUsuarios"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['Crear-Mensaje-titulo']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                        <!-- Datos de usuario -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <div>
                                    <h3 class="profile-username text-center"><?php echo $lang['Introduzca-datos-mensaje']?></h3>
                                </div>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item contenido-publicacion">
                                        <b><?php echo $lang['Contenido del mensaje']?>: </b> <textarea class="input-modificar pull-right contenido-publicacion-input" name="mensaje" required></textarea>
                                    </li>
                                    <?php if(isset($LUsuarios)){ ?>
                                        <li class="list-group-item">
                                            <b><?php echo $lang['usuario-solo']?>: </b>
                                            <select class="input-modificar pull-right" name="nick_receptor">
                                                <option value=""></option>
                                                <?php foreach ($LUsuarios as $data) { ?>
                                                    <option value="<?php echo $data['nick'] ?>"><?php echo $data['nick'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </li>
                                    <?php } ?>
                                    <li class="li-botones-modificar list-group-item">
                                        <input type="submit" class="boton-modificar-usuario btn btn-primary btn-block" name="accion" value="<?php echo $lang['Enviar Mensaje']?>"/>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <!-- Datos de usuario -->
                    </form>
                </div>

            </div>
            <!-- /.row -->
            <?php if(isset($_GET["registro_grupo_vacio"]) &&  $_GET["registro_grupo_vacio"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>

            <?php if(isset($_GET["registro_grupo_error"]) &&  $_GET["registro_grupo_error"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>