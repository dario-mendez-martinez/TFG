<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';
$documentos=$_SESSION["documento"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1><?php echo $lang["solicitudes-documento"]; ?></h1>

        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content container-fluid ">
            <div class="box-tools pull-right">
                <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="file" class="input-modificar" name="documento"/>
                        <div class="input-group-btn">

                            <input type="submit" class="btn btn-default pull-right" name="accion" value="<?php echo $lang["Subir Documento"]; ?>"/><i class="fa fa-search"></i>

                        </div>
                    </div>
                    <form>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $lang["solicitudes-documento"]; ?></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th><?php echo $lang["Propietario"]; ?></th>
                                <th><?php echo $lang["Codigo de la publicacion"]; ?></th>
                                <th><?php echo $lang["Nombre del documento"]; ?></th>
                                <th><?php echo $lang["Fecha Aceptación"]; ?></th>
                                <th><?php echo $lang["Acciones"]; ?></th>
                            </tr>
                            <?php if (isset($documentos)) { ?>
                                <form role="form1" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="POST">
                                        
                                <?php foreach ($documentos as $data) { ?>
                                   <tr>
                                            <td><?php echo $data["nick_propietario"] ?> <input type="hidden" class="btn btn-info" name="nick_propietario" value="<?php echo $data["nick_propietario"] ?>"></td>
                                            <td><?php echo $data["codigo_publicacion"] ?> <input type="hidden" class="btn btn-info" name="codigo_publicacion" value="<?php echo $data["codigo_publicacion"] ?>"></td>
                                            <td><?php echo substr($data["localizacion"], 36) ?> <input type="hidden" class="btn btn-info" name="localizacion" value="<?php echo $data["localizacion"] ?>"></td>
                                            <td><?php echo $data["fecha_subida_documento"] ?> <input type="hidden" class="btn btn-info" name="fecha_subida_documento" value="<?php echo $data["fecha_subida_documento"] ?>"></td>
                                            <td>
                                                <input type="hidden" class="btn btn-info" name="codigo_documento" value="<?php echo $data["codigo_documento"] ?>">
                                                <a href="<?php echo $data['localizacion'] ?>" class="btn btn-info"><?php echo $lang["Ver documento"]; ?></a>
                                                <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Eliminar documento"]; ?>"/>
                                            </td>
                                        </tr>
                                    
                                <?php } ?>
                                </form>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>