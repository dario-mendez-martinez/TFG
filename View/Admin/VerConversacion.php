<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$Conversacion = $_SESSION["conversacion"];
$flag=false;
$nick_receptor="";
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <?php if(isset($Conversacion)){ ?>
                <?php foreach ($Conversacion as $data){ ?>
                    <!-- Publicacion -->
                    <div class="row">
                        <!-- /.col -->
                        <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <div class="tab-content listar-publicacion-content">
                                        <div class="active tab-pane" id="activity">
                                            <!-- Post -->
                                            <div class="post">
                                                <div class="user-block">
                                        <span class="username">
                                            <a href="#">Usuario: <?php echo $data['nick_emisor']?></a>
                                        </span>
                                                    <span class="description">Time - <?php echo $data['fecha_envio_mensaje']?></span>
                                                </div>
                                                <!-- /.user-block -->
                                                <div class="user-block contenido-publicacion-listar">
                                                    <p><?php echo $data['mensaje']?></p>
                                                </div>
                                                <?php
                                                    if($data['codigo_mensaje_padre']) {
                                                        $codigo_mensaje_padre = $data['codigo_mensaje_padre'];
                                                    }else{
                                                        $codigo_mensaje_padre = $data['codigo_mensaje'];
                                                    }
                                                    if($data['nick_receptor']!=$_SESSION['nick']){
                                                        $nick_receptor=$data['nick_receptor'];
                                                    }

                                                    if($data['nick_emisor']==$_SESSION['nick'] || $data['nick_receptor']==$_SESSION['nick']){
                                                        $flag=true;
                                                    }
                                                ?>
                                                <?php if($data['nick_emisor']==$_SESSION['nick']){ ?>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <input type="hidden" name="codigo_mensaje" value="<?php echo $data['codigo_mensaje']?>"/>
                                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Mensaje']?>"/>
                                                        </li>
                                                    </ul>
                                                <?php } ?>
                                                <!--<ul class="list-inline">
                                                    <li>
                                                        <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                        <input type="submit" class="link-black text-sm" name="accion" value="Comentar"/>
                                                    </li>
                                                </ul>-->
                                            </div>
                                            <!-- /.post -->

                                            <!-- /.post -->
                                        </div>

                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.nav-tabs-custom -->
                            </div>
                        </form>
                    </div>
                    <!-- Publicacion -->
                <?php } ?>
            <?php } ?>

            <?php if($flag){ ?>
            <section>
                <div class="row">
                <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <div class="tab-content listar-publicacion-content">
                                <div class="active tab-pane" id="activity">
                                    <!-- Post -->
                                    <div class="post">
                                        <!-- /.user-block -->
                                        <div class="user-block contenido-publicacion-listar">
                                            <textarea class="input-modificar" name="mensaje" required></textarea>
                                        </div>
                                        <ul class="list-inline">
                                            <li class="">
                                                <input type="hidden" name="codigo_mensaje_padre" value="<?php echo $codigo_mensaje_padre?>"/>
                                                <input type="hidden" name="nick_receptor" value="<?php echo $nick_receptor?>"/>
                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Responder Mensaje']?>"/>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.post -->

                                    <!-- /.post -->
                                </div>

                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
                </form>
                </div>
            </section>
            <?php } ?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>