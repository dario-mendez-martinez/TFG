<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';
$LGrupos = $_SESSION["listarGrupos"];
$Leventos = $_SESSION["listareventos"];
$Publicacion = $_SESSION["consultarPublicacion"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['Modificar Publicacion']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">
            <?php if(isset($Publicacion)){ ?>
                <?php foreach($Publicacion as $dataP){ ?>
            <div class="row">
                <div class="col-md-12">
                    <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                        <!-- Datos de usuario -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <div>
                                    <h3 class="profile-username text-center"><?php echo $lang['datos-publicacion']?></h3>
                                </div>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item contenido-publicacion">
                                        <b><?php echo $lang['post']?>: </b> <textarea class="input-modificar pull-right contenido-publicacion-input" name="contenido" required><?php echo $dataP['contenido'] ?></textarea>
                                    </li>
                                    <?php if(isset($LGrupos)){ ?>
                                        <li class="list-group-item">
                                            <b><?php echo $lang['grupo-solo']?>: </b>
                                            <select class="input-modificar pull-right" name="nombre_grupo_publicacion">
                                                <option value="<?php echo $dataP['nombre_grupo_publicacion'] ?>"><?php echo $dataP['nombre_grupo_publicacion'] ?></option>
                                                <?php foreach ($LGrupos as $data) { ?>
                                                    <option value="<?php echo $data['nombre_grupo'] ?>"><?php echo $data['nombre_grupo'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </li>
                                    <?php } ?>
                                    <?php if(isset($Leventos)){ ?>
                                        <li class="list-group-item">
                                            <b><?php echo $lang['evento-solo']?>: </b>
                                            <select class="input-modificar pull-right" name="nombre_evento_publicacion">
                                                <option value="<?php echo $dataP['nombre_evento_publicacion'] ?>"><?php echo $dataP['nombre_evento_publicacion'] ?></option>
                                                <?php foreach ($Leventos as $data) { ?>
                                                    <option value="<?php echo $data['nombre_evento'] ?>"><?php echo $data['nombre_evento'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </li>
                                    <?php } ?>
                                    <li class="list-group-item">
                                        <b><?php echo $lang['documento-solo']?>: </b> <input name="documento_usuario[]" multiple="multiple" type="file" />
                                    </li>
                                    <li class="li-botones-modificar list-group-item">
                                        <input type="hidden" name="codigo_publicacion" value="<?php echo $dataP['codigo_publicacion'] ?>"/>
                                        <input type="submit" class="boton-modificar-usuario btn btn-primary btn-block" name="accion" value="<?php echo $lang['Modificar']?>"/>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <!-- Datos de usuario -->
                    </form>
                </div>

            </div>
            <?php } ?>
            <?php } ?>
            <!-- /.row -->
            <?php if(isset($_GET["registro_grupo_vacio"]) &&  $_GET["registro_grupo_vacio"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>

            <?php if(isset($_GET["registro_grupo_error"]) &&  $_GET["registro_grupo_error"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>