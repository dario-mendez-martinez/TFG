<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['datos-evento-crear']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                        <!-- Datos de usuario -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <div>
                                    <h3 class="profile-username text-center"><?php echo $lang['datos-evento-crear']?></h3>
                                </div>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b><?php echo $lang['Nombre del Evento']?>: </b> <input class="input-modificar pull-right" name="nombre_evento" />
                                    </li>
                                    <li class="list-group-item">
                                        <b><?php echo $lang['Limite de usuarios']?>(Default 500): </b> <input class="input-modificar pull-right" name="limite_usuarios_Evento" />
                                    </li>
                                    <li class="list-group-item">
                                        <b><?php echo $lang['Tipo de Evento']?>: </b>
                                        <select class="input-modificar pull-right" name="tipo_Evento"/>
                                        <option value="0"><?php echo $lang['Publico']?></option>
                                        <option value="1"><?php echo $lang['Privado']?></option>
                                        </select>
                                    </li>
                                    <li class="list-group-item">
                                        <b><?php echo $lang['Descripcion del Evento']?>: </b> <input class="input-modificar pull-right" name="descripcion_evento"/>
                                    </li>
                                    <li class="list-group-item">
                                        <b><?php echo $lang['Foto del Evento']?>: </b> <input type="file" class="input-modificar pull-right" name="foto"/>
                                    </li>
                                    <li class="li-botones-modificar list-group-item">
                                        <input type="submit" class="boton-modificar-usuario btn btn-primary btn-block" name="accion" value="<?php echo $lang['Crear Evento']?>"/>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <!-- Datos de usuario -->
                    </form>
                </div>

            </div>
            <!-- /.row -->
            <?php if(isset($_GET["registro_Evento_vacio"]) &&  $_GET["registro_Evento_vacio"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>

            <?php if(isset($_GET["registro_Evento_error"]) &&  $_GET["registro_Evento_error"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>