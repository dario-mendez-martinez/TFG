<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$CEvento=$_SESSION["Consultarevento"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['Modificar evento']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <?php if(isset($CEvento)){?>
                        <?php foreach($CEvento as $data){?>
                            <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                                <!-- Datos de usuario -->
                                <div class="box box-primary">
                                    <div class="box-body box-profile">
                                        <div>
                                            <img class="profile-user-img img-responsive img-circle" src="<?php echo $data["imagen_evento"] ?>" alt="User profile picture">

                                            <h3 class="profile-username text-center"><?php echo $data["nombre_evento"] ?></h3>
                                            <input type="hidden" name="nombre_evento" value="<?php echo $data["nombre_evento"] ?>"/>
                                            <input type="hidden" name="nick_creador" value="<?php echo $data["nick_creador"] ?>"/>
                                        </div>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b><?php echo $lang['Limite de usuarios']?>(Default 500): </b> <input class="input-modificar pull-right" name="limite_usuarios_Evento" value="<?php echo $data["limite_usuarios_evento"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['Tipo de Evento']?>: </b>
                                                <select class="input-modificar pull-right" name="tipo_evento"/>
                                                <option value=""></option>
                                                <option value="0"><?php echo $lang['Publico']?></option>
                                                <option value="1"><?php echo $lang['Privado']?></option>
                                                </select>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['Descripcion del Evento']?>: </b> <input class="input-modificar pull-right" name="descripcion_Evento" value="<?php echo $data["descripcion_evento"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['Foto del Evento']?>: </b> <input type="file" class="input-modificar pull-right" name="imagen_Evento"/>
                                            </li>
                                            <li class="li-botones-modificar list-group-item">
                                                <input type="submit" class="boton-modificar-usuario btn btn-primary btn-block" name="accion" value="<?php echo $lang['Modificar']?>"/>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                                <!-- Datos de usuario -->
                            </form>
                        <?php } ?>
                    <?php } ?>
                </div>

            </div>
            <!-- /.row -->
            <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Las contraseñas no son iguales!
                </div>
                <?php if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){ ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                        La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres
                    </div>
                <?php }?>
            <?php }else if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres
                </div>
                <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                        ¡Las contraseñas no son iguales!
                    </div>
                <?php }?>
            <?php }?>

            <?php if(isset($_GET["error_foto"]) &&  $_GET["error_foto"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>

            <?php if(isset($_GET["exito"]) &&  $_GET["exito"]==true){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Todo ha ido genial!</h4>
                    Sus datos se han modificado correctamente.
                </div>
            <?php }?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>