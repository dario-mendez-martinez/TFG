<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$PUsuario = $_SESSION["perfil"];
$UPublicaciones = $_SESSION["listarPublicaciones"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['mi-perfil']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">
                    <?php if(isset($PUsuario)){?>
                        <?php foreach($PUsuario as $data){?>
                            <!-- Datos de usuario -->
                            <div class="box box-primary">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="<?php echo $data["foto"] ?>" alt="User profile picture">

                                    <h3 class="profile-username text-center"><?php echo $data["nombre_usuario"]." ".$data["apellido_usuario"] ?></h3>

                                    <p class="text-muted text-center"><?php echo $data["cargo"] ?></p>

                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <b><?php echo $lang["Fecha de registro"]; ?>: </b> <a class="pull-right"><?php echo $data["fecha_registro_usuario"] ?></a>
                                        </li>
                                    </ul>

                                    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                            <!-- Datos de usuario -->
                        <?php } ?>
                    <?php } ?>
                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">About Me</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

                            <p class="text-muted">
                                B.S. in Computer Science from the University of Tennessee at Knoxville
                            </p>

                            <hr>

                            <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                            <p class="text-muted">Malibu, California</p>

                            <hr>

                            <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                            <p>
                                <span class="label label-danger">UI Design</span>
                                <span class="label label-success">Coding</span>
                                <span class="label label-info">Javascript</span>
                                <span class="label label-warning">PHP</span>
                                <span class="label label-primary">Node.js</span>
                            </p>

                            <hr>

                            <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <?php if(isset($UPublicaciones)){ ?>
                        <?php foreach ($UPublicaciones as $data){ ?>
                            <!-- Publicacion -->
                            <!-- /.col -->

                            <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">

                                <div class="nav-tabs-custom">
                                    <div class="tab-content listar-publicacion-content">
                                        <div class="active tab-pane" id="activity">
                                            <!-- Post -->
                                            <div class="post">
                                                <div class="user-block">
                                        <span class="username">
                                            <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                        </span>
                                                    <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                                </div>
                                                <!-- /.user-block -->
                                                <div class="user-block contenido-publicacion-listar">
                                                    <p><?php echo $data['contenido']?></p>
                                                </div>

                                                <ul class="list-inline">
                                                    <li>
                                                        <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Modificar Publicacion']?>"/>
                                                    </li>
                                                    <li>
                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Publicacion']?>"/>
                                                    </li>
                                                    <li class="pull-right">
                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Ver Publicacion']?>"/>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.post -->

                                            <!-- /.post -->
                                        </div>

                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.nav-tabs-custom -->

                            </form>

                            <!-- Publicacion -->
                        <?php } ?>
                    <?php } ?>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>