<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';
$Leventos = $_SESSION["listareventos"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1><?php echo $lang['mis eventos titulo']?></h1>
            <?php if(isset($_GET["registro_evento_exito"]) &&  $_GET["registro_evento_exito"]==true){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Todo ha ido genial!</h4>
                    El evento se ha creado correctamente.
                </div>
            <?php }?>
            <!-- Filtros -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtrar Usuarios</h3>
                </div>

                <!-- form start -->
                <form role="form" enctype="multipart/form-data" action="../../Controller/eventoController.php" method="post">
                    <div class="box-body">
                        <div class="form-group col-md-2">
                            <label for="nick">Nombre evento</label>
                            <input type="text" class="form-control" name="nombre_evento" id="nombre_evento" placeholder="Introduzca el nombre del evento a buscar">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nombre">Nombre Creador evento</label>
                            <input type="text" class="form-control" name="nick_creador" placeholder="Introduzca el nombre del creador del evento a buscar">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <input type="submit" name="accion" class="btn-lg btn-primary" value="<?php echo $lang['Filtrar']?>">
                    </div>
                </form>
            </div>
            <!-- Filtros -->
            <!-- Filtros -->
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content container-fluid">
            <?php if (isset($Leventos)) { ?>

                <!-- Tamaño de los contenedores -->
                <?php foreach ($Leventos as $data) { ?>
                    <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                        <div class="col-md-4">
                            <!-- Contenedor -->
                            <div class="box box-widget widget-user-2">

                                <!-- Cabecera -->
                                <div class="widget-user-header bg">
                                    <!-- Imagen -->
                                    <div class="widget-user-image">
                                        <img class="img-circle" src="<?php echo $data['imagen_evento']?>" alt="User Avatar">
                                    </div>
                                    <!-- Imagen -->

                                    <!-- Nombre,apellidos y cargo -->
                                    <h3 class="widget-user-username"><?php echo $data['nombre_evento']?></h3>
                                    <input type="hidden" name="nombre_evento" value="<?php echo $data['nombre_evento']?>" />
                                    <h5 class="widget-user-desc"><?php echo $data['fecha_creacion_evento']?></h5>
                                    <!-- Nombre,apellidos y cargo -->
                                    <!-- evento de botones -->
                                    <div class="btn-group">
                                        <?php if(!isset($_GET["amigo"])){ ?>
                                            <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Ver evento"]; ?>">
                                            <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Eliminar evento"]; ?>">
                                            <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Modificar evento"]; ?>">
                                        <?php }else { ?>
                                            <input type="hidden" name="nombre_amigo" value="<?php echo $_GET["amigo"]?>" />
                                            <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Invitar Amigo"]; ?>">
                                        <?php } ?>

                                    </div>
                                    <!-- evento de botones -->
                                </div>
                                <!-- Cabecera -->

                                <!-- Más detalles del usuario -->
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li>
                                            <a><?php echo $lang['Limite de usuarios']?>:
                                                <span class="pull-right"><?php echo $data['limite_usuarios_evento']?></span>
                                                <input type="hidden" name="nick" value="<?php echo $data['nick']?>" />
                                            </a>
                                        </li>

                                        <li>
                                            <a><?php echo $lang['nick-creador']?>:
                                                <span class="pull-right"><?php echo $data['nick_creador']?></span>
                                                <input type="hidden" name="email" value="<?php echo $data['nick_creador']?>" />
                                            </a>
                                        </li>

                                        <li>
                                            <a><?php echo $lang['Tipo de Evento']?>:
                                                <?php if($data['tipo_evento']=='0'){ ?>
                                                    <span class="pull-right"><?php echo $lang['Publico']?></span>
                                                <?php }else{?>
                                                    <span class="pull-right"><?php echo $lang['Privado']?></span>
                                                <?php } ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- Más detalles del usuario -->

                            </div>
                            <!-- Contenedor -->
                        </div>
                    </form>
                <?php } ?>
                <!-- Tamaño de los contenedores -->

            <?php } ?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>