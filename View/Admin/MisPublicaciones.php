<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$UPublicaciones = $_SESSION["listarPublicaciones"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['mis publicaciones titulo']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <?php if(isset($UPublicaciones)){ ?>
                <?php foreach ($UPublicaciones as $data){ ?>
            <!-- Publicacion -->
            <div class="row">
                <!-- /.col -->
                <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content listar-publicacion-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="post">
                                    <div class="user-block">
                                        <span class="username">
                                            <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                        </span>
                                        <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                    </div>
                                    <!-- /.user-block -->
                                    <div class="user-block contenido-publicacion-listar">
                                        <p><?php echo $data['contenido']?></p>
                                    </div>

                                    <ul class="list-inline">
                                        <li>
                                            <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Modificar Publicacion']?>"/>
                                        </li>
                                        <li>
                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Publicacion']?>"/>
                                        </li>
                                        <li class="pull-right">
                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Ver Publicacion']?>"/>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.post -->

                                <!-- /.post -->
                            </div>

                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                </form>
            </div>
            <!-- Publicacion -->
        <?php } ?>
            <?php } ?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>