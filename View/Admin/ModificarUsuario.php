<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$PUsuario = $_SESSION["perfil"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1>
                <?php echo $lang['Modificar Usuario']?>
                <small></small>
            </h1>
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <?php if(isset($PUsuario)){?>
                        <?php foreach($PUsuario as $data){?>
                            <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                <!-- Datos de usuario -->
                                <div class="box box-primary">
                                    <div class="box-body box-profile">
                                        <div>
                                            <img class="profile-user-img img-responsive img-circle" src="<?php echo $data["foto"] ?>" alt="User profile picture">

                                            <h3 class="profile-username text-center"><?php echo $data["nick"] ?></h3>
                                            <input type="hidden" name="nick" id="nick" value="<?php echo $data["nick"] ?>"/>
                                        </div>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b><?php echo $lang['nombre-user']?>: </b> <input class="input-modificar pull-right" name="nombre" value="<?php echo $data["nombre_usuario"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['apellido-user']?>: </b> <input class="input-modificar pull-right" name="apellido" value="<?php echo $data["apellido_usuario"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['contraseña-user']?>: </b> <input type="password" class="input-modificar pull-right" name="password1" value=""/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['contraseña-r-user']?>: </b> <input type="password" class="input-modificar pull-right" name="password2" value=""/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['cargo-user']?>: </b> <input class="input-modificar pull-right" name="cargo" value="<?php echo $data["cargo"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['telefono-user']?>: </b> <input class="input-modificar pull-right" name="telefono" value="<?php echo $data["telefono"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['descripcion-texto']?>: </b> <input class="input-modificar pull-right" name="descripcion" value="<?php echo $data["descripcion_usuario"] ?>"/>
                                            </li>
                                            <li class="list-group-item">
                                                <b><?php echo $lang['foto-user']?>: </b> <input type="file" class="input-modificar pull-right" name="foto"/>
                                            </li>
                                            <li class="li-botones-modificar list-group-item">
                                                <input type="submit" class="boton-modificar-usuario btn btn-primary btn-block" name="accion" value="<?php echo $lang['Modificar']?>"/>

                                                <?php if($data["borrado_usuario"]=='0'){?>
                                                    <input type="button" class="boton-modificar-usuario btn btn-primary btn-block" data-toggle="modal" data-target="#modal-default"  name="accion" value="<?php echo $lang['Eliminar Usuario']?>"/>
                                                <?php }else{?>
                                                    <input type="submit" class="boton-modificar-usuario btn btn-primary btn-block" name="accion" value="<?php echo $lang['Volver a crear']?>"/>
                                                <?php }?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="modal" id="modal-default" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title">ATENCIÓN</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>¿Está seguro de eliminar el usuario?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                                    <button type="button" class="btn btn-primary" id="EliminarUsuario">Si</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                                <!-- Datos de usuario -->
                            </form>
                        <?php } ?>
                    <?php } ?>
                </div>

            </div>
            <!-- /.row -->
            <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Las contraseñas no son iguales!
                </div>
                <?php if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){ ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                        La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres
                    </div>
                <?php }?>
            <?php }else if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres
                </div>
                <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                        ¡Las contraseñas no son iguales!
                    </div>
                <?php }?>
            <?php }?>

            <?php if(isset($_GET["error_foto"]) &&  $_GET["error_foto"]==true){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
                    ¡Ha ocurrido un error al subir su imagen!
                </div>
            <?php }?>

            <?php if(isset($_GET["exito"]) &&  $_GET["exito"]==true){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Todo ha ido genial!</h4>
                    Sus datos se han modificado correctamente.
                </div>
            <?php }?>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>