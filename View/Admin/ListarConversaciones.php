<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$Conversacion = $_SESSION["conversacion"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1><?php echo $lang["conversaciones-titulo"]; ?></h1>

        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content container-fluid">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $lang["conversaciones-titulo"]; ?></h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar por nick">

                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-default"/><i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th><?php echo $lang["Nick Emisor"]; ?></th>
                                <th><?php echo $lang["Fecha Petición"]; ?></th>
                                <th><?php echo $lang["Acciones"]; ?></th>
                            </tr>
                            <?php if (isset($Conversacion)) { ?>
                                <?php foreach ($Conversacion as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_emisor"] ?> <input type="hidden" class="btn btn-info" name="nick_emisor" value="<?php echo $data["nick_emisor"] ?>"></td>
                                            <td><?php echo $data["nick_receptor"] ?> <input type="hidden" class="btn btn-info" name="nick_receptor" value="<?php echo $data["nick_receptor"] ?>"></td>
                                            <td>
                                                <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Ver Conversacion"]; ?>">
                                                <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Eliminar Conversacion"]; ?>">
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>