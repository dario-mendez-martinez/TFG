<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$Solicitudes = $_SESSION["listarSolicitudesRecibidas"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1><?php echo $lang['solicitudes titulo']?></h1>

        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content container-fluid">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $lang['solicitudes recibidas titulo']?></h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar por nick">

                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-default"/><i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th><?php echo $lang['evento-solo']?></th>
                                <th><?php echo $lang['Nick Emisor']?></th>
                                <th><?php echo $lang['Fecha Petición']?></th>
                                <th><?php echo $lang['Fecha Aceptacion']?></th>
                                <th><?php echo $lang['Acciones']?></th>
                            </tr>
                            <?php if (isset($Solicitudes)) { ?>
                                <?php foreach ($Solicitudes as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nombre_evento_invitacion"] ?> <input type="hidden" class="btn btn-info" name="nombre_evento_invitacion" value="<?php echo $data["nombre_evento_invitacion"] ?>"></td>
                                            <td><?php echo $data["nick_emisor"] ?> <input type="hidden" class="btn btn-info" name="nick_emisor" value="<?php echo $data["nick_emisor"] ?>"></td>
                                            <td><?php echo $data["fecha_invitacion_evento"] ?> <input type="hidden" class="btn btn-info" name="fecha_invitacion_evento" value="<?php echo $data["fecha_invitacion_evento"] ?>"></td>
                                            <td><?php echo $data["fecha_aceptacion_evento"] ?> <input type="hidden" class="btn btn-info" name="fecha_aceptacion_evento" value="<?php echo $data["fecha_aceptacion_evento"] ?>"></td>
                                            <td>
                                                <?php if($data["fecha_aceptacion_evento"]==NULL){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Aceptar usuario']?>">
                                                <?php } ?>

                                                <?php if($data["fecha_aceptacion_evento"]==NULL){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Rechazar usuario']?>">
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>