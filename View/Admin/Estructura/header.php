<!DOCTYPE html>
<html>
<head>
  <meta content="text/html" charset="utf-8">
  <meta http-equiv="Content-Type" content="IE=edge">
  <title>Social</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../Recursos/styleAdmin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../Recursos/styleAdmin/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../Recursos/styleAdmin/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../../Recursos/styleAdmin/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../Recursos/styleAdmin/dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="../../Recursos/styleAdmin/cssGenerales/style.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">