<?php
    require_once "../ScriptsAcceso/Language.php"
?>


<!-- Inicio del header -->
<header class="main-header">

  <!-- Inicio del logo -->
  <a href="indexAdmin.php" class="logo">
    <!-- Logo cuando se oculta la barra lateral -->
    <span class="logo-mini">b<b>Te</b></span>
    <!-- Logo normal -->
    <span class="logo-lg">be<b>Team</b></span>
  </a>
  <!-- Fin del logo -->

  <!-- Inicio del nav -->
  <nav class="navbar navbar-static-top" role="navigation">
      
    <!-- Botón para ocultar el menú lateral-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
      
      <!-- Botones de la parte derecha del header -->
    <div class="navbar-custom-menu">
        
      <ul class="nav navbar-nav">
          
          <!-- Botón de mensajes-->
       <!-- <li class="dropdown messages-menu">
             Menu toggle button
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">4</span>
          </a>
        </li>-->
        <!-- Fin botón de mensajes-->

        <!-- Botón para visitar perfil -->
        <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <img src="<?php echo $_SESSION["foto"]?>" class="user-image" alt="User Image">
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs"><?php echo $_SESSION["nick"]?></span>
          </a>
        </li>
        <!-- Fin botón para visitar perfil -->
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="hidden-xs"><?php echo $lang["cambiar-idioma"]; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <form class="form-inline" method="POST">
                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="lang">
                        <option value="es"><?php echo $lang["idioma-es"]; ?></option>
                        <option value="en"><?php echo $lang["idioma-en"]; ?></option>
                    </select>
                    <button type="submit" class="btn btn-primary"><?php echo $lang["cambiar"]; ?></button>
                </form>
              </li>
            </ul>
          </li>
        <!-- Botón de cerrar sesión -->
        <li>
          <a class="cerrarSesion" onclick="location.href='../../Controller/UsuarioController.php?accion=cerrarSesion'" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-power-off"></i>
          </a>
        </li>
        <!-- Fin botón de cerrar sesión -->

      </ul>
    </div>
    <!-- Fin botones de la parte derecha del header -->

  </nav>
   <!-- Fin de nav -->

</header>
<!-- Fin del header -->

<!--Inicio del wrapper(El cierre se encuentra en footer.php)-->
<div class="wrapper">

<!-- Menú de la izquiera -->
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      
      <!-- Botón 1 -->
      <li><!--  class="active" -->
        <a href="../../Controller/UsuarioController.php?accion=Perfil">
          <i class="fa fa-link"></i> 
          <span><?php echo $lang["mi-perfil"]; ?></span>
        </a>
      </li>
    
      <!-- Botón 3 -->
      <li class="treeview">
          
          <a href="#"><i class="fa fa-link"></i> 
            <span><?php echo $lang["usuarios"] ?></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>

            <ul class="treeview-menu">
              
              <li>
                <a href="../../Controller/UsuarioController.php?accion=ListarUsuarios"><?php echo $lang["Listar Usuarios"] ?></a>
              </li>
              <li>
                <a href="../../Controller/UsuarioController.php?accion=ListarAmigos"><?php echo $lang["Mis Amigos"] ?></a>
              </li>
              <li>
                <a href="../../Controller/UsuarioController.php?accion=ListarSolicitudesAmistad"><?php echo $lang["Solicitudes de Amistad"] ?></a>
              </li>
              <!--<li>
                <a href="#">Link in level 2</a>
              </li>-->

            </ul>
      </li>

      <li class="treeview">

         <a href="#"><i class="fa fa-link"></i>
                <span><?php echo $lang["grupos"]; ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
         </a>

            <ul class="treeview-menu">
                <li>
                    <a href="CrearGrupo.php"><?php echo $lang["Crear Grupo Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/GrupoController.php?accion=ListarGrupos"><?php echo $lang["Ver Grupos Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/GrupoController.php?accion=MisGrupos"><?php echo $lang["Mis grupos Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/GrupoController.php?accion=MisSolicitudes"><?php echo $lang["Solicitudes Menu"] ?></a>
                </li>


                <!--<li>
                  <a href="#">Link in level 2</a>
                </li>-->

            </ul>
      </li>

        <li class="treeview">

            <a href="#"><i class="fa fa-link"></i>
                <span><?php echo $lang["eventos"]; ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>

            <ul class="treeview-menu">
                <li>
                    <a href="CrearEvento.php"><?php echo $lang["Crear Evento Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/EventoController.php?accion=ListarEventos"><?php echo $lang["Ver Eventos Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/EventoController.php?accion=MisEventos"><?php echo $lang["Mis Eventos Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/EventoController.php?accion=MisSolicitudes"><?php echo $lang["Solicitudes Menu"] ?></a>
                </li>


                <!--<li>
                  <a href="#">Link in level 2</a>
                </li>-->

            </ul>
        </li>

        <li class="treeview">

            <a href="#"><i class="fa fa-link"></i>
                <span><?php echo $lang["publicaciones"]; ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>

            <ul class="treeview-menu">
                <li>
                    <a href="../../Controller/PublicacionController.php?accion=CrearPublicacion"><?php echo $lang["Crear Publicacion Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/PublicacionController.php?accion=MisPublicaciones"><?php echo $lang["Mis Publicaciones Menu"] ?></a>
                </li>
            </ul>
        </li>

        <li class="treeview">

            <a href="#"><i class="fa fa-link"></i>
                <span><?php echo $lang["mensajes"]; ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>

            <ul class="treeview-menu">
                <li>
                    <a href="../../Controller/UsuarioController.php?accion=CrearMensaje"><?php echo $lang["Crear Mensaje Menu"] ?></a>
                </li>

                <li>
                    <a href="../../Controller/UsuarioController.php?accion=MisConversaciones"><?php echo $lang["Listar Conversaciones Menu"] ?></a>
                </li>
            </ul>
        </li>

        <li class="treeview">

            <a href="#"><i class="fa fa-link"></i>
                <span><?php echo $lang["documentos"]; ?></span>
                <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>

            <ul class="treeview-menu">
                <li>
                    <a href="../../Controller/UsuarioController.php?accion=ListarDocumentos"><?php echo $lang["Listar Documentos Menu"] ?></a>
                </li>
            </ul>
        </li>

    </ul>
  </section>
</aside>
<!-- Fin menú de la izquiera -->