<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';
$LGrupos = $_SESSION["listarGrupos"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1></h1>
            <?php if(isset($_GET["registro_grupo_exito"]) &&  $_GET["registro_grupo_exito"]==true){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Todo ha ido genial!</h4>
                    El grupo se ha creado correctamente.
                </div>
            <?php }?>
            <!-- Filtros -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtrar Usuarios</h3>
                </div>

                <!-- form start -->
                <form role="form" enctype="multipart/form-data" action="../../Controller/GrupoController.php" method="post">
                    <div class="box-body">
                        <div class="form-group col-md-2">
                            <label for="nick">Nombre Grupo</label>
                            <input type="text" class="form-control" name="nombre_grupo" id="nick" placeholder="Introduzca el nombre del grupo a buscar">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nombre">Nombre Creador Grupo</label>
                            <input type="text" class="form-control" name="nick_creador" placeholder="Introduzca el nombre del creador del grupo a buscar">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <input type="submit" name="accion" class="btn-lg btn-primary" value="<?php echo $lang["Filtrar"]; ?>">
                    </div>
                </form>
            </div>
            <!-- Filtros -->
        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content container-fluid">
            <?php if (isset($LGrupos)) { ?>

                <!-- Tamaño de los contenedores -->
                <?php foreach ($LGrupos as $data) { ?>
                    <form role="form" enctype="multipart/form-data" action="../../Controller/GrupoController.php" method="post">
                        <div class="col-md-4">
                            <!-- Contenedor -->
                            <div class="box box-widget widget-user-2">

                                <!-- Cabecera -->
                                <div class="widget-user-header bg">
                                    <!-- Imagen -->
                                    <div class="widget-user-image">
                                        <img class="img-circle" src="<?php echo $data['imagen_grupo']?>" alt="User Avatar">
                                    </div>
                                    <!-- Imagen -->

                                    <!-- Nombre,apellidos y cargo -->
                                    <h3 class="widget-user-username"><?php echo $data['nombre_grupo']?></h3>
                                    <input type="hidden" name="nombre_grupo" value="<?php echo $data['nombre_grupo']?>" />
                                    <h5 class="widget-user-desc"><?php echo $data['fecha_creacion_grupo']?></h5>
                                    <!-- Nombre,apellidos y cargo -->

                                    <!-- Grupo de botones -->
                                    <div class="btn-group">
                                        <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Ver Grupo']?>">
                                        <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Modificar Grupo']?>">
                                        <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Acceder a grupo']?>">
                                        <input type="submit" class="btn btn-info eliminar" data-toggle="modal"  data-target="#modal-default" name="accion" value="<?php echo $lang['Eliminar grupo']?>" key="<?php echo $data['nombre_grupo'];?>" id="modaleliminargrupo"/>
                                    </div>
                                    <!-- Grupo de botones -->
                                </div>
                                <!-- Cabecera -->

                                <!-- Más detalles del usuario -->
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li>
                                            <a><?php echo $lang['Limite de usuarios']?>:
                                                <span class="pull-right"><?php echo $data['limite_usuarios_grupo']?></span>
                                                <input type="hidden" name="nick" value="<?php echo $data['nick']?>" />
                                            </a>
                                        </li>

                                        <li>
                                            <a><?php echo $lang['nick-creador']?>:
                                                <span class="pull-right"><?php echo $data['nick_creador']?></span>
                                                <input type="hidden" name="email" value="<?php echo $data['nick_creador']?>" />
                                            </a>
                                        </li>

                                        <li>
                                            <a><?php echo $lang['Tipo de Grupo']?>:
                                                <?php if($data['tipo_grupo']=='0'){ ?>
                                                    <span class="pull-right"><?php echo $lang['Publico']?></span>
                                                <?php }else{?>
                                                    <span class="pull-right"><?php echo $lang['Privado']?></span>
                                                <?php } ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- Más detalles del usuario -->

                            </div>
                            <!-- Contenedor -->
                        </div>
                    </form>
                <?php } ?>
                <!-- Tamaño de los contenedores -->

            <?php } ?>
        </section>
        <!-- Fin del contenido de la página-->
        <div class="modal" id="modal-default" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" id="cerrarModal">×</span></button>
                        <h4 class="modal-title">ATENCIÓN</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Está seguro de eliminar el grupo?</p>
                        <p id="nombre_grupo_eliminar"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="cerrarModalButton">No</button>
                        <button type="button" class="btn btn-primary" id="EliminarGrupo">Si</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>