<?php
  require_once'../ScriptsAcceso/Acceso.php';
  require_once'Estructura/header.php';
  require_once'Estructura/navAdmin.php';
  $LUsuarios = $_SESSION["listarUsuarios"];
?>

<!-- Inicio de la página -->
<div class="content-wrapper">
    
  <!-- Título de la página -->
  <section class="content-header">
    <h1>Usuarios</h1>

    <!-- Filtros -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Filtrar Usuarios</h3>
      </div>
      
      <!-- form start -->
      <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
        <div class="box-body">
          <div class="form-group col-md-2">
            <label for="nick">Nick</label>
            <input type="text" class="form-control" name="nick" id="nick" placeholder="Introduzca el nick a buscar">
          </div>
          <div class="form-group col-md-2">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" placeholder="Introduzca el nombre a buscar">
          </div>
          <div class="form-group col-md-2">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Introduzca el email a buscar">
          </div>
          <div class="form-group col-md-2">
            <label for="cargo">Cargo</label>
            <input type="text" class="form-control" name="cargo" id="cargo" placeholder="Introduzca el cargo a buscar">
          </div>
        </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" name="accion" class="btn-lg btn-primary" value="<?php echo $lang["Filtrar"]; ?>">
              </div>
            </form>
    </div>
    <!-- Filtros -->
  </section>
  <!-- Fin del título de la página -->

  <!-- Contenido de la página -->
  <section class="content container-fluid">
        <?php if (isset($LUsuarios)) { ?>

        <!-- Tamaño de los contenedores -->
        <?php foreach ($LUsuarios as $data) { ?>
        <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
            <div class="col-md-4">
              <!-- Contenedor -->
              <div class="box box-widget widget-user-2">

                <!-- Cabecera -->
                <div class="widget-user-header bg">
                  <!-- Imagen -->
                  <div class="widget-user-image">
                    <img class="img-circle" src="<?php echo $data['foto']?>" alt="User Avatar">
                  </div>
                  <!-- Imagen -->

                  <!-- Nombre,apellidos y cargo -->
                  <h3 class="widget-user-username"><?php echo $data['nombre_usuario']." ".$data['apellido_usuario']?></h3>
                  <input type="hidden" name="nombre" value="<?php echo $data['nombre_usuario']?>" />
                  <input type="hidden" name="nick" value="<?php echo $data['nick']?>" />
                  <input type="hidden" name="apellidos" value="<?php echo $data['apellido_usuario']?>" />
                  <h5 class="widget-user-desc"><?php echo $data['cargo']?></h5>
                  <input type="hidden" name="cargo" value="<?php echo $data['cargo']?>" />
                  <!-- Nombre,apellidos y cargo -->

                  <!-- Grupo de botones -->
                  <div class="btn-group">
                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Ver Usuario"]; ?>" >
                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Modificar Usuario"]; ?>">
                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Pedir amistad"]; ?>">
                  </div>
                  <!-- Grupo de botones -->
                </div>
                <!-- Cabecera -->

                <!-- Más detalles del usuario -->
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li>
                        <a><?php echo $lang["nick-user"]; ?>:
                          <span class="pull-right"><?php echo $data['nick']?></span>
                          <input type="hidden" name="nick" value="<?php echo $data['nick']?>" />
                        </a>
                    </li>

                    <li>
                      <a><?php echo $lang["email-user"]; ?>:
                        <span class="pull-right"><?php echo $data['email']?></span>
                        <input type="hidden" name="email" value="<?php echo $data['email']?>" />
                      </a>
                    </li>

                    <li>
                      <a><?php echo $lang["telefono-user"]; ?>:
                        <span class="pull-right"><?php echo $data['telefono']?></span>
                        <input type="hidden" name="telefono" value="<?php echo $data['telefono']?>" />
                      </a>
                    </li>

                    <li>
                      <a><?php echo $lang["descripcion-user"]; ?>:
                        <span class="pull-right"><?php echo $data['nick']?></span>
                        <input type="hidden" name="descripcion" value="<?php echo $data['nick']?>" />
                      </a>
                    </li>

                    <li>
                      <a><?php echo $lang["estado-user"]; ?>:
                          <?php if($data['borrado_usuario']=='0'){ ?>
                              <span class="pull-right"><?php echo $lang["NO ELIMINADO"]; ?></span>
                          <?php }else{?>
                              <span class="pull-right"><?php echo $lang["ELIMINADO"]; ?></span>
                          <?php } ?>
                      </a>
                    </li>
                  </ul>
                </div>
                <!-- Más detalles del usuario -->

              </div>
              <!-- Contenedor -->
            </div>
        </form>
        <?php } ?>
        <!-- Tamaño de los contenedores -->

        <?php } ?>
  </section>
  <!-- Fin del contenido de la página-->
  
</div>
  <!-- Fin de la página -->

<?php
  require_once'Estructura/footer.php';
?>