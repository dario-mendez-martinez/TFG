<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/navAdmin.php';

$SolicitudesEmitidas = $_SESSION["listarSolicitudesEmitidas"];
$SolicitudesRecibidas = $_SESSION["listarSolicitudesRecibidas"];
?>

    <!-- Inicio de la página -->
    <div class="content-wrapper">

        <!-- Título de la página -->
        <section class="content-header">
            <h1><?php echo $lang['solicitudes titulo']?></h1>

        </section>
        <!-- Fin del título de la página -->

        <!-- Contenido de la página -->
        <section class="content container-fluid">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $lang['solicitudes recibidas titulo']?></h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar por nick">

                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-default"/><i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th><?php echo $lang['usuario-solo']?></th>
                                <th><?php echo $lang['Fecha Petición']?></th>
                                <th><?php echo $lang['Acciones']?></th>
                            </tr>
                            <?php if (isset($SolicitudesRecibidas)) { ?>
                                <?php foreach ($SolicitudesRecibidas as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_peticion_amistad"] ?> <input type="hidden" class="btn btn-info" name="nick_peticion_amistad" value="<?php echo $data["nick_peticion_amistad"] ?>"></td>
                                            <td><?php echo $data["fecha_peticion_amistad"] ?> <input type="hidden" class="btn btn-info" name="fecha_peticion_amistad" value="<?php echo $data["fecha_invitacion_amistad"] ?>"></td>
                                            <td>
                                                <?php if($data["fecha_aceptacion_amistad"]==null){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Aceptar Amistad']?>">
                                                <?php } ?>

                                                <?php if($data["fecha_aceptacion_amistad"]==null){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Rechazar Amistad']?>">
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $lang['Solicitudes emitidas']?></h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar por nick">

                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-default"/><i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th><?php echo $lang['usuario-solo']?></th>
                                <th><?php echo $lang['Fecha Petición']?></th>
                            </tr>
                            <?php if (isset($SolicitudesEmitidas)) { ?>
                                <?php foreach ($SolicitudesEmitidas as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_receptor_amistad"] ?></td>
                                            <td><?php echo $data["fecha_peticion_amistad"] ?></td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </section>
        <!-- Fin del contenido de la página-->

    </div>
    <!-- Fin de la página -->

<?php
require_once'Estructura/footer.php';
?>