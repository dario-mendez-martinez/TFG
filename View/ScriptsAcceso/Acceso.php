<?php
session_start();

if (!$_SESSION['permitido'] ) {
    header('location: ../Login.php');
}
?>