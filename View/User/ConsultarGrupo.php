<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$Cgrupo=$_SESSION["ConsultarGrupo"];
$CusuariosGrupo=$_SESSION["ConsultarUsuariosGrupo"];
$UPublicaciones = $_SESSION["listarPublicaciones"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->
        <?php if(isset($Cgrupo)) { ?>

        <?php foreach ($Cgrupo as $data){?>
        <h2></h2>
        <!---728x90--->
        <div class="col-md-4 compose">
            <div>
                <div class="mail-profile">
                    <div class="widget-user-image">
                        <a><img class="img-circle foto-perfil" src="<?php echo $data["imagen_grupo"] ?>" alt=""></a>
                    </div>
                    <div class="mailer-name">
                        <h5>
                            <a href="#"><?php echo $data["nombre_grupo"] ?></a>
                            <input type="hidden" name="nombre_grupo" value="<?php echo $data["nombre_grupo"]  ?>"/>
                        </h5>
                        <h6>
                            <a href=""><?php echo $data["nick_creador"] ?></a>
                        </h6>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
                <div class="compose-bottom">
                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                        <nav class="nav-sidebar">
                            <ul class="nav tabs">
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['descripcion-texto']?>: <span class="atributo-descripcion"><?php  echo $data["descripcion_grupo"] ?></span>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </form>
                </div>
            </div>
            <?php }?>
            <?php } ?>
            <div class="amigos-usuario">
                <div class="mail-profile">
                    <div class="mail-pic foto-usuario">
                        <?php if(isset($CusuariosGrupo)){?>
                            <?php foreach($CusuariosGrupo as $data){?>
                                    <img class="img-circle foto-perfil" src="<?php echo $data["foto"] ?>" >
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="inbox">
            <div class="col-md-8 mailbox-content  tab-content tab-content-in">
                <div class="tab-pane active text-style" id="tab1">
                    <div class="mailbox-border">
                        <?php if(isset($UPublicaciones)){ ?>
                            <?php foreach ($UPublicaciones as $data){ ?>
                                <!-- Publicacion -->
                                <!-- /.col -->
                                <div class="col-md-12">
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">

                                        <div class="nav-tabs-custom">
                                            <div class="tab-content listar-publicacion-content">
                                                <div class="active tab-pane" id="activity">
                                                    <!-- Post -->
                                                    <div class="post">
                                                        <div class="user-block">
                                                            <div>
                                                            <span class="username">
                                                                <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                                            </span>
                                                            </div>
                                                            <div>
                                                                <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                                            </div>
                                                        </div>
                                                        <!-- /.user-block -->
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <p><?php echo $data['contenido']?></p>
                                                        </div>

                                                        <ul class="list-inline">
                                                            <li>
                                                                <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Modificar Publicacion']?>"/>
                                                            </li>
                                                            <li>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Publicacion']?>"/>
                                                            </li>
                                                            <li class="pull-right">
                                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Ver Publicacion']?>"/>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.post -->

                                                    <!-- /.post -->
                                                </div>

                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->

                                    </form>
                                </div>
                                <!-- Publicacion -->
                            <?php } ?>
                        <?php } ?>
                        </section>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
