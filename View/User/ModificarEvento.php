<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$Cevento=$_SESSION["Consultarevento"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->
        <?php if(isset($Cevento)) { ?>

            <?php foreach ($Cevento as $data){?>
                <h2><?php echo $lang['Modificar evento']?></h2>
                <!---728x90--->
                <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                    <div class="col-md-12 compose">
                        <div>

                            <div class="mail-profile">
                                <div class="col-md-2 widget-user-image">
                                    <a><img class="img-circle foto-perfil" src="<?php echo $data["imagen_evento"] ?>" alt=""></a>
                                </div>
                                <div class="col-md-6">
                                    <h3><?php echo $data["nombre_evento"]?></h3>
                                    <input type="hidden" name="nombre_evento" value="<?php echo $data["nombre_evento"] ?>"/>
                                    <input type="hidden" name="nick_creador" value="<?php echo $data["nick_creador"] ?>"/>
                                </div>
                                <div class="col-md-4">
                                    <div style="margin-top: 10px;" class="col-md-12">
                                        <input type="submit" class="b-ModificarUser btn btn-info" name="accion" value="<?php echo $lang['Modificar']?>">
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="clearfix"> </div>

                            <div class="compose-bottom">

                                <nav class="nav-sidebar">
                                    <ul class="nav tabs">
                                        <ul class="nav tabs">
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['Limite de usuarios']?>: <input class="input-modificar-usuario" name="limite_usuarios_evento" value="<?php echo $data["limite_usuarios_evento"] ?>"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['Tipo de Evento']?>:
                                                    <select class="input-modificar-usuario" name="tipo_evento">
                                                        <option value=""></option>
                                                        <option value="0"><?php echo $lang['Publico']?></option>
                                                        <option value="1"><?php echo $lang['Privado']?></option>
                                                    </select>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="">
                                                <a>
                                                    <i class="fa fa-pencil-square-o"></i><?php echo $lang['Descripcion del Evento']?>:  <input class="input-modificar-usuario" name="descripcion_evento"  value="<?php echo $data["descripcion_evento"] ?>"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="">
                                                <a>
                                                    <?php echo $lang['Foto del Evento']?>:  <input class="input-modificar-usuario" type="file" name="imagen_evento"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                        </ul>
                                </nav>

                            </div>

                            <div class="col-md-12 boton-modificar-user">

                            </div>
                        </div>
                </form>
            <?php }?>
        <?php } ?>

        <div>
            <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡Las contraseñas no son iguales!</span>
                </div>
                <?php if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){ ?>
                    <div style="margin-top: 10px;" class="alert alert-info alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                        <span>La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres</span>
                    </div>
                <?php }?>
            <?php }else if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres</span>
                </div>
                <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                    <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                        <span>¡Las contraseñas no son iguales!</span>
                    </div>
                <?php }?>
            <?php }?>

            <?php if(isset($_GET["error_foto"]) &&  $_GET["error_foto"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡Ha ocurrido un error al subir su imagen!</span>
                </div>
            <?php }?>

            <?php if(isset($_GET["exito"]) &&  $_GET["exito"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    Todo ha ido bien! Sus datos se han modificado. </div>
            <?php }?>
        </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
