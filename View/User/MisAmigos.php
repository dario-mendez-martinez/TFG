<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$LAmigos = $_SESSION["listarAmigos"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang["Amigos"]; ?></h2>

            <div class="filtros-usuario col-md-12">
                <div>
                    <h2>Filtros</h2>
                </div>
                <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                    <div class="form-group col-md-2">
                        <label for="nick">Nick</label>
                        <input type="text" class="form-control" name="nick" id="nick" placeholder="Introduzca el Nick a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduzca el Nombre a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Introduzca el Email a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="cargo">Cargo</label>
                        <input type="text" class="form-control" name="cargo" id="cargo" placeholder="Introduzca el cargo a buscar">
                    </div>

                    <div class="form-group col-md-12">
                        <input type="submit" name="accion" value="<?php echo $lang["Filtrar"]; ?>">
                    </div>
                </form>
            </div>
        </div>
        <!-- Titulo de la página -->

        <div>
            <?php if (isset($LAmigos)) { ?>
                <!-- Usuario -->
                <?php foreach ($LAmigos as $data) { ?>
                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                        <div class="col-md-3 product-grid ">
                            <!-- Contenedor -->
                            <div class="product-items">

                                <!-- Imagen -->
                                <div class="project-eff ">
                                    <img class="img-responsive" style="width: 100px; height: 100px;" src="<?php echo $data['foto']?>" alt="">
                                </div>
                                <!-- Imagen -->

                                <!-- Nick,Nombre,Apellidos,Cargo -->
                                <div class="produ-cost">
                                    <h4><?php echo $data['nick']?></h4>
                                    <input type="hidden" name="nick" value="<?php echo $data['nick']?>" />
                                    <h5><?php echo $data['nombre_usuario']." ".$data['apellido_usuario']?></h5>
                                    <h5><?php echo $data['cargo']?></h5>
                                </div>
                                <!-- Nick,Nombre,Apellidos,Cargo -->

                                <!-- Botones -->
                                <div class="btn-group">
                                    <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Ver Usuario"]; ?>">
                                    <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Eliminar Amigo"]; ?>">
                                    <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Invitar a grupo"]; ?>">
                                    <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Invitar a evento"]; ?>">
                                </div>
                                <!-- Botones -->

                            </div>
                            <!-- Contenedor -->
                        </div>
                        <!-- Usuario -->
                    </form>
                <?php } ?>


            <?php } ?>
        </div>
        <div class="clearfix"> </div>

    </div>

</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
