<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$Leventos = $_SESSION["listareventos"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang['mis eventos titulo']?></h2>

            <?php if(isset($_GET["registro_evento_exito"]) &&  $_GET["registro_evento_exito"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    Todo ha ido bien! El evento se ha creado. </div>
            <?php }?>

            <div class="filtros-usuario col-md-12">
                <div>
                    <h2>Filtros</h2>
                </div>
                <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                    <div class="form-group col-md-2">
                        <label for="nick">Nick</label>
                        <input type="text" class="form-control" name="nombre_evento" id="nombre_evento" placeholder="Introduzca el Nombre del evento a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduzca el Nombre del creador de evento a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <select class="input-modificar pull-right" name="tipo_evento"/>
                        <option value="0">Publico</option>
                        <option value="1">Privado</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <input type="submit" name="accion" value="<?php echo $lang['Filtrar']?>">
                    </div>
                </form>
            </div>
        </div>
        <!-- Titulo de la página -->

        <div>
            <?php if (isset($Leventos)) { ?>
                <!-- Usuario -->
                <?php foreach ($Leventos as $data) { ?>
                    <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
                        <div class="col-md-3 product-grid ">
                            <!-- Contenedor -->
                            <div class="product-items">

                                <!-- Imagen -->
                                <div class="project-eff ">
                                    <img class="img-responsive" style="width: 100px; height: 100px;" src="<?php echo $data['imagen_evento']?>" alt="">
                                </div>
                                <!-- Imagen -->

                                <!-- Nick,Nombre,Apellidos,Cargo -->
                                <div class="produ-cost">
                                    <h4><?php echo $data['nombre_evento']?></h4>
                                    <input type="hidden" name="nombre_evento" value="<?php echo $data['nombre_evento']?>" />
                                    <h5><?php echo $data['nick_creador']?></h5>
                                </div>
                                <!-- Nick,Nombre,Apellidos,Cargo -->

                                <!-- Botones -->
                                <div class="btn-group">
                                    <?php if(!isset($_GET["amigo"])){ ?>
                                        <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Ver evento"]; ?>">
                                        <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Modificar evento"]; ?>">
                                        <?php if($data["nick_creador"]==$_SESSION['nick']){ ?>
                                            <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Eliminar evento"]; ?>">
                                        <?php } ?>
                                    <?php }else { ?>
                                        <input type="hidden" name="nombre_amigo" value="<?php echo $_GET["amigo"]?>" />
                                        <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Invitar Amigo"]; ?>">
                                    <?php } ?>

                                </div>
                                <!-- Botones -->

                            </div>
                            <!-- Contenedor -->
                        </div>
                        <!-- Usuario -->
                    </form>
                <?php } ?>


            <?php } ?>
        </div>
        <div class="clearfix"> </div>

    </div>

</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
