<!DOCTYPE HTML>
<html>
<head>
<title>Registro</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="../../Recursos/styleUser/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="../../Recursos/styleUser/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="../../Recursos/styleUser/js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="../../Recursos/styleUser/css/font-awesome.css" rel="stylesheet">
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--//charts-->
</head>
<body>


<!--Inicio bloque de registro-->
<div class="signup-page-main">
     <div class="signup-main">  	
    	 <div class="signup-head">
			</div>
			<div class="signup-block">
				<form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
					<input type="text" name="nick" placeholder="Nick" required="">
					<?php if(isset($_GET["error_nick"]) &&  $_GET["error_nick"]==true){ ?>
						<div class="alert alert-danger alert-dismissable">
	                		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
	               			<span>¡El nick introducido ya existe en la plataforma!</span>
	               		</div>
					<?php }?>

					<input type="password" name="password1" placeholder="Contraseña" required="">
					<input type="password" name="password2" placeholder="Repetir Contraseña" required="">
					<?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
						<div class="alert alert-danger alert-dismissable">
	                		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
	               			<span>¡Las contraseñas no son iguales!</span>
	               		</div>
					<?php }else if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){?>
						<div class="alert alert-info alert-dismissable">
	                		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
	               			<span>La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres</span>
	               		</div>
					<?php }?>
					
					<input type="text" name="email" placeholder="Email" required="">
					<?php if(isset($_GET["error_mail"]) &&  $_GET["error_mail"]==true){ ?>
						<div class="alert alert-danger alert-dismissable">
	                		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
	               			<span>¡El email introducido ya existe en la plataforma!</span>
	               		</div>
					<?php }?>

					<input type="text" name="nombre" placeholder="Nombre" required="">
					<input type="text" name="apellido" placeholder="Apellidos" required="">
					<input type="text" name="cargo" placeholder="Cargo" required="">
					<input type="text" name="telefono" placeholder="Telefono" required="">					
					<div class="btn btn-default btn-file">
						<i class="fa fa-paperclip"></i>Foto de perfil
						<!--<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />-->
						<input type="file" name="foto">

					</div>
					<?php if(isset($_GET["error_foto"]) &&  $_GET["error_foto"]==true){ ?>
						<div class="alert alert-danger alert-dismissable">
	                		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
	               			<span>¡Ha ocurrido un error al subir su imagen!</span>
	               		</div>
					<?php }?>
					<div class="forgot-top-grids">
						<!--<div class="forgot-grid">
							<ul>
								<li>
									<input type="checkbox" id="terminos" name="terminos" value="">
									<label for="terminos"><span></span>Acepto los términos y condiciones</label>
								</li>
							</ul>
						</div>-->
						
						<div class="clearfix"> </div>
					</div>

					<input type="submit" name="accion" value="Registro" value="Registrarse">														
				</form>
				<div class="sign-down">
				<h5>Ya tienes una cuenta? <a href="../Login.php">Entra</a></h5>
				</div>
			</div>
    </div>
</div>
<!--Fin bloque de registro-->


<script src="../../Recursos/styleUser/js/jquery.nicescroll.js"></script>
<script src="../../Recursos/styleUser/js/scripts.js"></script>
<!--//scrolling js-->
<script src="../../Recursos/styleUser/js/bootstrap.js"> </script>
<!-- mother grid end here-->
</body>
</html>


                      
						
