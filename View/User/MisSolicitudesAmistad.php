<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';
$SolicitudesEmitidas = $_SESSION["listarSolicitudesEmitidas"];
$SolicitudesRecibidas = $_SESSION["listarSolicitudesRecibidas"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang['solicitudes titulo']?></h2>
            <div class="col-md-12 chit-chat-layer1-left">
                <div class="work-progres">
                    <div class="chit-chat-heading">

                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $lang['usuario-solo']?></th>
                                <th><?php echo $lang['Fecha Petición']?></th>
                                <th><?php echo $lang['Acciones']?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($SolicitudesRecibidas)) { ?>
                                <?php foreach ($SolicitudesRecibidas as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_peticion_amistad"] ?> <input type="hidden" class="btn btn-info" name="nick_peticion_amistad" value="<?php echo $data["nick_peticion_amistad"] ?>"></td>
                                            <td><?php echo $data["fecha_peticion_amistad"] ?> <input type="hidden" class="btn btn-info" name="fecha_peticion_amistad" value="<?php echo $data["fecha_invitacion_amistad"] ?>"></td>
                                            <td>
                                                <?php if($data["fecha_aceptacion_amistad"]==null){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Aceptar Amistad']?>">
                                                <?php } ?>

                                                <?php if($data["fecha_aceptacion_amistad"]==null){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Rechazar Amistad']?>">
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="pro-head">
            <h2>Solicitudes Enviadas</h2>
            <div class="col-md-12 chit-chat-layer1-left">
                <div class="work-progres">
                    <div class="chit-chat-heading">

                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $lang['usuario-solo']?></th>
                                <th><?php echo $lang['Fecha Petición']?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($SolicitudesEmitidas)) { ?>
                                <?php foreach ($SolicitudesEmitidas as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_receptor_amistad"] ?></td>
                                            <td><?php echo $data["fecha_peticion_amistad"] ?></td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Titulo de la página -->

            <div class="clearfix"> </div>

        </div>

    </div>
    <!--Fin contenido de la página-->


    <?php
    require_once 'Estructura/footer.php';
    ?>
