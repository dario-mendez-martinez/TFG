<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';
$LGrupos = $_SESSION["listarGrupos"];
$Leventos = $_SESSION["listareventos"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->

        <h2><?php echo $lang['Crear-publicacion-titulo']?></h2>
        <!---728x90--->
        <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
            <div class="col-md-12 compose">
                <div>

                    <div class="mail-profile">
                        <div class="col-md-4">
                            <h2></h2>
                        </div>
                        <div class="col-md-8">
                            <div style="margin-top: 10px;" class="col-md-12">
                                <input type="submit" class="b-ModificarUser btn btn-info" name="accion" value="<?php echo $lang['Publicar']?>">
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div class="clearfix"> </div>

                    <div class="compose-bottom">

                        <nav class="nav-sidebar">
                            <ul class="nav tabs">
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['post']?>: <textarea  type="text" class="input-modificar-usuario contenido-publicacion" name="contenido" required></textarea>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <?php if(isset($LGrupos)){ ?>
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['grupo-solo']?>:
                                        <select class="input-modificar-usuario" name="nombre_grupo_publicacion">
                                            <option value=""></option>
                                            <?php foreach ($LGrupos as $data) { ?>
                                            <option value="<?php echo $data['nombre_grupo'] ?>"><?php echo $data['nombre_grupo'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <?php } ?>
                                <?php if(isset($Leventos)){ ?>
                                    <li class="active">
                                        <a>
                                            <i class="fa fa-inbox"></i><?php echo $lang['evento-solo']?>:
                                            <select class="input-modificar-usuario" name="nombre_evento_publicacion">
                                                <option value=""></option>
                                                <?php foreach ($Leventos as $data) { ?>
                                                    <option value="<?php echo $data['nombre_evento'] ?>"><?php echo $data['nombre_evento'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class="">
                                    <a>
                                        <?php echo $lang['documento-solo']?>:  <input class="input-modificar-usuario" type="file" name="documento_usuario[]" multiple="multiple"/>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>

                    <div class="col-md-12 boton-modificar-user">

                    </div>
                </div>
        </form>


        <div>
            <?php if(isset($_GET["registro_grupo_vacio"]) &&  $_GET["registro_grupo_vacio"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡El nombre del grupo no puede estar vacio!</span>
                </div>
            <?php }?>
            <?php if(isset($_GET["registro_grupo_error"]) &&  $_GET["registro_grupo_error"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡El nombre del grupo ya existe!</span>
                </div>
            <?php }?>
        </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
