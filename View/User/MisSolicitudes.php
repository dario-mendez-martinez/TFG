<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$Solicitudes = $_SESSION["listarSolicitudesRecibidas"];
$SolicitudesEmitidas=$_SESSION["listarSolicitudesEmitidas"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang['solicitudes titulo']?></h2>
                <div class="col-md-12 chit-chat-layer1-left">
                    <div class="work-progres">
                        <div class="chit-chat-heading">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo $lang['nombre grupo peticon']?></th>
                                    <th><?php echo $lang['Nick Emisor']?></th>
                                    <th><?php echo $lang['Fecha Petición']?></th>
                                    <th><?php echo $lang['Fecha Aceptacion']?></th>
                                    <th><?php echo $lang['Acciones']?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($Solicitudes)) { ?>
                                    <?php foreach ($Solicitudes as $data) { ?>
                                        <form role="form" enctype="multipart/form-data" action="../../Controller/GrupoController.php" method="post">
                                            <tr>
                                                <td><?php echo $data["nombre_grupo_invitacion"] ?> <input type="hidden" class="btn btn-info" name="nombre_grupo_invitacion" value="<?php echo $data["nombre_grupo_invitacion"] ?>"></td>
                                                <td><?php echo $data["nick_emisor"] ?> <input type="hidden" class="btn btn-info" name="nick_emisor" value="<?php echo $data["nick_emisor"] ?>"></td>
                                                <td><?php echo $data["fecha_invitacion_grupo"] ?> <input type="hidden" class="btn btn-info" name="fecha_invitacion_grupo" value="<?php echo $data["fecha_invitacion_grupo"] ?>"></td>
                                                <td><?php echo $data["fecha_aceptacion_grupo"] ?> <input type="hidden" class="btn btn-info" name="fecha_aceptacion_grupo" value="<?php echo $data["fecha_aceptacion_grupo"] ?>"></td>
                                                <td>
                                                    <?php if($data["fecha_aceptacion_grupo"]==null){ ?>
                                                        <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Aceptar usuario']?>">
                                                    <?php } ?>

                                                    <?php if($data["fecha_aceptacion_grupo"]==null){ ?>
                                                        <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang['Rechazar usuario']?>">
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        </form>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>

        <div class="pro-head">
            <h2><?php echo $lang['solicitudes titulo']?></h2>
            <div class="col-md-12 chit-chat-layer1-left">
                <div class="work-progres">
                    <div class="chit-chat-heading">
                        Recent Followers
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $lang['nombre grupo peticon']?></th>
                                <th><?php echo $lang['Nick Emisor']?></th>
                                <th><?php echo $lang['Fecha Petición']?></th>
                                <th><?php echo $lang['Fecha Aceptacion']?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($SolicitudesEmitidas)) { ?>
                                <?php foreach ($SolicitudesEmitidas as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/GrupoController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nombre_grupo_invitacion"] ?></td>
                                            <td><?php echo $data["nick_receptor"] ?></td>
                                            <td><?php echo $data["fecha_invitacion_grupo"] ?></td>
                                            <td><?php echo $data["fecha_aceptacion_grupo"] ?></td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <!-- Titulo de la página -->

        <div class="clearfix"> </div>

    </div>

</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
