<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$ConversacionEmitida = $_SESSION["conversacion"];
$ConversacionRecibida=$_SESSION["conversacionRecibida"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang["conversaciones-titulo"]; ?></h2>
            <div class="col-md-12 chit-chat-layer1-left">
                <div class="work-progres">
                    <div class="chit-chat-heading">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $lang["Nick Emisor"]; ?></th>
                                <th><?php echo $lang["Fecha Petición"]; ?></th>
                                <th><?php echo $lang["Acciones"]; ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($ConversacionRecibida)) { ?>
                                <?php foreach ($ConversacionRecibida as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_emisor"] ?> <input type="hidden" class="btn btn-info" name="nick_emisor" value="<?php echo $data["nick_emisor"] ?>"></td>
                                            <td><?php echo $data["nick_receptor"] ?> <input type="hidden" class="btn btn-info" name="nick_receptor" value="<?php echo $data["nick_receptor"] ?>"></td>
                                            <td>
                                                <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Ver Conversacion"]; ?>">
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="pro-head">
            <h2><?php echo $lang["conversaciones-titulo"]; ?></h2>
            <div class="col-md-12 chit-chat-layer1-left">
                <div class="work-progres">
                    <div class="chit-chat-heading">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $lang["Nick Emisor"]; ?></th>
                                <th><?php echo $lang["Fecha Petición"]; ?></th>
                                <th><?php echo $lang["Acciones"]; ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($ConversacionEmitida)) { ?>
                                <?php foreach ($ConversacionEmitida as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_emisor"] ?> <input type="hidden" class="btn btn-info" name="nick_emisor" value="<?php echo $data["nick_emisor"] ?>"></td>
                                            <td><?php echo $data["nick_receptor"] ?> <input type="hidden" class="btn btn-info" name="nick_receptor" value="<?php echo $data["nick_receptor"] ?>"></td>
                                            <td>
                                                <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Ver Conversacion"]; ?>">
                                                <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Eliminar Conversacion"]; ?>">
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Titulo de la página -->

            <div class="clearfix"> </div>

        </div>

    </div>
    <!--Fin contenido de la página-->


    <?php
    require_once 'Estructura/footer.php';
    ?>
