<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$UPublicaciones = $_SESSION["listarPublicaciones"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <div class="col-md-12 mailbox-content  tab-content tab-content-in">
            <div class="tab-pane active text-style" id="tab1">
                <div class="mailbox-border">
                    <?php if(isset($UPublicaciones)){ ?>
                        <?php foreach ($UPublicaciones as $data){ ?>
                            <!-- Publicacion -->
                            <div class="row">
                                <!-- /.col -->
                                <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                                <div class="col-md-12">
                                    <div class="nav-tabs-custom">
                                        <div class="tab-content listar-publicacion-content">
                                            <div class="active tab-pane" id="activity">
                                                <!-- Post -->
                                                <div class="post">
                                                    <div class="user-block">
                                                        <div>
                                                            <span class="username">
                                                                <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                                            </span>
                                                        </div>
                                                        <div>
                                                            <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                                        </div>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <div class="user-block contenido-publicacion-listar">
                                                        <p><?php echo $data['contenido']?></p>
                                                    </div>

                                                    <ul class="list-inline">
                                                        <li>
                                                            <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Modificar Publicacion']?>"/>
                                                        </li>
                                                        <li>
                                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Publicacion']?>"/>
                                                        </li>
                                                        <li class="pull-right">
                                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Ver Publicacion']?>"/>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- /.post -->

                                                <!-- /.post -->
                                            </div>

                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- /.nav-tabs-custom -->
                                </div>
                                </form>
                            </div>
                            <!-- Publicacion -->
                        <?php } ?>
                    <?php } ?>
                    </section>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
