<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->

        <h2><?php echo $lang['datos-evento-crear']?></h2>
        <!---728x90--->
        <form role="form" enctype="multipart/form-data" action="../../Controller/EventoController.php" method="post">
            <div class="col-md-12 compose">
                <div>

                    <div class="mail-profile">
                        <div class="col-md-4">
                            <h2></h2>
                        </div>
                        <div class="col-md-8">
                            <div style="margin-top: 10px;" class="col-md-12">
                                <input type="submit" class="b-ModificarUser btn btn-info" name="accion" value="<?php echo $lang['Crear Evento']?>">
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div class="clearfix"> </div>

                    <div class="compose-bottom">

                        <nav class="nav-sidebar">
                            <ul class="nav tabs">
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['Nombre del Evento']?>: <input class="input-modificar-usuario" name="nombre_evento"/>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['Limite de usuarios']?>: <input class="input-modificar-usuario" name="limite_usuarios_evento" />
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['Tipo de Evento']?>:
                                        <select class="input-modificar-usuario" name="tipo_evento">
                                            <option value="0"><?php echo $lang['Publico']?></option>
                                            <option value="1"><?php echo $lang['Privado']?></option>
                                        </select>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li class="">
                                    <a>
                                        <i class="fa fa-pencil-square-o"></i><?php echo $lang['Descripcion del Evento']?>:  <input class="input-modificar-usuario" name="descripcion_evento" />
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li class="">
                                    <a>
                                        <?php echo $lang['Foto del Evento']?>:  <input class="input-modificar-usuario" type="file" name="imagen_evento"/>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>

                    <div class="col-md-12 boton-modificar-user">

                    </div>
                </div>
        </form>


        <div>
            <?php if(isset($_GET["registro_evento_vacio"]) &&  $_GET["registro_evento_vacio"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡El nombre del evento no puede estar vacio!</span>
                </div>
            <?php }?>
            <?php if(isset($_GET["registro_evento_error"]) &&  $_GET["registro_evento_error"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡El nombre del evento ya existe!</span>
                </div>
            <?php }?>
        </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
