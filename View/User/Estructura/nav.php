<?php
	require_once "../ScriptsAcceso/Language.php"
?>

<!-- Inicio de la página(Fin en footer.php) -->
<div class="page-container sidebar-collapsed">
   <div class="left-content">

	   <!-- Inicio del nav y menú lateral-->
	   <div class="mother-grid-inner">
            <!--Inicio del nav(solo) -->
			<div class="header-main">

					<!-- Inicio parte izquierda del nav -->
					<div class="header-left">

							<!-- Inicio logo -->
							<div class="logo-name">
									 <a href="../IndexUsuario.php"> <h1>beTeam</h1>
									<!--<img id="logo" src="" alt="Logo"/>-->
								  </a>
							</div>
							<!-- Fin logo -->

							<div class="clearfix"> </div>
					</div>
					<!-- Fin parte izquierda del nav -->

					<!-- Inicio Parte derecha del nav-->
					<div class="header-right">

						 	<!--Inicio mensajes y notificaciones -->
							<div class="profile_details_left">
								<ul>
                                    <li class="dropdown profile_details_drop" style="display: inline;">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="hidden-xs"><?php echo $lang["cambiar-idioma"]; ?></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <!-- User image -->
                                            <li class="user-header">
                                                <form class="form-inline" method="POST">
                                                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="lang">
                                                        <option value="es"><?php echo $lang["idioma-es"]; ?></option>
                                                        <option value="en"><?php echo $lang["idioma-en"]; ?></option>
                                                    </select>
                                                    <button type="submit" class="btn btn-primary"><?php echo $lang["cambiar"]; ?></button>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
								</ul>
								<div class="clearfix"> </div>
							</div>
							<!--Fin mensajes y notificaciones -->

							<!-- Inicio perfil de usuario -->
							<div class="profile_details">
								<ul>
									<li class="dropdown profile_details_drop">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<div class="profile_img" >
												<span class="prfil-img" ><img style="width: 30px;" src="<?php echo $_SESSION["foto"]?>" alt=""> </span>

												<div class="user-name" >
													<p><?php echo $_SESSION["nick"]?>
													<i class="fa fa-angle-down lnr"></i>
													<i class="fa fa-angle-up lnr"></i></p>
												</div>
												
												<div class="clearfix"></div>

											</div>
										</a>

										<ul class="dropdown-menu drp-mnu">
											<li>
                                                <?php if($lang["idioma"]=="castellano"){?>
												  <a class="botton_Modificar-Usuario" onclick="location.href='../../Controller/UsuarioController.php?accion=Modificar Usuario'">
                                                <?php } else {?>
                                                  <a class="botton_Modificar-Usuario" onclick="location.href='../../Controller/UsuarioController.php?accion=Modify User'">
                                                <?php } ?>
													<i class="fa fa-cog"></i>
                                                      <?php echo $lang["Modificar Usuario"]; ?></a>
											</li>

											<!--<li>
												<a href="#">
													<i class="fa fa-user"></i>
													Perfil</a>
											</li>-->

											<li>
												<a class="botton_cerrarSesion" onclick="location.href='../../Controller/UsuarioController.php?accion=cerrarSesion'">
													<i class="fa fa-sign-out"></i>
                                                    <?php echo $lang["Cerrar Sesion Menu"]; ?></a>
											</li>

										</ul>
									</li>
								</ul>
							</div>
							<!-- Fin perfil de usuario -->
							<div class="clearfix"> </div>
					</div>
						<!-- Fin Parte derecha del nav-->
				     <div class="clearfix"> </div>
			</div>
			<!-- Fin del nav -->

<!--Inicio menú lateral-->
<div class="sidebar-menu sidebar-collapsed">

	<!-- Inicio botón para ocultar -->
  	<div class="logo">
  		<a href="#" class="sidebar-icon">
  			<span class="fa fa-bars"></span>
  		</a>
  		<a href="#">
  			<span id="logo" ></span>
			      <!--<img id="logo" src="" alt="Logo"/>-->
		</a>
	</div>
	<!-- Fin botón para ocultar -->

	<!-- Inicio botones menú -->
	<div class="menu">
		<ul id="menu" >
		    <li id="menu-home" >
		    	<a href="../../Controller/UsuarioController.php?accion=Perfil">
		    		<i class="fa fa-tachometer"></i>
		    		<span><?php echo $lang["mi-perfil"]; ?></span>
		    	</a>
		    </li>

		    <li>
		    	<a href="#">
		    		<i class="fa fa-cogs"></i>
		    		<span><?php echo $lang["usuarios"] ?></span>
		    		<span class="fa fa-angle-right" style="float: right"></span>
		    	</a>
		          <ul>
		            <li>
		            	<a href="../../Controller/UsuarioController.php?accion=ListarUsuarios"><?php echo $lang["Listar Usuarios"] ?></a>
		            </li>
                    <li>
                        <a href="../../Controller/UsuarioController.php?accion=ListarAmigos"><?php echo $lang["Mis Amigos"] ?></a>
                    </li>
                    <li>
                        <a href="../../Controller/UsuarioController.php?accion=ListarSolicitudesAmistad"><?php echo $lang["Solicitudes de Amistad"] ?></a>
                    </li>
		            <!--<li>
		            	<a href="portlet.html">Portlets</a>
		            </li>-->
		          </ul>
		    </li>

		    <li id="menu-comunicacao" >
		    	<a href="#">
		    		<i class="fa fa-book nav_icon"></i>
		    		<span><?php echo $lang["grupos"]; ?></span>
		    		<span class="fa fa-angle-right" style="float: right"></span>
		    	</a>
		          <ul id="menu-arquivos" >
                    <li id="menu-mensagens">
                          <a href="CrearGrupo.php"><?php echo $lang["Crear Grupo Menu"] ?></a>
                    </li>
		            <li id="menu-mensagens">
		            	<a href="../../Controller/GrupoController.php?accion=ListarGrupos"><?php echo $lang["Ver Grupos Menu"] ?></a>
		            </li>
		            <li id="menu-arquivos" >
		            	<a href="../../Controller/GrupoController.php?accion=MisGrupos"><?php echo $lang["Mis grupos Menu"] ?></a>
		            </li>
		            <li id="menu-arquivos" >
		            	<a href="../../Controller/GrupoController.php?accion=MisSolicitudes"><?php echo $lang["Solicitudes Menu"] ?></a>
		            </li>
		          </ul>
		    </li>
            <li id="menu-comunicacao" >
                <a href="#">
                    <i class="fa fa-book nav_icon"></i>
                    <span><?php echo $lang["eventos"]; ?></span>
                    <span class="fa fa-angle-right" style="float: right"></span>
                </a>
                <ul id="menu-arquivos" >
                    <li id="menu-mensagens">
                        <a href="CrearEvento.php"><?php echo $lang["Crear Evento Menu"] ?></a>
                    </li>
                    <li id="menu-mensagens">
                        <a href="../../Controller/EventoController.php?accion=ListarEventos"><?php echo $lang["Ver Eventos Menu"] ?></a>
                    </li>
                    <li id="menu-arquivos" >
                        <a href="../../Controller/EventoController.php?accion=MisEventos"><?php echo $lang["Mis Eventos Menu"] ?></a>
                    </li>
                    <li id="menu-arquivos" >
                        <a href="../../Controller/EventoController.php?accion=MisSolicitudes"><?php echo $lang["Solicitudes Menu"] ?></a>
                    </li>
                </ul>
            </li>
            <li id="menu-comunicacao" >
                <a href="#">
                    <i class="fa fa-book nav_icon"></i>
                    <span><?php echo $lang["publicaciones"]; ?></span>
                    <span class="fa fa-angle-right" style="float: right"></span>
                </a>
                <ul id="menu-arquivos" >
                    <li id="menu-mensagens">
                        <a href="../../Controller/PublicacionController.php?accion=CrearPublicacion"><?php echo $lang["Crear Publicacion Menu"] ?></a>
                    </li>
                    <li id="menu-mensagens">
                        <a href="../../Controller/PublicacionController.php?accion=MisPublicaciones"><?php echo $lang["Mis Publicaciones Menu"] ?></a>
                    </li>
                </ul>
            </li>
            <li id="menu-comunicacao" >
                <a href="#">
                    <i class="fa fa-book nav_icon"></i>
                    <span><?php echo $lang["mensajes"]; ?></span>
                    <span class="fa fa-angle-right" style="float: right"></span>
                </a>
                <ul id="menu-arquivos" >
                    <li id="menu-mensagens">
                        <a href="../../Controller/UsuarioController.php?accion=CrearMensaje"><?php echo $lang["Crear Mensaje Menu"] ?></a>
                    </li>
                    <li id="menu-mensagens">
                        <a href="../../Controller/UsuarioController.php?accion=MisConversaciones"><?php echo $lang["Listar Conversaciones Menu"] ?></a>
                    </li>
                </ul>
            </li>
            <li id="menu-comunicacao" >
                <a href="#">
                    <i class="fa fa-book nav_icon"></i>
                    <span><?php echo $lang["documentos"]; ?></span>
                    <span class="fa fa-angle-right" style="float: right"></span>
                </a>
                <ul id="menu-arquivos" >
                    <li id="menu-mensagens">
                        <a href="../../Controller/UsuarioController.php?accion=ListarDocumentos"><?php echo $lang["Listar Documentos Menu"] ?></a>
                    </li>
                </ul>
            </li>
		</ul>
	</div>
	<!-- Fin botones menú -->
</div>
<!--Fin menú lateral-->

	<div class="clearfix">
	</div>

</div>
<!-- Fin nav y menú lateral -->

<!-- Inicio Script pàra ocultar el menú lateral -->
<script>
var toggle = true;

$(".sidebar-icon").click(function() {
    if (toggle)
    {
        $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
        setTimeout(function() {
            $("#menu span").css({"position":"relative"});
    }, 400);

    }
    else
    {
        $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
        $("#menu span").css({"position":"absolute"});
  }
    toggle = !toggle;
});
</script>
<!--Fin Script pàra ocultar el menú lateral-->