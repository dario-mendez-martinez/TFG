<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$documentos=$_SESSION["documento"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang["solicitudes-documento"]; ?></h2>
            <div class="box-tools pull-right">
                <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="file" class="input-modificar" name="documento" required/>
                        <div class="input-group-btn">

                            <input type="submit" class="btn btn-default pull-right" name="accion" value="<?php echo $lang["Subir Documento"]; ?>"/><i class="fa fa-search"></i>

                        </div>
                    </div>
                    <form>
            </div>

            <div class="col-md-12 chit-chat-layer1-left">
                <div class="work-progres">
                    <div class="chit-chat-heading">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $lang["Propietario"]; ?></th>
                                <th><?php echo $lang["Codigo de la publicacion"]; ?></th>
                                <th><?php echo $lang["Nombre del documento"]; ?></th>
                                <th><?php echo $lang["Fecha Aceptación"]; ?></th>
                                <th><?php echo $lang["Acciones"]; ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($documentos)) { ?>
                                <?php foreach ($documentos as $data) { ?>
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                        <tr>
                                            <td><?php echo $data["nick_propietario"] ?> <input type="hidden" class="btn btn-info" name="nick_propietario" value="<?php echo $data["nick_propietario"] ?>"></td>
                                            <td><?php echo $data["codigo_publicacion"] ?> <input type="hidden" class="btn btn-info" name="codigo_publicacion" value="<?php echo $data["codigo_publicacion"] ?>"></td>
                                            <td><?php echo substr($data["localizacion"], 36) ?> <input type="hidden" class="btn btn-info" name="localizacion" value="<?php echo $data["localizacion"] ?>"></td>
                                            <td><?php echo $data["fecha_subida_documento"] ?> <input type="hidden" class="btn btn-info" name="fecha_subida_documento" value="<?php echo $data["fecha_subida_documento"] ?>"></td>
                                            <td>
                                                <input type="hidden" class="btn btn-info" name="codigo_documento" value="<?php echo $data["codigo_documento"] ?>">
                                                <a href="<?php echo $data['localizacion'] ?>" class="btn btn-info"><?php echo $lang["Ver documento"]; ?></a>
                                                <?php if($data['nick_propietario']==$_SESSION['nick']){ ?>
                                                    <input type="submit" class="btn btn-info" name="accion" value="<?php echo $lang["Eliminar documento"]; ?>">
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--Fin contenido de la página-->


    <?php
    require_once 'Estructura/footer.php';
    ?>
