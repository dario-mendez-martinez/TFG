<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$LGrupos = $_SESSION["listarGrupos"];
?>
<!--Contenido de la página-->
<div class="inner-block">

    <div class="product-block">
        <!-- Titulo de la página -->
        <div class="pro-head">
            <h2><?php echo $lang['mis grupos titulo']?></h2>

            <?php if(isset($_GET["registro_grupo_exito"]) &&  $_GET["registro_grupo_exito"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    Todo ha ido bien! El grupo se ha creado. </div>
            <?php }?>

            <div class="filtros-usuario col-md-12">
                <div>
                    <h2>Filtros</h2>
                </div>
                <form role="form" enctype="multipart/form-data" action="../../Controller/GrupoController.php" method="post">
                    <div class="form-group col-md-2">
                        <label for="nick">Nick</label>
                        <input type="text" class="form-control" name="nombre_grupo" id="nombre_grupo" placeholder="Introduzca el Nombre del grupo a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Introduzca el Nombre del creador de grupo a buscar">
                    </div>
                    <div class="form-group col-md-2">
                        <select class="input-modificar pull-right" name="tipo_grupo"/>
                        <option value="0">Publico</option>
                        <option value="1">Privado</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <input type="submit" name="accion" value="<?php echo $lang['Filtrar']?>">
                    </div>
                </form>
            </div>
        </div>
        <!-- Titulo de la página -->

        <div>
            <?php if (isset($LGrupos)) { ?>
                <!-- Usuario -->
                <?php foreach ($LGrupos as $data) { ?>
                    <form role="form" enctype="multipart/form-data" action="../../Controller/GrupoController.php" method="post">
                        <div class="col-md-3 product-grid ">
                            <!-- Contenedor -->
                            <div class="product-items">

                                <!-- Imagen -->
                                <div class="project-eff ">
                                    <img class="img-responsive" style="width: 100px; height: 100px;" src="<?php echo $data['imagen_grupo']?>" alt="">
                                </div>
                                <!-- Imagen -->

                                <!-- Nick,Nombre,Apellidos,Cargo -->
                                <div class="produ-cost">
                                    <h4><?php echo $data['nombre_grupo']?></h4>
                                    <input type="hidden" name="nombre_grupo" id="nombre_grupo" value="<?php echo $data['nombre_grupo']?>" />
                                    <h5><?php echo $data['nick_creador']?></h5>
                                </div>
                                <!-- Nick,Nombre,Apellidos,Cargo -->

                                <!-- Botones -->
                                <div class="btn-group">
                                    <?php if(!isset($_GET["amigo"])){ ?>
                                        <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Ver Grupo"]; ?>">
                                        <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Modificar Grupo"]; ?>">
                                        <?php if($data["nick_creador"]==$_SESSION['nick']){ ?>
                                            <input type="submit" class="b-listarUser btn btn-info b-listarUser" data-toggle="modal" data-target="#modal-default" id="modalEliminarGrupo" key="<?php echo $data['nombre_grupo']?>" name="accion" value="<?php echo $lang["Eliminar grupo"]; ?>">
                                        <?php } ?>
                                    <?php }else { ?>
                                        <input type="hidden" name="nombre_amigo" value="<?php echo $_GET["amigo"]?>" />
                                        <input type="submit" class="b-listarUser btn btn-info b-listarUser" name="accion" value="<?php echo $lang["Invitar Amigo"]; ?>">
                                    <?php } ?>
                                </div>
                                <!-- Botones -->
                                <div class="modal" id="modal-default" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true" id="cerrarModal">×</span></button>
                                                <h4 class="modal-title">ATENCIÓN</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>¿Está seguro de eliminar el grupo?</p>
                                                <p id="nombre_grupo_eliminar"></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="cerrarModalButton">No</button>
                                                <button type="button" class="btn btn-primary" id="EliminarGrupo">Si</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            </div>
                            <!-- Contenedor -->
                        </div>
                        <!-- Usuario -->
                    </form>
                <?php } ?>


            <?php } ?>
        </div>
        <div class="clearfix"> </div>

    </div>

</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
