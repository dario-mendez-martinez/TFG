<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$UPublicaciones = $_SESSION["consultarPublicacion"];
$comentario_padre=$_SESSION["comentarios_padre"];
$comentario_hijo = $_SESSION["comentarios_hijo"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <div class="col-md-12 mailbox-content  tab-content tab-content-in">
            <div class="tab-pane active text-style" id="tab1">
                <div class="mailbox-border">
                    <?php if(isset($UPublicaciones)){ ?>
                        <?php foreach ($UPublicaciones as $data){ ?>
                            <!-- Publicacion -->
                            <div class="row">
                                <!-- /.col -->
                                <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                                    <div class="col-md-12">
                                        <div class="nav-tabs-custom">
                                            <div class="tab-content listar-publicacion-content">
                                                <div class="active tab-pane" id="activity">
                                                    <!-- Post -->
                                                    <div class="post">
                                                        <div class="user-block">
                                                            <div>
                                                            <span class="username">
                                                                <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                                            </span>
                                                            </div>
                                                            <div>
                                                                <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                                            </div>
                                                        </div>
                                                        <!-- /.user-block -->
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <p><?php echo $data['contenido']?></p>
                                                        </div>
                                                        <?php
                                                            $codigo_publicacion=$data['codigo_publicacion']
                                                        ?>
                                                        <!--<ul class="list-inline">
                                                            <li>
                                                                <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Modificar Publicacion"/>
                                                            </li>
                                                            <li>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Eliminar Publicacion"/>
                                                            </li>
                                                            <li class="pull-right">
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Comentarios"/>
                                                            </li>
                                                        </ul>-->
                                                    </div>
                                                    <!-- /.post -->

                                                    <!-- /.post -->
                                                </div>

                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->
                                    </div>
                                </form>
                            </div>
                            <!-- Publicacion -->
                        <?php } ?>
                    <?php } ?>
                    </section>
                </div>
            </div>
        </div>

        <div class="col-md-12 mailbox-content  tab-content tab-content-in">
            <div class="tab-pane active text-style" id="tab1">
                <div class="mailbox-border">
                            <!-- Publicacion -->
                            <div class="row">
                                <!-- /.col -->
                                <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                                    <div class="col-md-12">
                                        <div class="nav-tabs-custom">
                                            <div class="tab-content listar-publicacion-content">
                                                <div class="active tab-pane" id="activity">
                                                    <!-- Post -->
                                                    <div class="post">
                                                        <!-- /.user-block -->
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <textarea class="input-modificar" name="comentario" required></textarea>
                                                        </div>

                                                        <ul class="list-inline">
                                                            <li>
                                                                <input type="hidden" name="codigo_publicacion" value="<?php echo $codigo_publicacion?>"/>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Comentar']?>"/>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.post -->

                                                    <!-- /.post -->
                                                </div>

                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->
                                    </div>
                                </form>
                            </div>
                            <!-- Publicacion -->
                    </section>
                </div>
            </div>
        </div>

        <div class="clearfix"> </div>

        <div class="comentario">
            <h2>Comentarios:</h2>
        </div>
        <div class="col-md-12 mailbox-content tab-content tab-content-in comentario">
            <div class="tab-pane active text-style" id="tab1">
                <div class="mailbox-border">
                    <?php if(isset($comentario_padre)){ ?>
                    <?php foreach ($comentario_padre as $data){ ?>
                        <?php if(!$data['codigo_comentario_padre']){ ?>
                            <!-- Publicacion -->
                            <div class="row">
                                <!-- /.col -->
                                <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                                    <div class="col-md-12">
                                        <div class="nav-tabs-custom">
                                            <div class="tab-content listar-publicacion-content">
                                                <div class="active tab-pane" id="activity">
                                                    <!-- Post -->
                                                    <div class="post">
                                                        <div class="user-block">
                                                    <span class="username">
                                                        <a href="#"><?php echo $lang['usuario-solo']?>: <?php echo $data['nick_emisor']?></a>
                                                    </span>
                                                            <span class="description"><?php echo $lang['hora-comentario']?> - <?php echo $data['fecha_comentario']?></span>
                                                        </div>
                                                        <!-- /.user-block -->
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <p><?php echo $data['comentario']?></p>
                                                        </div>

                                                        <?php if(isset($comentario_hijo)){ ?>
                                                            <?php foreach ($comentario_hijo as $data_hijo){ ?>
                                                                <?php if($data_hijo['codigo_comentario_padre']==$data['codigo_comentario']){ ?>
                                                                    <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">
                                                                        <div class="col-md-12">
                                                                            <div class="nav-tabs-custom">
                                                                                <div class="tab-content listar-publicacion-content comentario-hijo">
                                                                                    <div class="active tab-pane" id="activity">
                                                                                        <!-- Post -->
                                                                                        <div class="post">
                                                                                            <!-- /.user-block -->
                                                                                            <div class="user-block contenido-publicacion-listar">
                                                                                                <p><?php echo $data_hijo['comentario']?></p>
                                                                                            </div>
                                                                                            <ul class="list-inline">
                                                                                                <li class="">
                                                                                                    <input type="hidden" name="codigo_publicacion" value="<?php echo $codigo_publicacion?>"/>
                                                                                                    <?php if($data_hijo['nick_emisor']==$_SESSION['nick']){ ?>
                                                                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Comentario']?>"/>
                                                                                                    <?php } ?>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <!-- /.post -->

                                                                                        <!-- /.post -->
                                                                                    </div>

                                                                                </div>
                                                                                <!-- /.tab-content -->
                                                                            </div>
                                                                            <!-- /.nav-tabs-custom -->
                                                                        </div>
                                                                    </form>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <textarea class="input-modificar" name="comentario" required></textarea>
                                                        </div>
                                                        <ul class="list-inline">
                                                            <li>
                                                                <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                                <input type="hidden" name="codigo_comentario" value="<?php echo $data['codigo_comentario']?>"/>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Comentario']?>"/>
                                                            </li>
                                                            <li class="pull-right">
                                                                <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Responder']?>"/>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!-- /.post -->

                                                    <!-- /.post -->
                                                </div>

                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->
                                    </div>
                                </form>
                            </div>
                            <!-- Publicacion -->
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    </section>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
