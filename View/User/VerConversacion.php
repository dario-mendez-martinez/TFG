<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$Conversacion = $_SESSION["conversacion"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <div class="col-md-12 mailbox-content  tab-content tab-content-in">
            <div class="tab-pane active text-style" id="tab1">
                <div class="mailbox-border">
                    <?php if(isset($Conversacion)){ ?>
                        <?php foreach ($Conversacion as $data){ ?>
                            <!-- Publicacion -->
                            <div class="row">
                                <!-- /.col -->
                                <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                    <div class="col-md-12">
                                        <div class="nav-tabs-custom">
                                            <div class="tab-content listar-publicacion-content">
                                                <div class="active tab-pane" id="activity">
                                                    <!-- Post -->
                                                    <div class="post">
                                                        <div class="user-block">
                                                            <div>
                                                            <span class="username">
                                                                <a href="#">Usuario: <?php echo $data['nick_emisor']?></a>
                                                            </span>
                                                            </div>
                                                            <div>
                                                                <span class="description">Hora - <?php echo $data['fecha_envio_mensaje']?></span>
                                                            </div>
                                                        </div>
                                                        <!-- /.user-block -->
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <p><?php echo $data['mensaje']?></p>
                                                        </div>
                                                        <?php
                                                            if($data['codigo_mensaje_padre']) {
                                                                $codigo_mensaje_padre = $data['codigo_mensaje_padre'];
                                                            }else{
                                                                $codigo_mensaje_padre = $data['codigo_mensaje'];
                                                            }
                                                            if($data['nick_emisor']!=$_SESSION['nick']){

                                                                $nick_receptor=$data['nick_emisor'];
                                                            }
                                                        ?>
                                                        <ul class="list-inline">
                                                            <?php if($data['nick_emisor']==$_SESSION['nick']){ ?>
                                                                <li>
                                                                    <input type="hidden" name="codigo_mensaje" value="<?php echo $data['codigo_mensaje']?>"/>
                                                                    <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Eliminar Mensaje']?>"/>
                                                                </li>
                                                            <?php } ?>
                                                            <!--<li>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Eliminar Publicacion"/>
                                                            </li>
                                                            <li class="pull-right">
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Comentarios"/>
                                                            </li>
                                                        </ul>-->
                                                    </div>
                                                    <!-- /.post -->

                                                    <!-- /.post -->
                                                </div>

                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->
                                    </div>
                                </form>
                            </div>
                            <!-- Publicacion -->
                        <?php } ?>
                    <?php } ?>
                    </section>
                </div>
            </div>
        </div>

        <div class="col-md-12 mailbox-content  tab-content tab-content-in">
            <div class="tab-pane active text-style" id="tab1">
                <div class="mailbox-border">
                    <!-- Publicacion -->
                    <div class="row">
                        <!-- /.col -->
                        <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <div class="tab-content listar-publicacion-content">
                                        <div class="active tab-pane" id="activity">
                                            <!-- Post -->
                                            <div class="post">
                                                <!-- /.user-block -->
                                                <div class="user-block contenido-publicacion-listar">
                                                    <textarea class="input-modificar" name="mensaje" required></textarea>
                                                </div>

                                                <ul class="list-inline">
                                                    <li>
                                                        <input type="hidden" name="codigo_mensaje_padre" value="<?php echo $codigo_mensaje_padre?>"/>
                                                        <input type="hidden" name="nick_receptor" value="<?php echo $nick_receptor?>"/>
                                                        <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Responder Mensaje']?>"/>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.post -->

                                            <!-- /.post -->
                                        </div>

                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- /.nav-tabs-custom -->
                            </div>
                        </form>
                    </div>
                    <!-- Publicacion -->
                    </section>
                </div>
            </div>
        </div>

                    </section>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
