<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$PUsuario = $_SESSION["perfil"];
$UPublicaciones = $_SESSION["listarPublicaciones"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->
        <?php if (isset($PUsuario)) { ?>

            <?php foreach ($PUsuario as $data){?>
                <h2><?php echo $lang['mi-perfil']?> <?php echo $data["nick"] ?></h2>
                <!---728x90--->
                <div class="col-md-4 compose">
                    <div>
                        <div class="mail-profile">
                            <div class="widget-user-image">
                                <a><img class="img-circle foto-perfil" src="<?php echo $data["foto"] ?>" alt=""></a>
                            </div>
                            <div class="mailer-name">
                                <h5>
                                    <a href="#"><?php echo $data["nombre_usuario"]." ".$data["apellido_usuario"] ?></a>
                                    <input type="hidden" name="nick" value="<?php echo $data["nick"] ?>"/>
                                </h5>
                                <h6>
                                    <a href=""><?php echo $data["email"] ?></a>
                                </h6>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="clearfix"> </div>
                        <div class="compose-bottom">
                            <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                                <nav class="nav-sidebar">
                                    <ul class="nav tabs">
                                        <li class="active">
                                            <a>
                                                <i class="fa fa-inbox"></i><?php echo $lang['telefono-user']?>: <span class="atributo-descripcion"><?php echo $data["telefono"] ?></span>
                                                <div class="clearfix"></div>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a>
                                                <i class="fa fa-envelope-o"></i><?php echo $lang['cargo-user']?>: <span class="atributo-descripcion"><?php echo $data["cargo"] ?></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a>
                                                <i class="fa fa-star-o"></i><?php echo $lang["Fecha de registro"]; ?>: <span class="atributo-descripcion"><?php echo $data["fecha_registro_usuario"] ?></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a>
                                                <i class="fa fa-pencil-square-o"></i><?php echo $lang['descripcion-texto']?> <span class="atributo-perfil"><?php echo $data["nick"] ?></span>:  <span class="atributo-descripcion"><?php echo $data["descripcion_usuario"] ?></span>
                                                <div class="clearfix"></div>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </form>
                        </div>
                    </div>
            <?php }?>
        <?php } ?>
            <div class="amigos-usuario">
                <div class="mail-profile">
                    <div class="mail-pic foto-usuario">
                        <a href="#"><img src="../../Recursos/styleUser/images/p7.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="inbox">
            <div class="col-md-8 mailbox-content  tab-content tab-content-in">
                <div class="tab-pane active text-style" id="tab1">
                    <div class="mailbox-border">
                        <?php if(isset($UPublicaciones)){ ?>
                            <?php foreach ($UPublicaciones as $data){ ?>
                                <!-- Publicacion -->
                                <!-- /.col -->
                                <div class="col-md-12">
                                    <form role="form" enctype="multipart/form-data" action="../../Controller/PublicacionController.php" method="post">

                                        <div class="nav-tabs-custom">
                                            <div class="tab-content listar-publicacion-content">
                                                <div class="active tab-pane" id="activity">
                                                    <!-- Post -->
                                                    <div class="post">
                                                        <div class="user-block">
                                                            <div>
                                                            <span class="username">
                                                                <a href="#"><?php echo $lang['user-creador-publicacion']?>: <?php echo $data['nick_propietario']?></a>
                                                            </span>
                                                            </div>
                                                            <div>
                                                                <span class="description"><?php echo $lang['hora-publicacion']?> - <?php echo $data['fecha_publicacion']?></span>
                                                            </div>
                                                        </div>
                                                        <!-- /.user-block -->
                                                        <div class="user-block contenido-publicacion-listar">
                                                            <p><?php echo $data['contenido']?></p>
                                                        </div>

                                                        <!--<ul class="list-inline">
                                                            <li>
                                                                <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Modificar Publicacion"/>
                                                            </li>
                                                            <li>
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Eliminar Publicacion"/>
                                                            </li>
                                                            <li class="pull-right">
                                                                <input type="submit" class="link-black text-sm" name="accion" value="Comentarios"/>
                                                            </li>
                                                        </ul>-->
                                                        <li>
                                                            <input type="hidden" name="codigo_publicacion" value="<?php echo $data['codigo_publicacion']?>"/>
                                                            <input type="submit" class="link-black text-sm" name="accion" value="<?php echo $lang['Ver Publicacion']?>"/>
                                                        </li>
                                                    </div>
                                                    <!-- /.post -->

                                                    <!-- /.post -->
                                                </div>

                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- /.nav-tabs-custom -->

                                    </form>
                                </div>
                                <!-- Publicacion -->
                            <?php } ?>
                        <?php } ?>
                        </section>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>

