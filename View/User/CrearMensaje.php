<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';
$LUsuarios = $_SESSION["listarAmigos"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->

        <h2><?php echo $lang['Crear-Mensaje-titulo']?></h2>
        <!---728x90--->
        <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
            <div class="col-md-12 compose">
                <div>

                    <div class="mail-profile">
                        <div class="col-md-4">
                            <h2></h2>
                        </div>
                        <div class="col-md-8">
                            <div style="margin-top: 10px;" class="col-md-12">
                                <input type="submit" class="b-ModificarUser btn btn-info" name="accion" value="<?php echo $lang['Enviar Mensaje']?>">
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div class="clearfix"> </div>

                    <div class="compose-bottom">

                        <nav class="nav-sidebar">
                            <ul class="nav tabs">
                                <li class="active">
                                    <a>
                                        <i class="fa fa-inbox"></i><?php echo $lang['Contenido del mensaje']?>: <textarea  type="text" class="input-modificar-usuario contenido-publicacion" name="mensaje" required></textarea>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <?php if(isset($LUsuarios)){ ?>
                                    <li class="active">
                                        <a>
                                            <i class="fa fa-inbox"></i><?php echo $lang['usuario-solo']?>:
                                            <select class="input-modificar-usuario" name="nick_receptor">
                                                <option value=""></option>
                                                <?php foreach ($LUsuarios as $data) { ?>
                                                    <option value="<?php echo $data['nick'] ?>"><?php echo $data['nick'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </nav>

                    </div>

                    <div class="col-md-12 boton-modificar-user">

                    </div>
                </div>
        </form>


        <div>
            <?php if(isset($_GET["registro_grupo_vacio"]) &&  $_GET["registro_grupo_vacio"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡El nombre del grupo no puede estar vacio!</span>
                </div>
            <?php }?>
            <?php if(isset($_GET["registro_grupo_error"]) &&  $_GET["registro_grupo_error"]==true){ ?>
                <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                    <span>¡El nombre del grupo ya existe!</span>
                </div>
            <?php }?>
        </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
