<?php
require_once'../ScriptsAcceso/Acceso.php';
require_once'Estructura/header.php';
require_once'Estructura/nav.php';

$PUsuario = $_SESSION["perfil"];
?>
<!--Contenido de la página-->
<div class="inner-block">
    <div class="inbox">
        <!---728x90--->
        <?php if(isset($PUsuario)) { ?>

            <?php foreach ($PUsuario as $data){?>
                <h2><?php echo $lang['Modificar Usuario']?></h2>
                <!---728x90--->
                <form role="form" enctype="multipart/form-data" action="../../Controller/UsuarioController.php" method="post">
                    <div class="col-md-12 compose">
                        <div>

                            <div class="mail-profile">
                                <div class="col-md-2 widget-user-image">
                                    <a><img class="img-circle foto-perfil" src="<?php echo $data["foto"] ?>" alt=""></a>
                                </div>
                                <div class="col-md-6">
                                    <h3><?php echo $data["nombre_usuario"]." ".$data["apellido_usuario"] ?></h3>
                                    <input type="hidden" name="nick" id="nick" value="<?php echo $data["nick"] ?>"/>
                                    <h4><?php echo $data["email"] ?></h4>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <input type="submit" class="b-ModificarUser btn btn-info" data-toggle="modal" data-target="#modal-default" id="botonEliminarUsuario" name="accion" value="<?php echo $lang['Eliminar Usuario']?>">
                                    </div>
                                    <div style="margin-top: 10px;" class="col-md-12">
                                        <input type="submit" class="b-ModificarUser btn btn-info" name="accion" value="<?php echo $lang['Modificar']?>">
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="clearfix"> </div>

                            <div class="compose-bottom">

                                    <nav class="nav-sidebar">
                                        <ul class="nav tabs">
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['nombre-user']?>: <input class="input-modificar-usuario" name="nombre" value="<?php echo $data["nombre_usuario"] ?>"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['apellido-user']?>: <input class="input-modificar-usuario" name="apellido" value="<?php echo $data["apellido_usuario"] ?>"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['contraseña-user']?>: <input type="password" class="input-modificar-usuario" name="password1"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['contraseña-r-user']?>: <input type="password" class="input-modificar-usuario" name="password2"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a>
                                                    <i class="fa fa-inbox"></i><?php echo $lang['telefono-user']?>: <input class="input-modificar-usuario" name="telefono" value="<?php echo $data["telefono"] ?>"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="">
                                                <a>
                                                    <i class="fa fa-envelope-o"></i><?php echo $lang['cargo-user']?>: <input class="input-modificar-usuario" name="cargo" value="<?php echo $data["cargo"] ?>"/>
                                                </a>
                                            </li>
                                            <li class="">
                                                <a>
                                                    <i class="fa fa-pencil-square-o"></i><?php echo $lang['descripcion-texto']?> <span class="atributo-perfil"><?php echo $data["nick"] ?></span>:  <input class="input-modificar-usuario" name="descripcion" value="<?php echo $data["descripcion_usuario"] ?>"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                            <li class="">
                                                <a>
                                                    <?php echo $lang['foto-user']?>:  <input class="input-modificar-usuario" type="file" name="foto"/>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>

                            </div>

                            <div class="col-md-12 boton-modificar-user">

                            </div>
                        </div>

                        <div class="modal" id="modal-default" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" id="cerrarModal">×</span></button>
                                        <h4 class="modal-title">ATENCIÓN</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>¿Está seguro de eliminar el usuario?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" id="cerrarModalButton">No</button>
                                        <button type="button" class="btn btn-primary" id="EliminarUsuario">Si</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                </form>
            <?php }?>
            <?php } ?>

            <div>
                <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                    <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                        <span>¡Las contraseñas no son iguales!</span>
                    </div>
                    <?php if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){ ?>
                        <div style="margin-top: 10px;" class="alert alert-info alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                            <span>La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres</span>
                        </div>
                    <?php }?>
                <?php }else if(isset($_GET["error_reglas_password"]) &&  $_GET["error_reglas_password"]==true){?>
                    <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                        <span>La contraseña debe contener mínimo una letra minúscula, una letra mayúscula,un número y una longitud entre 6 y 16 caracteres</span>
                    </div>
                    <?php if(isset($_GET["error_password"]) &&  $_GET["error_password"]==true){ ?>
                        <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                            <span>¡Las contraseñas no son iguales!</span>
                        </div>
                    <?php }?>
                <?php }?>

                <?php if(isset($_GET["error_foto"]) &&  $_GET["error_foto"]==true){ ?>
                    <div style="margin-top: 10px;" class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                        <span>¡Ha ocurrido un error al subir su imagen!</span>
                    </div>
                <?php }?>

                <?php if(isset($_GET["exito"]) &&  $_GET["exito"]==true){ ?>
                    <div style="margin-top: 10px;" class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
                        Todo ha ido bien! Sus datos se han modificado. </div>
                <?php }?>
            </div>
    </div>
</div>
<!--Fin contenido de la página-->


<?php
require_once 'Estructura/footer.php';
?>
