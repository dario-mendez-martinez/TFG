$(document).ready(function() {
    $("#botonEliminarUsuario").on('click',function () {
        $("#modal-default").css("display", "-webkit-box");
        return false;
    });
    $("#cerrarModal").on('click',function () {
        $("#modal-default").css("display", "none");
    });
    $("#cerrarModalButton").on('click',function () {
        $("#modal-default").css("display", "none");
    });
    $("#cerrarModal").on('submit',function () {
        $("#modal-default").css("display", "none");
    });
    $("#EliminarUsuario").on('click',function(e){
        e.preventDefault();
        //llamada al fichero PHP con AJAX
        $.ajax({
            url:   '../../Controller/UsuarioController.php?accion=Eliminar Usuario',
            data: {
                "nick": $('#nick').val(),
            },
            type:  'post',
            beforeSend: function () {
            },
            success:  function (response) {
                $("#modal-default").css("display", "none");
                window.location.href = "http://localhost/TFG/View/Login.php";

            }
        });
    });

    $("#modalEliminarGrupo").on('click',function (e) {
        e.preventDefault();
        $('#nombre_grupo_eliminar').text(e.target.attributes.key.value);
        $("#modal-default").css("display", "-webkit-box");
        return false;
    });

    $("#EliminarGrupo").on('click',function(e){
        e.preventDefault();

        //llamada al fichero PHP con AJAX
        $.ajax({
            url:   '../../Controller/GrupoController.php?accion=Eliminar grupo',
            data: {
                "nombre_grupo": $('#nombre_grupo_eliminar').text(),
            },
            type:  'post',
            beforeSend: function () {
            },
            success:  function (response) {
                $("#modal-default").css("display", "none");
                window.location.href = "http://localhost/TFG/View/Login.php";

            }
        });
    });
});